# Authors

The following people have contributed to the translation
of Linux manpages. The list is sorted alphabetically.


## Brazilian Portuguese:

* André L. Fassone Canova <lonelywolf@blv.com.br>
* Carlos Augusto Horylka <horylka@conectiva.com.br>
* Leslie Harlley Watter <leslie@netpar.com.br>
* Rafael Fontenelle <rafaelff@gnome.org>
* Roberto Selbach Teixeira <robteix@zaz.com.br>
* Rubens de Jesus Nogueira <darkseid99@usa.net>


## Dutch:

* Joost van Baal <joostv-manpages-nl-2398@mdcc.cx>
* Jos Boersema <joshb@xs4all.nl>
* Mario Blättermann <mario.blaettermann@gmail.com>


## French:

* Alain Portal <aportal@univ-montp2.fr>
* Alexandre Kuoch <alex.kuoch@gmail.com>
* Alexandre Normand <aj.normand@free.fr>
* Amand Tihon <amand@alrj.org>
* Aymeric Nys <aymeric AT nnx POINT com>
* Bastien Scher <bastien0705@gmail.com>
* Bernard Siaud
* carmie
* Cédric Boutillier <cedric.boutillier@gmail.com>
* Cédric Lucantis <omer@no-log.org>
* Christophe Blaess <ccb@club-internet.fr>
* Christophe Blaess <http://www.blaess.fr/christophe/>
* Christophe Sauthier <christophe@sauthier.com>
* Cyril Guilloud <guilloud@lautre.net>
* Danny <dannybrain@noos.fr>
* David Prévot <david@tilapin.org>
* Denis Barbier <barbier@debian.org>
* Denis Mugnier <myou72@orange.fr>
* Dominique Simen <dominiquesimen@hotmail.com>
* Emmanuel Araman <Emmanuel@araman.org>
* Éric Piel <eric.piel@tremplin-utc.net>
* Florentin Duneau <fduneau@gmail.com>
* Franck Bassi <fblinux@wanadoo.fr>
* François Micaux
* François Wendling <frwendling@free.fr>
* Frederic Daniel Luc Lehobey <Frederic@Lehobey.net>
* Frédéric Delanoy <delanoy_f@yahoo.com>
* Frédéric Hantrais <fhantrais@gmail.com>
* Frédéric Zulian <zulian@free.fr>
* Gérard Delafond <gerard@delafond.org>
* Grégoire Scano <gregoire.scano@malloc.fr>
* Grégory Colpart <reg@evolix.fr>
* Guilhelm Panaget <guilhelm.panaget@free.fr>
* Guillaume Bour
* Guillaume Delacour <guillaume.delacour@gmail.com>
* Jade Alglave <jade.alglave@ens-lyon.org>
* Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>
* Jean-Baptiste Holcroft <jean-baptiste@holcroft.fr>
* Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>
* Jean-Marc Chaton <chaton@debian.org>
* Jean-Philippe Guérard <fevrier@tigreraye.org>
* Jean-Philippe MENGUAL <jpmengual@debian.org>
* Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>
* Jérôme Perzyna <jperzyna@yahoo.fr>
* José JORGE
* Julien Cristau <jcristau@debian.org>
* Julien Rosal <albator.deb@gmail.com>
* Luc Froidefond <luc.froidefond@free.fr>
* Lyes Zemmouche <iliaas@hotmail.fr>
* Michel Quercia <quercia AT cal DOT enst DOT fr>
* Nicolas François <nicolas.francois@centraliens.net>
* Nicolas Haller <nicolas@boiteameuh.org>
* Nicolas Sauzède <nsauzede@free.fr>
* Philippe Batailler
* Philippe Piette <foudre-blanche@skynet.be>
* Romain Doumenc <rd6137@gmail.com>
* Sébastien Blanchet
* Simon Depiets
* Simon Paillard <simon.paillard@resel.enst-bretagne.fr>
* Stephane Blondon <stephane.blondon@gmail.com>
* Stéphan Rafin <stephan.rafin@laposte.net>
* Steve Petruzzello <dlist@bluewin.ch>
* Sylvain Archenault <sylvain.archenault@laposte.net>
* Sylvain Cherrier <sylvain.cherrier@free.fr>
* Thierry Vignaud <tvignaud@mandriva.com>
* Thomas Blein <tblein@tblein.eu>
* Thomas Huriaux <thomas.huriaux@gmail.com>
* Thomas Vincent <thomas@vinc-net.fr>
* Valéry Perrin <valery.perrin.debian@free.fr>
* Yves Rütschlé <l10n@rutschle.net>


## German:

* Aldo Valente <aldo@dagobar.rhein.de>
* Alexander Bachmer <alex.bachmer@t-online.de>
* Andreas Braukmann <andy@abra.de>
* Andreas D. Preissig <andreas@sanix.ruhr.de>
* Chris Leick <c.leick@vollbio.de>
* Christian Schmidt <c.schmidt@ius.gun.de>
* Daniel Kobras <kobras@linux.de>
* David Thamm <dthamm@bfs.de>
* Dennis Stampfer <kontakt@dstampfer.de>
* Dr. Tobias Quathamer <toddy@debian.org>
* Eduard Bloch <blade@debian.org>
* Elmar Jansen <ej@pumuckel.gun.de>
* Erik Pfannenstein <debianignatz@gmx.de>
* Florian Jenn <jennf@tu-cottbus.de>
* Florian Rehnisch <fr@fm-r.eu>
* Frank Stähr <der-storch-85@gmx.net>
* Gerd Koenig <koenig.bodensee@googlemail.com>
* Hanno Wagner <wagner@bidnix.bid.fh-hannover.de>
* Helge Kreutzmann <debian@helgefjell.de>
* Holger Wansing <linux@wansing-online.de>
* Jens Püschel <jepu0000@stud.uni-sb.de>
* Jens Rohler <jkcr@rohler.de>
* Jens Seidel <tux-master@web.de>
* Jochen Hein <jochen@jochen.org>
* Joern Vehoff <joern@vehoff.net>
* Johnny Teveßen <j.tevessen@gmx.de>
* Jonas Rovan <jonas@blitz.de>
* Karl Eichwalder <ke@suse.de>
* Lars J. Brandt <ljbrandt@jorma.ping.de>
* Lutz Behnke <lutz.behnke@gmx.de>
* Lutz Donnerhacke <Lutz.Donnerhacke@Jena.Thur.De>
* Maik Messerschmidt <Maik.Messerschmidt@gmx.net>
* Mario Blättermann <mario.blaettermann@gmail.com>
* Markus Hiereth <markus.hiereth@freenet.de>
* Markus Kaufmann <markus.kaufmann@gmx.de>
* Markus Schmitt <fw@math.uni-sb.de>
* Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>
* Martin Okrslar <okrslar@informatik.uni-muenchen.de>
* Martin Schmitt <martin@schmitt.li>
* Martin Schulze <joey@infodrom.org>
* Michaela Hohenner <mhohenne@techfak.uni-bielefeld.de>
* Michael Arndt <michael@scriptkiller.de>
* Michael Haardt <michael@moria.de>
* Michael Piefel <piefel@debian.org>
* Michael Schmidt <michael@guug.de>
* Mike Fengler <mike@krt3.krt-soft.de>
* Norbert Kümin <norbert.kuemin@lugs.ch>
* Norbert Weuster <weuster@etecs0.uni-duisburg.de>
* Patrick Rother <krd@gulu.net>
* Peter Gerbrandt <pgerbrandt@bfs.de>
* Ralf Baechle <ralf@waldorf-gmbh.de>
* Ralf Baumert <bau@heineken.chemie.uni-dortmund.de>
* Ralf Demmer <rdemmer@rdemmer.de>
* Regine Bast <regine.bast@bigfoot.com>
* René Tschirley <gremlin@cs.tu-berlin.de>
* Roland Krause <Rokrause@aol.com>
* Sebastian Hetze <S.Hetze@Linux-AG.com>
* Sebastian Rittau <srittau@jroger.in-berlin.de>
* Stefan Janke <gonzo@burg.studfb.unibw-muenchen.de>
* Stephan Beck <tlahcuilo@gmx.net>
* Thomas Koenig <ig25@rz.uni-karlsruhe.de>
* Walter Harms <walter.harms@informatik.uni-oldenburg.de>
* Wolfgang Jung <woju@keep.in-berlin.de>


## Polish:

* Adam Byrtek <alpha@irc.pl>
* Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>
* Artur Kruszewski <mazdac@gmail.com>
* Bartek Głośnicki <bartek@miramex.com.pl>
* Bartłomiej Sowa <bartowl@kki.net.pl>
* Bartosz Jakubski <B.Jakubski@supernet.com.pl>
* Damian Szeluga <damjanek@gentoo.pl>
* Daniel Koć <kocio@linuxnews.pl>
* Grzegorz Goławski <grzegol@pld.org.pl>
* Gwidon S. Naskrent <naskrent@hoth.amu.edu.pl>
* Jakub Bogusz (eglibc) <qboosh@pld-linux.org>
* Jakub Klimczak <zuomarket@tuta.io>
* Jarek Wołoszyn <yossa@dione.ids.pl>
* Jarosław Beczek <bexx@poczta.onet.pl>
* Kuba Basiura <kuba.basiura@gmail.com>
* Leszek Krupiński <d@z.pl>
* Łukasz Kowalczyk <lukow@tempac.okwf.fuw.edu.pl>
* Maciej Wojciechowski <wojciech@staszic.waw.pl>
* Marcin Garski <mgarski@post.pl>
* Marcin Mazurek <mazek@capella.ae.poznan.pl>
* Michał Górny <zrchos+manpagespl@gmail.com>
* Michał Kułach <michal.kulach@gmail.com>
* Paweł Krawczyk (eglibc) <kravietz@ceti.pl>
* Paweł Olszewski <alder@amg.net.pl>
* Paweł Sędrowski (ptm.berlios.de) <sedros@gmail.com>
* Paweł Wilk <siefca@pl.qmail.org>
* Piotr Pogorzelski <piotr.pogorzelski@ippt.gov.pl>
* Piotr Roszatycki <dexter@debian.org>
* Przemek Borys <pborys@dione.ids.pl>
* Radek Marcinkowski <radek@cbk.waw.pl>
* Rafał Lewczuk <R.Lewczuk@elka.pw.edu.p>
* Rafał Maszkowski <rzm@icm.edu.pl>
* Rafał Witowski
* Robert Luberda <robert@debian.org>
* Szymon Lamkiewicz <s.lam@o2.pl>
* Tomasz Kłoczko <kloczek@rudy.mif.pg.gda.pl>
* Tomasz Wendlandt <juggler@box.cp.com.pl>
* Wiktor J. Łukasik <wiktorlu@technologist.com>
* Wojtek Kotwica <wkotwica@post.pl>


## Romanian:

* Eugen Hoanca <eugenh@urban-grafx.ro>
* Laurențiu Buzdugan <lbuz@rolix.org>
* Mihai Cristescu <mihai.cristescu@gmail.com>
