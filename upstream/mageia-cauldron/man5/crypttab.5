'\" t
.TH "CRYPTTAB" "5" "" "systemd 245" "crypttab"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
crypttab \- Configuration for encrypted block devices
.SH "SYNOPSIS"
.PP
/etc/crypttab
.SH "DESCRIPTION"
.PP
The
/etc/crypttab
file describes encrypted block devices that are set up during system boot\&.
.PP
Empty lines and lines starting with the
"#"
character are ignored\&. Each of the remaining lines describes one encrypted block device\&. Fields are delimited by white space\&.
.PP
Each line is in the form
.sp
.if n \{\
.RS 4
.\}
.nf
\fIname\fR \fIencrypted\-device\fR \fIpassword\fR \fIoptions\fR
.fi
.if n \{\
.RE
.\}
.sp
The first two fields are mandatory, the remaining two are optional\&.
.PP
Setting up encrypted block devices using this file supports three encryption modes: LUKS, TrueCrypt and plain\&. See
\fBcryptsetup\fR(8)
for more information about each mode\&. When no mode is specified in the options field and the block device contains a LUKS signature, it is opened as a LUKS device; otherwise, it is assumed to be in raw dm\-crypt (plain mode) format\&.
.PP
The first field contains the name of the resulting encrypted block device; the device is set up within
/dev/mapper/\&.
.PP
The second field contains a path to the underlying block device or file, or a specification of a block device via
"UUID="
followed by the UUID\&.
.PP
The third field specifies the encryption password\&. If the field is not present or the password is set to
"none"
or
"\-", the password has to be manually entered during system boot\&. Otherwise, the field is interpreted as an absolute path to a file containing the encryption password\&. For swap encryption,
/dev/urandom
or the hardware device
/dev/hw_random
can be used as the password file; using
/dev/random
may prevent boot completion if the system does not have enough entropy to generate a truly random encryption key\&.
.PP
The fourth field, if present, is a comma\-delimited list of options\&. The following options are recognized:
.PP
\fBcipher=\fR
.RS 4
Specifies the cipher to use\&. See
\fBcryptsetup\fR(8)
for possible values and the default value of this option\&. A cipher with unpredictable IV values, such as
"aes\-cbc\-essiv:sha256", is recommended\&.
.RE
.PP
\fBdiscard\fR
.RS 4
Allow discard requests to be passed through the encrypted block device\&. This improves performance on SSD storage but has security implications\&.
.RE
.PP
\fBhash=\fR
.RS 4
Specifies the hash to use for password hashing\&. See
\fBcryptsetup\fR(8)
for possible values and the default value of this option\&.
.RE
.PP
\fBheader=\fR
.RS 4
Use a detached (separated) metadata device or file where the LUKS header is stored\&. This option is only relevant for LUKS devices\&. See
\fBcryptsetup\fR(8)
for possible values and the default value of this option\&.
.RE
.PP
\fBkeyfile\-offset=\fR
.RS 4
Specifies the number of bytes to skip at the start of the key file\&. See
\fBcryptsetup\fR(8)
for possible values and the default value of this option\&.
.RE
.PP
\fBkeyfile\-size=\fR
.RS 4
Specifies the maximum number of bytes to read from the key file\&. See
\fBcryptsetup\fR(8)
for possible values and the default value of this option\&. This option is ignored in plain encryption mode, as the key file size is then given by the key size\&.
.RE
.PP
\fBkey\-slot=\fR
.RS 4
Specifies the key slot to compare the passphrase or key against\&. If the key slot does not match the given passphrase or key, but another would, the setup of the device will fail regardless\&. This option implies
\fBluks\fR\&. See
\fBcryptsetup\fR(8)
for possible values\&. The default is to try all key slots in sequential order\&.
.RE
.PP
\fBkeyfile\-timeout=\fR
.RS 4
Specifies the timeout for the device on which the key file resides and falls back to a password if it could not be mounted\&. See
\fBsystemd-cryptsetup-generator\fR(8)
for key files on external devices\&.
.RE
.PP
\fBluks\fR
.RS 4
Force LUKS mode\&. When this mode is used, the following options are ignored since they are provided by the LUKS header on the device:
\fBcipher=\fR,
\fBhash=\fR,
\fBsize=\fR\&.
.RE
.PP
\fB_netdev\fR
.RS 4
Marks this cryptsetup device as requiring network\&. It will be started after the network is available, similarly to
\fBsystemd.mount\fR(5)
units marked with
\fB_netdev\fR\&. The service unit to set up this device will be ordered between
remote\-fs\-pre\&.target
and
remote\-cryptsetup\&.target, instead of
cryptsetup\-pre\&.target
and
cryptsetup\&.target\&.
.sp
Hint: if this device is used for a mount point that is specified in
\fBfstab\fR(5), the
\fB_netdev\fR
option should also be used for the mount point\&. Otherwise, a dependency loop might be created where the mount point will be pulled in by
local\-fs\&.target, while the service to configure the network is usually only started
\fIafter\fR
the local file system has been mounted\&.
.RE
.PP
\fBnoauto\fR
.RS 4
This device will not be added to
cryptsetup\&.target\&. This means that it will not be automatically unlocked on boot, unless something else pulls it in\&. In particular, if the device is used for a mount point, it\*(Aqll be unlocked automatically during boot, unless the mount point itself is also disabled with
\fBnoauto\fR\&.
.RE
.PP
\fBnofail\fR
.RS 4
This device will not be a hard dependency of
cryptsetup\&.target\&. It\*(Aqll still be pulled in and started, but the system will not wait for the device to show up and be unlocked, and boot will not fail if this is unsuccessful\&. Note that other units that depend on the unlocked device may still fail\&. In particular, if the device is used for a mount point, the mount point itself also needs to have the
\fBnofail\fR
option, or the boot will fail if the device is not unlocked successfully\&.
.RE
.PP
\fBoffset=\fR
.RS 4
Start offset in the backend device, in 512\-byte sectors\&. This option is only relevant for plain devices\&.
.RE
.PP
\fBplain\fR
.RS 4
Force plain encryption mode\&.
.RE
.PP
\fBread\-only\fR, \fBreadonly\fR
.RS 4
Set up the encrypted block device in read\-only mode\&.
.RE
.PP
\fBsame\-cpu\-crypt\fR
.RS 4
Perform encryption using the same cpu that IO was submitted on\&. The default is to use an unbound workqueue so that encryption work is automatically balanced between available CPUs\&.
.sp
This requires kernel 4\&.0 or newer\&.
.RE
.PP
\fBsubmit\-from\-crypt\-cpus\fR
.RS 4
Disable offloading writes to a separate thread after encryption\&. There are some situations where offloading write bios from the encryption threads to a single thread degrades performance significantly\&. The default is to offload write bios to the same thread because it benefits CFQ to have writes submitted using the same context\&.
.sp
This requires kernel 4\&.0 or newer\&.
.RE
.PP
\fBskip=\fR
.RS 4
How many 512\-byte sectors of the encrypted data to skip at the beginning\&. This is different from the
\fBoffset=\fR
option with respect to the sector numbers used in initialization vector (IV) calculation\&. Using
\fBoffset=\fR
will shift the IV calculation by the same negative amount\&. Hence, if
\fBoffset=\fR\fB\fIn\fR\fR
is given, sector
\fIn\fR
will get a sector number of 0 for the IV calculation\&. Using
\fBskip=\fR
causes sector
\fIn\fR
to also be the first sector of the mapped device, but with its number for IV generation being
\fIn\fR\&.
.sp
This option is only relevant for plain devices\&.
.RE
.PP
\fBsize=\fR
.RS 4
Specifies the key size in bits\&. See
\fBcryptsetup\fR(8)
for possible values and the default value of this option\&.
.RE
.PP
\fBsector\-size=\fR
.RS 4
Specifies the sector size in bytes\&. See
\fBcryptsetup\fR(8)
for possible values and the default value of this option\&.
.RE
.PP
\fBswap\fR
.RS 4
The encrypted block device will be used as a swap device, and will be formatted accordingly after setting up the encrypted block device, with
\fBmkswap\fR(8)\&. This option implies
\fBplain\fR\&.
.sp
WARNING: Using the
\fBswap\fR
option will destroy the contents of the named partition during every boot, so make sure the underlying block device is specified correctly\&.
.RE
.PP
\fBtcrypt\fR
.RS 4
Use TrueCrypt encryption mode\&. When this mode is used, the following options are ignored since they are provided by the TrueCrypt header on the device or do not apply:
\fBcipher=\fR,
\fBhash=\fR,
\fBkeyfile\-offset=\fR,
\fBkeyfile\-size=\fR,
\fBsize=\fR\&.
.sp
When this mode is used, the passphrase is read from the key file given in the third field\&. Only the first line of this file is read, excluding the new line character\&.
.sp
Note that the TrueCrypt format uses both passphrase and key files to derive a password for the volume\&. Therefore, the passphrase and all key files need to be provided\&. Use
\fBtcrypt\-keyfile=\fR
to provide the absolute path to all key files\&. When using an empty passphrase in combination with one or more key files, use
"/dev/null"
as the password file in the third field\&.
.RE
.PP
\fBtcrypt\-hidden\fR
.RS 4
Use the hidden TrueCrypt volume\&. This option implies
\fBtcrypt\fR\&.
.sp
This will map the hidden volume that is inside of the volume provided in the second field\&. Please note that there is no protection for the hidden volume if the outer volume is mounted instead\&. See
\fBcryptsetup\fR(8)
for more information on this limitation\&.
.RE
.PP
\fBtcrypt\-keyfile=\fR
.RS 4
Specifies the absolute path to a key file to use for a TrueCrypt volume\&. This implies
\fBtcrypt\fR
and can be used more than once to provide several key files\&.
.sp
See the entry for
\fBtcrypt\fR
on the behavior of the passphrase and key files when using TrueCrypt encryption mode\&.
.RE
.PP
\fBtcrypt\-system\fR
.RS 4
Use TrueCrypt in system encryption mode\&. This option implies
\fBtcrypt\fR\&.
.RE
.PP
\fBtcrypt\-veracrypt\fR
.RS 4
Check for a VeraCrypt volume\&. VeraCrypt is a fork of TrueCrypt that is mostly compatible, but uses different, stronger key derivation algorithms that cannot be detected without this flag\&. Enabling this option could substantially slow down unlocking, because VeraCrypt\*(Aqs key derivation takes much longer than TrueCrypt\*(Aqs\&. This option implies
\fBtcrypt\fR\&.
.RE
.PP
\fBtimeout=\fR
.RS 4
Specifies the timeout for querying for a password\&. If no unit is specified, seconds is used\&. Supported units are s, ms, us, min, h, d\&. A timeout of 0 waits indefinitely (which is the default)\&.
.RE
.PP
\fBtmp\fR
.RS 4
The encrypted block device will be prepared for using it as
/tmp; it will be formatted using
\fBmke2fs\fR(8)\&. This option implies
\fBplain\fR\&.
.sp
WARNING: Using the
\fBtmp\fR
option will destroy the contents of the named partition during every boot, so make sure the underlying block device is specified correctly\&.
.RE
.PP
\fBtries=\fR
.RS 4
Specifies the maximum number of times the user is queried for a password\&. The default is 3\&. If set to 0, the user is queried for a password indefinitely\&.
.RE
.PP
\fBverify\fR
.RS 4
If the encryption password is read from console, it has to be entered twice to prevent typos\&.
.RE
.PP
\fBpkcs11\-uri=\fR
.RS 4
Takes a
\m[blue]\fBRFC7512 PKCS#11 URI\fR\m[]\&\s-2\u[1]\d\s+2
pointing to a private RSA key which is used to decrypt the key specified in the third column of the line\&. This is useful for unlocking encrypted volumes through security tokens or smartcards\&. See below for an example how to set up this mechanism for unlocking a LUKS volume with a YubiKey security token\&. The specified URI can refer directly to a private RSA key stored on a token or alternatively just to a slot or token, in which case a search for a suitable private RSA key will be performed\&. In this case if multiple suitable objects are found the token is refused\&. The key configured in the third column is passed as is to RSA decryption\&. The resulting decrypted key is then base64 encoded before it is used to unlock the LUKS volume\&.
.RE
.PP
\fBx\-systemd\&.device\-timeout=\fR
.RS 4
Specifies how long systemd should wait for a device to show up before giving up on the entry\&. The argument is a time in seconds or explicitly specified units of
"s",
"min",
"h",
"ms"\&.
.RE
.PP
\fBx\-initrd\&.attach\fR
.RS 4
Setup this encrypted block device in the initramfs, similarly to
\fBsystemd.mount\fR(5)
units marked with
\fBx\-initrd\&.mount\fR\&.
.sp
Although it\*(Aqs not necessary to mark the mount entry for the root file system with
\fBx\-initrd\&.mount\fR,
\fBx\-initrd\&.attach\fR
is still recommended with the encrypted block device containing the root file system as otherwise systemd will attempt to detach the device during the regular system shutdown while it\*(Aqs still in use\&. With this option the device will still be detached but later after the root file system is unmounted\&.
.sp
All other encrypted block devices that contain file systems mounted in the initramfs should use this option\&.
.RE
.PP
At early boot and when the system manager configuration is reloaded, this file is translated into native systemd units by
\fBsystemd-cryptsetup-generator\fR(8)\&.
.SH "EXAMPLES"
.PP
\fBExample\ \&1.\ \&/etc/crypttab example\fR
.PP
Set up four encrypted block devices\&. One using LUKS for normal storage, another one for usage as a swap device and two TrueCrypt volumes\&.
.sp
.if n \{\
.RS 4
.\}
.nf
luks       UUID=2505567a\-9e27\-4efe\-a4d5\-15ad146c258b
swap       /dev/sda7       /dev/urandom       swap
truecrypt  /dev/sda2       /etc/container_password  tcrypt
hidden     /mnt/tc_hidden  /dev/null    tcrypt\-hidden,tcrypt\-keyfile=/etc/keyfile
external   /dev/sda3       keyfile:LABEL=keydev keyfile\-timeout=10s
.fi
.if n \{\
.RE
.\}
.PP
\fBExample\ \&2.\ \&Yubikey\-based Volume Unlocking Example\fR
.PP
The PKCS#11 logic allows hooking up any compatible security token that is capable of storing RSA decryption keys\&. Here\*(Aqs an example how to set up a Yubikey security token for this purpose, using
\fBykman\fR
from the yubikey\-manager project:
.sp
.if n \{\
.RS 4
.\}
.nf
# Make sure noone can read the files we generate but us
umask 077

# Destroy any old key on the Yubikey (careful!)
ykman piv reset

# Generate a new private/public key pair on the device, store the public key in \*(Aqpubkey\&.pem\*(Aq\&.
ykman piv generate\-key \-a RSA2048 9d pubkey\&.pem

# Create a self\-signed certificate from this public key, and store it on the
# device\&. The "subject" should be an arbitrary string to identify the token in
# the p11tool output below\&.
ykman piv generate\-certificate \-\-subject "Knobelei" 9d pubkey\&.pem

# Check if the newly create key on the Yubikey shows up as token in PKCS#11\&. Have a look at the output, and
# copy the resulting token URI to the clipboard\&.
p11tool \-\-list\-tokens

# Generate a (secret) random key to use as LUKS decryption key\&.
dd if=/dev/urandom of=plaintext\&.bin bs=128 count=1

# Encode the secret key also as base64 text (with all whitespace removed)
base64 < plaintext\&.bin | tr \-d \*(Aq\en\er\et \*(Aq > plaintext\&.base64

# Encrypt this newly generated (binary) LUKS decryption key using the public key whose private key is on the
# Yubikey, store the result in /etc/encrypted\-luks\-key\&.bin, where we\*(Aqll look for it during boot\&.
sudo openssl rsautl \-encrypt \-pubin \-inkey pubkey\&.pem \-in plaintext\&.bin \-out /etc/encrypted\-luks\-key\&.bin

# Configure the LUKS decryption key on the LUKS device\&. We use very low pbkdf settings since the key already
# has quite a high quality (it comes directly from /dev/urandom after all), and thus we don\*(Aqt need to do much
# key derivation\&. Replace /dev/sdXn by the partition to use (e\&.g\&. sda1)
sudo cryptsetup luksAddKey /dev/sdXn plaintext\&.base64 \-\-pbkdf=pbkdf2 \-\-pbkdf\-force\-iterations=1000

# Now securely delete the plain text LUKS key, we don\*(Aqt need it anymore, and since it contains secret key
# material it should be removed from disk thoroughly\&.
shred \-u plaintext\&.bin plaintext\&.base64

# We don\*(Aqt need the public key anymore either, let\*(Aqs remove it too\&. Since this one is not security
# sensitive we just do a regular "rm" here\&.
rm pubkey\&.pem

# Test: Let\*(Aqs run systemd\-cryptsetup to test if this all worked\&. The option string should contain the full
# PKCS#11 URI we have in the clipboard, it tells the tool how to decypher the encrypted LUKS key\&.
sudo systemd\-cryptsetup attach mytest /dev/sdXn /etc/encrypted\-luks\-key\&.bin \*(Aqpkcs11\-uri=pkcs11:\&...\*(Aq

# If that worked, let\*(Aqs now add the same line persistently to /etc/crypttab, for the future\&.
sudo bash \-c \*(Aqecho "mytest /dev/sdXn /etc/encrypted\-luks\-key \e\*(Aqpkcs11\-uri=pkcs11:\&...\e\*(Aq" >> /etc/crypttab\*(Aq
.fi
.if n \{\
.RE
.\}
.PP
A few notes on the above:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
We use RSA (and not ECC), since Yubikeys support PKCS#11 Decrypt() only for RSA keys
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
We use RSA2048, which is the longest key size current Yubikeys support
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
LUKS key size must be shorter than 2048bit due to RSA padding, hence we use 128 bytes
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
We use Yubikey key slot 9d, since that\*(Aqs apparently the keyslot to use for decryption purposes,
\m[blue]\fBsee documentation\fR\m[]\&\s-2\u[2]\d\s+2\&.
.RE
.SH "SEE ALSO"
.PP
\fBsystemd\fR(1),
\fBsystemd-cryptsetup@.service\fR(8),
\fBsystemd-cryptsetup-generator\fR(8),
\fBfstab\fR(5),
\fBcryptsetup\fR(8),
\fBmkswap\fR(8),
\fBmke2fs\fR(8)
.SH "NOTES"
.IP " 1." 4
RFC7512 PKCS#11 URI
.RS 4
\%https://tools.ietf.org/html/rfc7512
.RE
.IP " 2." 4
see documentation
.RS 4
\%https://developers.yubico.com/PIV/Introduction/Certificate_slots.html
.RE
