# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1998.
# Robert Luberda <robert@debian.org>, 2013, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2020-04-18 20:17+02:00\n"
"PO-Revision-Date: 2019-08-16 21:45+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "GET_KERNEL_SYMS"
msgstr "GET_KERNEL_SYMS"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid "get_kernel_syms - retrieve exported kernel and module symbols"
msgstr "get_kernel_syms - pobranie udostępnionych symboli jądra i modułów"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "B<#include E<lt>linux/module.hE<gt>>\n"
msgstr "B<#include E<lt>linux/module.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "B<int get_kernel_syms(struct kernel_sym *>I<table>B<);>\n"
msgstr "B<int get_kernel_syms(struct kernel_sym *>I<table>B<);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid ""
"I<Note>: No declaration of this system call is provided in glibc headers; "
"see NOTES."
msgstr ""
"I<Uwaga>: W nagłówkach glibc nie udostępnia się deklaracji tego wywołania "
"systemowego, zob. UWAGI."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid "B<Note>: This system call is present only in kernels before Linux 2.6."
msgstr ""
"B<Uwaga:> To wywołanie systemowe jest obecne tylko w jądrach Linuksa "
"wcześniejszych niż 2.6."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid ""
"If I<table> is NULL, B<get_kernel_syms>()  returns the number of symbols "
"available for query.  Otherwise, it fills in a table of structures:"
msgstr ""
"Jeśli I<table> jest równe NULL, B<get_kernel_syms>() zwraca liczbę symboli "
"dostępnych dla zapytania. W przeciwnym wypadku wypełniana jest tabela "
"struktur:"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid ""
"struct kernel_sym {\n"
"    unsigned long value;\n"
"    char          name[60];\n"
"};\n"
msgstr ""
"struct kernel_sym {\n"
"    unsigned long value;\n"
"    char          name[60];\n"
"};\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid ""
"The symbols are interspersed with magic symbols of the form B<#>I<module-"
"name> with the kernel having an empty name.  The value associated with a "
"symbol of this form is the address at which the module is loaded."
msgstr ""
"Symbole są przeplatane magicznymi symbolami o postaci B<#>I<nazwa-modułu>, "
"gdzie jądru odpowiada pusta nazwa.Wartością związaną z symbolem tej postaci "
"jest adres, pod którym moduł został załadowany."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid ""
"The symbols exported from each module follow their magic module tag and the "
"modules are returned in the reverse of the order in which they were loaded."
msgstr ""
"Symbole udostępniane przez poszczególne moduły następują po magicznych "
"znacznikach modułów, a same moduły są zwracane kolejności odwrotnej do "
"kolejności ich załadowania."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid ""
"On success, returns the number of symbols copied to I<table>.  On error, -1 "
"is returned and I<errno> is set appropriately."
msgstr ""
"W przypadku powodzenia zwracana jest liczba symboli skopiowanych do "
"I<table>. W razie wystąpienia błędu zwracane jest -1 i ustawiana jest "
"odpowiednia wartość zmiennej I<errno>."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid "There is only one possible error return:"
msgstr "Istnieje tylko jeden możliwy powód zwrócenia błędu:"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid "B<get_kernel_syms>()  is not supported in this version of the kernel."
msgstr "B<get_kernel_syms>() nie jest obsługiwane w tej wersji jądra."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "VERSIONS"
msgstr "WERSJE"

#.  Removed in Linux 2.5.48
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid ""
"This system call is present on Linux only up until kernel 2.4; it was "
"removed in Linux 2.6."
msgstr ""
"To wywołanie systemowe jest obecne w Linuksie tylko do wersji 2.4 jądra; "
"zostało usunięte w Linuksie 2.6."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid "B<get_kernel_syms>()  is Linux-specific."
msgstr "B<get_kernel_syms>() jest specyficzna dla Linuksa."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid ""
"This obsolete system call is not supported by glibc.  No declaration is "
"provided in glibc headers, but, through a quirk of history, glibc versions "
"before 2.23 did export an ABI for this system call.  Therefore, in order to "
"employ this system call, it was sufficient to manually declare the interface "
"in your code; alternatively, you could invoke the system call using "
"B<syscall>(2)."
msgstr ""
"To przestarzałe wywołanie systemowe nie jest obsługiwane przez glibc. W "
"nagłówkach glibc nie ma jego deklaracji, ale z powodów pewnych zaszłości "
"historycznych wersje glibc przed 2.23 eksportowały ABI dla tego wywołania "
"systemowego. Z tego powodu, aby go użyć wystarczyło manualnie zadeklarować "
"interfejs w swoim kodzie; alternatywnie można wywołać to wywołanie systemowe "
"za pomocą B<syscall>(2)."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "BUGS"
msgstr "BŁĘDY"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid ""
"There is no way to indicate the size of the buffer allocated for I<table>.  "
"If symbols have been added to the kernel since the program queried for the "
"symbol table size, memory will be corrupted."
msgstr ""
"Nie ma możliwości wskazania rozmiaru bufora przydzielonego dla I<table>.  "
"Jeśli po zapytaniu przez program o rozmiar tabeli symboli zostały dodane do "
"jądra nowe symbole, pamięć może zostać zamazana."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid "The length of exported symbol names is limited to 59 characters."
msgstr "Długość nazw udostępnianych symboli jest ograniczona do 59 znaków."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid ""
"Because of these limitations, this system call is deprecated in favor of "
"B<query_module>(2)  (which is itself nowadays deprecated in favor of other "
"interfaces described on its manual page)."
msgstr ""
"Z powodu powyższych ograniczeń, to wywołanie systemowe jest przestarzałe, "
"zamiast tego wywołanie systemowego zalecane jest B<query_module>(2) (które "
"dziś również jest uważane za przestarzałe na rzecz innych interfejsów "
"opisanych na stronie podręcznika tego wywołania systemowego)."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid ""
"B<create_module>(2), B<delete_module>(2), B<init_module>(2), "
"B<query_module>(2)"
msgstr ""
"B<create_module>(2), B<delete_module>(2), B<init_module>(2), "
"B<query_module>(2)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"This page is part of release 5.06 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 5.06 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: Plain text
#: debian-buster
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 5.04 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."
