# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1998.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2001.
# Robert Luberda <robert@debian.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2020-03-14 21:46+01:00\n"
"PO-Revision-Date: 2019-08-17 11:56+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DUMPKEYS"
msgstr "DUMPKEYS"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "1 Sep 1993"
msgstr "1 września 1993"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "dumpkeys - dump keyboard translation tables"
msgstr "dumpkeys - wypisuje tabele translacji klawiatury"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. .B \-1Vdfhiklnstv
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "B<dumpkeys> [ B<-hilfn> B<-c>I<charset> B<--help --short-info --long-info "
#| "--numeric --full-table --funcs-only --keys-only --compose-only> B<--"
#| "charset=>I<charset> ]"
msgid ""
"B<dumpkeys> [ B<-h --help -i --short-info -l -s --long-info> B<-n --numeric -"
"f --full-table -1 --separate-lines> B<-S>I<shape> B<--shape=>I<shape> B<-t --"
"funcs-only -k --keys-only -d --compose-only> B<-c>I<charset> B<--"
"charset=>I<charset> B<-v --verbose -V --version> ]"
msgstr ""
"B<dumpkeys> [ B<-hilfn> B<-c>I<charset> B<--help --short-info --long-info --"
"numeric --full-table --funcs-only --keys-only --compose-only> B<--"
"charset=>I<charset> ]"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: IX
#: archlinux fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<dumpkeys --funcs-only>"
msgid "dumpkeys command"
msgstr "B<dumpkeys --funcs-only>"

#. type: IX
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\fLdumpkeys\\fR command"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<dumpkeys> writes, to the standard output, the current contents of the "
"keyboard driver's translation tables, in the format specified by "
"B<keymaps>(5)."
msgstr ""
"B<dumpkeys> zapisuje na standardowe wyjście obecną zawartość tablic "
"translacji sterownika klawiatury. Formatem wyjściowym jest ten sam format, "
"którego używa B<keymaps>(5)."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Using the various options, the format of the output can be controlled and "
"also other information from the kernel and the programs B<dumpkeys>(1)  and "
"B<loadkeys>(1)  can be obtained."
msgstr ""
"Przy użyciu różnych opcji, można kontrolować format wyjścia, a także uzyskać "
"inne dane od jądra i programów B<dumpkeys>(1) oraz B<loadkeys>(1)."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-h --help>"
msgstr "B<-h --help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Prints the program's version number and a short usage message to the "
"program's standard error output and exits."
msgstr ""
"Wyświetla wersję programu i krótką informację o jego użyciu na standardowe "
"wyjście błędów i kończy program."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-i --short-info>"
msgstr "B<-i --short-info>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Prints some characteristics of the kernel's keyboard driver. The items shown "
"are:"
msgstr ""
"Wyświetla niektóre parametry sterownika klawiatury. Pokazywane elementy "
"włączają:"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Keycode range supported by the kernel"
msgstr ""
"Keycode range supported by kernel (zakres kodów klawiszy, obsługiwanych "
"przez jądro)"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This tells what values can be used after the B<keycode> keyword in keytable "
"files. See B<keymaps>(5)  for more information and the syntax of these files."
msgstr ""
"Mówi to, które wartości mogą być użyte po słowie kluczowym B<keycode> w "
"plikach z tablicami klawiszy (keytable). Zobacz B<keymaps>(5)  aby "
"dowiedzieć się więcej o formacie tych plików."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Number of actions bindable to a key"
msgstr ""
"Number of actions bindable to a key (liczba akcji, które można przypisać do "
"klawisza)"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This tells how many different actions a single key can output using various "
"modifier keys. If the value is 16 for example, you can define up to 16 "
"different actions to a key combined with modifiers. When the value is 16, "
"the kernel probably knows about four modifier keys, which you can press in "
"different combinations with the key to access all the bound actions."
msgstr ""
"Mówi, ile różnych akcji można przypisać do danego klawisza przy użyciu "
"różnych klawiszy modyfikujących. Jeśli wartość ta wynosi np. 16, możesz "
"zdefiniować do 16 różnych akcji na klawisz (w kombinacji z modyfikatorami).  "
"Jeśli wartość wynosi 16, to jądro prawdopodobnie zna około czterech klawiszy "
"modyfikujących, które możesz nacisnąć w różnych kombinacjach z danym "
"normalnym klawiszem."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Ranges of action codes supported by the kernel"
msgstr ""
"Ranges of action codes supported by the kernel (zakresy kodów akcji "
"obsługiwanych przez jądro)"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This item contains a list of action code ranges in hexadecimal notation.  "
"These are the values that can be used in the right hand side of a key "
"definition, ie. the I<vv>'s in a line"
msgstr ""
"To pole zawiera listę zakresów kodów akcji w notacji szesnastkowej. Są to "
"wartości, które mogą być użyte po prawej stronie definicji klawisza, np.  "
"symbole I<vv> w linii"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<keycode> I<xx> = I<vv vv vv vv>"
msgstr "B<keycode> I<xx> = I<vv vv vv vv>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"(see B<keymaps>(5)  for more information about the format of key definition "
"lines).  B<dumpkeys>(1)  and B<loadkeys>(1)  support a symbolic notation, "
"which is preferable to the numeric one, as the action codes may vary from "
"kernel to kernel while the symbolic names usually remain the same. However, "
"the list of action code ranges can be used to determine, if the kernel "
"actually supports all the symbols B<loadkeys>(1)  knows, or are there maybe "
"some actions supported by the kernel that have no symbolic name in your "
"B<loadkeys>(1)  program. To see this, you compare the range list with the "
"action symbol list, see option B<--long-info> below."
msgstr ""
"(Zobacz B<keymaps>(5)  aby poznać dalsze szczegóły formatu linii definicji "
"klawisza). B<dumpkeys>(1)  i B<loadkeys>(1) obsługują notację symboliczną, "
"która jest bardziej preferowana niż numeryczna, gdyż kody akcji mogą się "
"różnić w różnych wersjach jądra, podczas gdy nazwy symboliczne zwykle "
"pozostają jednakowe. Jednak mimo wszystko lista zakresów kodów akcji może "
"zostać użyta do sprawdzenia czy jądro rzeczywiście obsługuje wszystkie "
"symbole, które zna B<loadkeys>(1), czy też może istnieją jakieś akcje jądra, "
"które nie są nazwane w programie B<loadkeys>(1). Aby to sprawdzić, porównaj "
"listę zakresów z listą symboli akcji; zobacz niżej opcję B<--long-info>."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Number of function keys supported by kernel"
msgstr ""
"Number of function keys supported by kernel (liczba klawiszy funkcyjnych, "
"obsługiwanych przez jądro)"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This tells the number of action codes that can be used to output strings of "
"characters. These action codes are traditionally bound to the various "
"function and editing keys of the keyboard and are defined to send standard "
"escape sequences. However, you can redefine these to send common command "
"lines, email addresses or whatever you like.  Especially if the number of "
"this item is greater than the number of function and editing keys in your "
"keyboard, you may have some \"spare\" action codes that you can bind to "
"AltGr-letter combinations, for example, to send some useful strings. See "
"B<loadkeys>(1)  for more details."
msgstr ""
"Określa to liczbę kodów akcji, które mogą zostać użyte do wyprowadzania "
"ciągów znaków. Te kody akcji tradycyjnie są przypisane do różnych klawiszy "
"funkcyjnych i edytujących klawiatury i są zdefiniowane tak, aby wysyłały "
"sekwencje eskejpowe. Jednakże możesz przedefiniować je aby wysyłały często "
"używane polecenia, adresy e-mail czy jeszcze coś innego.  Szczególnie jeśli "
"liczba tych elementów jest większa niż liczba klawiszy funkcyjnych i "
"edytujących twojej klawiatury, możesz wykorzystać niektóre \"wolne\" kody "
"akcji, które można przypisać np. do kombinacji AltGr-litera aby wysyłały "
"przydatne ciągi znaków. Szczegóły można znaleźć w B<loadkeys>(1)."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Function strings"
msgstr "Function strings (funkcyjne ciągi znaków)"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "You can see you current function key definitions with the command"
msgstr "Można obejrzeć obecne definicje klawiszy z pomocą polecenia"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<dumpkeys --funcs-only>"
msgstr "B<dumpkeys --funcs-only>"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<-l --long-info>"
msgid "B<-l -s --long-info>"
msgstr "B<-l --long-info>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This option instructs B<dumpkeys> to print a long information listing. The "
"output is the same as with the B<--short-info> appended with the list of "
"action symbols supported by B<loadkeys>(1)  and B<dumpkeys>(1), along with "
"the symbols' numeric values."
msgstr ""
"Opcja ta nakazuje B<dumpkeys> wydrukowanie długiego listingu informacyjnego. "
"Wyjście jest takie samo jak z opcją B<--short-info> z dodaną listą symboli "
"obsługiwanych przez B<loadkeys>(1) i B<dumpkeys>(1), wraz z ich wartościami "
"numerycznymi."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-n --numeric>"
msgstr "B<-n --numeric>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This option causes B<dumpkeys> to by-pass the conversion of action code "
"values to symbolic notation and to print the in hexadecimal format instead."
msgstr ""
"Opcja ta powoduje, że B<dumpkeys> pomija konwersję kodów akcji na notację "
"symboliczną i drukuje je w formacie szesnastkowym."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-f --full-table>"
msgstr "B<-f --full-table>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This makes B<dumpkeys> skip all the short-hand heuristics (see "
"B<keymaps>(5))  and output the key bindings in the canonical form. First a "
"keymaps line describing the currently defined modifier combinations is "
"printed. Then for each key a row with a column for each modifier combination "
"is printed. For example, if the current keymap in use uses seven modifiers, "
"every row will have seven action code columns. This format can be useful for "
"example to programs that post-process the output of B<dumpkeys>."
msgstr ""
"Powoduje to, że B<dumpkeys> pomija wszystkie heurystyki short-hand. (zobacz "
"B<keymaps>(5)) i na wyjściu wypisuje powiązania klawiszy w formie "
"kanonicznej. Najpierw linię w formacie keymaps opisującą aktualnie "
"zdefiniowane kombinacje modyfikatorów. Następnie dla każdego klawisza "
"drukowany jest wiersz zawierający kolumny dla wszystkich kombinacji "
"modyfikatorów. Na przykład, jeśli aktualnie używana mapa klawiatury korzysta "
"z siedmiu modyfikatorów, to każdy wiersz będzie miał siedem kolumn akcji. "
"Format ten może być przydatny dla programów przetwarzających wyjście "
"B<dumpkeys>."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<-c>I<charset>B< >I< >B<--charset=>I<charset>"
msgid "B<-S>I<shape>B< >I< >B<--shape=>I<shape>"
msgstr "B<-c>I<charset>B< >I< >B<--charset=>I<charset>"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<--funcs-only>"
msgid "B<-t --funcs-only>"
msgstr "B<--funcs-only>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"When this option is given, B<dumpkeys> prints only the function key string "
"definitions. Normally B<dumpkeys> prints both the key bindings and the "
"string definitions."
msgstr ""
"Gdy podana jest ta opcja, B<dumpkeys> drukuje tylko ciągi definicyjne "
"klawiszy funkcyjnych. Normalnie B<dumpkeys> drukuje zarówno przypisania "
"klawiszy, jak i te definicje."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<--keys-only>"
msgid "B<-k --keys-only>"
msgstr "B<--keys-only>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"When this option is given, B<dumpkeys> prints only the key bindings. "
"Normally B<dumpkeys> prints both the key bindings and the string definitions."
msgstr ""
"Gdy podana jest ta opcja, B<dumpkeys> drukuje tylko powiązania klawiszy. "
"Normalnie B<dumpkeys> drukuje zarówno przypisania klawiszy, jak i ich "
"definicje."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<--compose-only>"
msgid "B<-d --compose-only>"
msgstr "B<--compose-only>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"When this option is given, B<dumpkeys> prints only the compose key "
"combinations.  This option is available only if your kernel has compose key "
"support."
msgstr ""
"Gdy podana jest ta opcja, B<dumpkeys> drukuje tylko kombinacje klawiszy "
"kompozycji (compose keys). Opcja ta jest dostępna tylko jeśli jądro "
"obsługuje te klawisze."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-c>I<charset>B< >I< >B<--charset=>I<charset>"
msgstr "B<-c>I<charset>B< >I< >B<--charset=>I<charset>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This instructs B<dumpkeys> to interpret character code values according to "
"the specified character set. This affects only the translation of character "
"code values to symbolic names. Valid values for I<charset> currently are "
"B<iso-8859-X>, Where X is a digit in 1-9.  If no I<charset> is specified, "
"B<iso-8859-1> is used as a default.  This option produces an output line "
"`charset \"iso-8859-X\"', telling loadkeys how to interpret the keymap. (For "
"example, \"division\" is 0xf7 in iso-8859-1 but 0xba in iso-8859-8.)"
msgstr ""
"Nakazuje programowi B<dumpkeys> interpretację wartości kodów znaków według "
"określonego zestawu znaków (character set, charset). Dotyczy to tylko "
"tłumaczenia kodowych wartości znaków na nazwy symboliczne. Poprawne wartości "
"I<charset> to: B<iso-8859-X>, gdzie X jest cyfrą z zakresu 1-9. Jeśli nie "
"poda się parametru I<charset>, Domyślnie zostanie użyty B<iso-8859-1>. Ta "
"opcja wypisuje także linię `charset \"iso-8859-X\"', informującą loadkeys, "
"jak zinterpretować mapę klawiszy (keymap). (Np. \"division\" to 0xf7 w "
"iso-8859-1, lecz 0xba w iso-8859-8.)"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<--version>"
msgid "B<-v --verbose>"
msgstr "B<--version>"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<--version>"
msgid "B<-V --version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid "output version information and exit"
msgid "Prints version number and exits."
msgstr "wyświetla informacje o wersji i kończy działanie"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#. type: TP
#: archlinux
#, fuzzy, no-wrap
#| msgid "B</usr/share/keymaps>"
msgid "B</usr/share/kbd/keymaps>"
msgstr "B</usr/share/keymaps>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "recommended directory for keytable files"
msgstr "zalecany katalog plików tablic klawiszowych"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<loadkeys>(1), B<keymaps>(5)"
msgstr "B<loadkeys>(1), B<kemaps>(5)"

#. type: TP
#: debian-buster debian-unstable
#, no-wrap
msgid "B</usr/share/keymaps>"
msgstr "B</usr/share/keymaps>"

#. type: TP
#: fedora-rawhide
#, fuzzy, no-wrap
#| msgid "B<-i --short-info>"
msgid "B<-1 --separate-lines>"
msgstr "B<-i --short-info>"

#. type: Plain text
#: fedora-rawhide
msgid ""
"This forces B<dumpkeys> to write one line per (modifier,keycode) pair. It "
"prefixes the word I<plain> for plain keycodes."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B</usr/share/keymaps>"
msgid "B</usr/lib/kbd/keymaps>"
msgstr "B</usr/share/keymaps>"
