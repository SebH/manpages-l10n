# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-03 18:01+02:00\n"
"PO-Revision-Date: 2015-07-05 18:06-0400\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "LDATTACH"
msgstr "LDATTACH"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "July 2014"
msgstr "Juillet 2014"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "System Administration"
msgstr "Administration Système"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "ldattach - attach a line discipline to a serial line"
msgstr "ldattach - Attacher une procédure de contrôle à une ligne série"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<ldattach> [B<-1278denoVh>] [B<-i> I<iflag>] [B<-s> I<speed>] I<ldisc "
"device>"
msgstr ""
"B<ldattach> [B<-1278denoVh>] [B<-i> I<indici>] [B<-s> I<vitesse>] I<ldisc "
"périphérique>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The B<ldattach> daemon opens the specified I<device> file (which should "
"refer to a serial device)  and attaches the line discipline I<ldisc> to it "
"for processing of the sent and/or received data.  It then goes into the "
"background keeping the device open so that the line discipline stays loaded."
msgstr ""
"Le démon B<ldattach> ouvre le fichier de I<périphérique> indiqué (qui doit "
"faire référence à un périphérique série) et lui attribue une procédure de "
"contrôle I<ldisc> pour gérer l'envoi et la réception de données. Ensuite, il "
"passe en arrière-plan tout en gardant le périphérique ouvert afin que la "
"procédure de contrôle reste chargée."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The line discipline I<ldisc> may be specified either by name or by number."
msgstr ""
"La procédure de contrôle I<ldisc> peut être indiquée par nom ou par numéro."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"In order to detach the line discipline, B<kill>(1)  the B<ldattach> process."
msgstr ""
"Afin de détacher la procédure de contrôle, tuer (avec B<kill>(1)) le "
"processus B<ldattach>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "With no arguments, B<ldattach> prints usage information."
msgstr "Sans paramètre, B<ldattach> affiche un message d'utilisation."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "LINE DISCIPLINES"
msgstr "PROCÉDURES DE CONTRÔLE"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Depending on the kernel release, the following line disciplines are "
"supported:"
msgstr "Selon la version du noyau, les procédures suivantes sont gérées :"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<TTY>(B<0>)"
msgstr "B<TTY>(B<0>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The default line discipline, providing transparent operation (raw mode)  as "
"well as the habitual terminal line editing capabilities (cooked mode)."
msgstr ""
"La procédure par défaut fournit un mode d'opération transparent (mode brut) "
"ainsi que les capacités d'édition de ligne habituelles d'une console (mode "
"« cooked »)."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<SLIP>(B<1>)"
msgstr "B<SLIP>(B<1>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Serial Line IP (SLIP) protocol processor for transmitting TCP/IP packets "
"over serial lines."
msgstr ""
"Protocole IP sur ligne série (SLIP) afin de transmettre des paquets TCP/IP "
"sur une ligne série."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<MOUSE>(B<2>)"
msgstr "B<MOUSE>(B<2>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Device driver for RS232 connected pointing devices (serial mice)."
msgstr "Pilote de périphérique de pointage connecté en RS232 (souris série)."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<PPP>(B<3>)"
msgstr "B<PPP>(B<3>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Point to Point Protocol (PPP) processor for transmitting network packets "
"over serial lines."
msgstr ""
"Protocole point à point (PPP) afin de transmettre des paquets réseaux sur "
"une ligne série."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<STRIP>(B<4>)"
msgstr "B<STRIP>(B<4>)"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<AX25>(B<5>)"
msgstr "B<AX25>(B<5>)"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<X25>(B<6>)"
msgstr "B<X25>(B<6>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Line driver for transmitting X.25 packets over asynchronous serial lines."
msgstr ""
"Pilote de ligne pour transmettre des paquets X.25 sur une ligne série "
"asynchrone."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<6PACK>(B<7>)"
msgstr "B<6PACK>(B<7>)"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<R3964>(B<9>)"
msgstr "B<R3964>(B<9>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Driver for Simatic R3964 module."
msgstr "Pilote pour le module Simatic R3964."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<IRDA>(B<11>)"
msgstr "B<IRDA>(B<11>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Linux IrDa (infrared data transmission) driver - see http://irda.sourceforge."
"net/"
msgstr ""
"Pilote de transmission de données par infrarouge pour Linux (IrDa), "
"consultez E<lt>I<http://irda.sourceforge.net/>E<gt>."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<HDLC>(B<13>)"
msgstr "B<HDLC>(B<13>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Synchronous HDLC driver."
msgstr "Pilote HDLC synchrone."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<SYNC_PPP>(B<14>)"
msgstr "B<SYNC_PPP>(B<14>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Synchronous PPP driver."
msgstr "Pilote PPP synchrone."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<HCI>(B<15>)"
msgstr "B<HCI>(B<15>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Bluetooth HCI UART driver."
msgstr "Pilote d'UART HCI Bluetooth."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<GIGASET_M101>(B<16>)"
msgstr "B<GIGASET_M101>(B<16>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Driver for Siemens Gigaset M101 serial DECT adapter."
msgstr "Pilote pour l'adaptateur série DECT Siemens Gigaset M101."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<PPS>(B<18>)"
msgstr "B<PPS>(B<18>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Driver for serial line Pulse Per Second (PPS) source."
msgstr "Pilote de ligne série de source PPS « Pulse Per Second »."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<GSM0710>(B<21>)"
msgstr "B<GSM0710>(B<21>)"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Driver for GSM 07.10 multiplexing protocol modem (CMUX)."
msgstr "Pilote pour modem à protocole de multiplexage GSM 07.10 (CMUX)."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-1>,B< --onestopbit>"
msgstr "B<-1>, B<--onestopbit>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Set the number of stop bits of the serial line to one."
msgstr "Définir le nombre de bits d'arrêt de la ligne série à 1 bit."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-2>,B< --twostopbits>"
msgstr "B<-2>, B<--twostopbits>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Set the number of stop bits of the serial line to two."
msgstr "Définir le nombre de bits d'arrêt de la ligne série à 2 bits."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-7>,B< --sevenbits>"
msgstr "B<-7>, B<--sevenbits>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Set the character size of the serial line to 7 bits."
msgstr "Définir la taille des caractères de la ligne série à 7 bits."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-8>,B< --eightbits>"
msgstr "B<-8>, B<--eightbits>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Set the character size of the serial line to 8 bits."
msgstr "Définir la taille des caractères de la ligne série à 8 bits."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-d>,B< --debug>"
msgstr "B<-d>,B< --debug>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Keep B<ldattach> in the foreground so that it can be interrupted or "
"debugged, and to print verbose messages about its progress to standard error "
"output."
msgstr ""
"Garder B<ldattach> au premier plan pour qu’il puisse être interrompu ou "
"débogué et afficher des messages à propos de son état sur la sortie d'erreur "
"standard."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-e>,B< --evenparity>"
msgstr "B<-e>, B<--evenparity>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Set the parity of the serial line to even."
msgstr "Définir la parité de la ligne série à la valeur paire."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-i>,B< --iflag >[B<->]I<value>..."
msgstr "B<-i>, B<--iflag> [B<->]I<indici>[B<,>[B<->]I<indici> ...]"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Set the specified bits in the c_iflag word of the serial line.  The given "
"I<value> may be a number or a symbolic name.  If I<value> is prefixed by a "
"minus sign, the specified bits are cleared instead.  Several comma-separated "
"values may be given in order to set and clear multiple bits."
msgstr ""
"Définir les bits indiqués dans le mot c_iflag de la ligne série. l’I<indici> "
"donné peut être un nombre ou un nom symbolique. Si I<indici> est précédé "
"d'un signe moins, les bits indiqués sont effacés à la place. Plusieurs "
"I<indici>s séparés par des virgules peuvent être données afin de définir et "
"effacer plusieurs bits."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-n>,B< --noparity>"
msgstr "B<-n>, B<--noparity>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Set the parity of the serial line to none."
msgstr "Définir la parité de la ligne série à la valeur aucune."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-o>,B< --oddparity>"
msgstr "B<-o>, B<--oddparity>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Set the parity of the serial line to odd."
msgstr "Définir la parité de la ligne série à valeur impaire."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-s>,B< --speed >I<value>"
msgstr "B<-s>, B<--speed> I<valeur>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Set the speed (the baud rate) of the serial line to the specified I<value>."
msgstr ""
"Définir la vitesse (le taux en baud) de la ligne série à la I<valeur> "
"indiquée."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-c>,B< --intro-command >I<string>"
msgstr "B<-c>, B<--intro-command> I<chaîne>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Define an intro command that is sent through the serial line before the "
"invocation of ldattach. E.g. in conjunction with line discipline GSM0710, "
"the command \\'AT+CMUX=0\\r\\' is commonly suitable to switch the modem into "
"the CMUX mode."
msgstr ""
"Définir une commande d’introduction qui est envoyée par la ligne série avant "
"l’appel de ldattach. Par exemple, en conjonction avec la procédure GSM0710, "
"la commande « AT+CMUX=0\\r » est généralement appropriée pour basculer le "
"modem en mode CMUX."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-p>,B< --pause >I<value>"
msgstr "B<-p>, B<--pause> I<valeur>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Sleep for I<value> seconds before the invocation of ldattach. Default is one "
"second."
msgstr ""
"Attendre I<valeur> secondes avant d’appeler ldattach. Une seconde par défaut."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr "B<-V>,B< --version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display version information and exit."
msgstr "Afficher le nom et la version du logiciel et quitter."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display help text and exit."
msgstr "Afficher l’aide-mémoire puis quitter."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<inputattach>(1), B<ttys>(4)"
msgstr "B<inputattach>(1), B<ttys>(4)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Tilman Schmidt (tilman@imap.cc)\n"
msgstr "Tilman Schmidt E<lt>I<tilman@imap.cc>E<gt>\n"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, fuzzy
msgid ""
"The ldattach command is part of the util-linux package and is available from "
"https://www.kernel.org/pub/linux/utils/util-linux/."
msgstr ""
"La commande B<ldattach> fait partie du paquet util-linux, elle est "
"disponible sur E<lt>I<ftp://ftp.kernel.org/pub/linux/utils/util-linux/>E<gt>."
