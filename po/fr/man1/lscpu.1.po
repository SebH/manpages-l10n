# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-07 13:27+01:00\n"
"PO-Revision-Date: 2015-07-05 18:06-0400\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "LSCPU"
msgstr "LSCPU"

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "March 2019"
msgstr "Mars 2019"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "lscpu - display information about the CPU architecture"
msgstr "lscpu - Afficher des informations sur l'architecture du processeur"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid "B<lscpu> [options]"
msgstr "B<lslocks> [I<options>]"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"B<lscpu> gathers CPU architecture information from sysfs, /proc/cpuinfo and "
"any applicable architecture-specific libraries (e.g.\\& librtas on "
"Powerpc).  The command output can be optimized for parsing or for easy "
"readability by humans.  The information includes, for example, the number of "
"CPUs, threads, cores, sockets, and Non-Uniform Memory Access (NUMA) nodes.  "
"There is also information about the CPU caches and cache sharing, family, "
"model, bogoMIPS, byte order, and stepping."
msgstr ""
"B<lscpu> collecte des renseignements sur l'architecture processeur à partir "
"de sysfs et I</proc/cpuinfo>. La sortie de la commande peut être optimisée "
"pour l’analyse ou pour faciliter la lecture. Par exemple, le nombre de "
"processeurs, de processus légers, de cœurs, de sockets et de nœuds NUMA font "
"partie des renseignements. Des renseignements sont aussi fournis sur les "
"caches et les partages de cache, la famille, le modèle, le BogoMips, le "
"boutisme et la révision."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"In virtualized environments, the CPU architecture information displayed "
"reflects the configuration of the guest operating system which is typically "
"different from the physical (host) system.  On architectures that support "
"retrieving physical topology information, B<lscpu> also displays the number "
"of physical sockets, chips, cores in the host system."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Options that result in an output table have a I<list> argument.  Use this "
"argument to customize the command output.  Specify a comma-separated list of "
"column labels to limit the output table to only the specified columns, "
"arranged in the specified order.  See B<COLUMNS> for a list of valid column "
"labels.  The column labels are not case sensitive."
msgstr ""
"Les options ayant pour résultat un tableau en sortie ont un argument "
"I<liste>. Utilisez cet argument pour personnaliser la sortie de la commande. "
"Indiquez une liste d’étiquettes de colonne séparées par des virgules pour "
"limiter le tableau en sortie à ces colonnes dans l’ordre indiqué. Consultez "
"B<COLONNES> pour une liste des étiquettes de colonne possibles. Les "
"étiquettes de colonne ne sont pas sensibles à la casse."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Not all columns are supported on all architectures.  If an unsupported "
"column is specified, B<lscpu> prints the column but does not provide any "
"data for it."
msgstr ""
"Toutes les colonnes ne sont pas prises en charge sur toutes les "
"architectures. Si une colonne non prise en charge est indiquée, B<lscpu> "
"affiche la colonne, mais ne fournit pas de données pour cette colonne."

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"The default output formatting on terminal maybe optimized for better "
"readability.  The output for non-terminals (e.g., pipes) is never affected "
"by this optimization and it is always in \"Field: data\\en\" format."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"The cache sizes are reported as summary from all CPUs.  The versions before "
"v2.34 reported per-core sizes, but this output was confusing due to "
"complicated CPUs topology and the way how caches are shared between CPUs. "
"For more details about caches see B<--cache>."
msgstr ""

#. type: SS
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COLUMNS"
msgstr "COLONNES"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"Note that topology elements (core, socket, etc.) use a sequential unique ID "
"starting from zero, but CPU logical numbers follow the kernel where there is "
"no guarantee of sequential numbering."
msgstr ""
"Remarquez que la topologie des éléments (cœur, socket, etc.) utilise des "
"identifiants uniques successifs commençant à zéro, mais les numéros logiques "
"de processeur suivent le noyau qui ne garantit pas de numérotation "
"séquentielle."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<CPU>"
msgstr "B<CPU>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The logical CPU number of a CPU as used by the Linux kernel."
msgstr ""
"Le numéro de processeur logique d’un processeur tel qu’utilisé par le noyau "
"Linux."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<CORE>"
msgstr "B<CORE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The logical core number.  A core can contain several CPUs."
msgstr ""
"Le numéro de cœur logique. Un cœur peut contenir plusieurs processeurs."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<SOCKET>"
msgstr "B<SOCKET>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The logical socket number.  A socket can contain several cores."
msgstr "Le numéro de socket logique. Une socket peut contenir plusieurs cœurs."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<BOOK>"
msgstr "B<BOOK>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The logical book number.  A book can contain several sockets."
msgstr "Le numéro de livre logique. Un livre peut contenir plusieurs sockets."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<DRAWER>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid "The logical drawer number.  A drawer can contain several books."
msgstr ""
"Le numéro de cœur logique. Un cœur peut contenir plusieurs processeurs."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<NODE>"
msgstr "B<NODE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid "The logical NUMA node number.  A node can contain several drawers."
msgstr ""
"Le numéro de nœud NUMA logique. Un nœud peut contenir plusieurs livres."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<CACHE>"
msgstr "B<CACHE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Information about how caches are shared between CPUs."
msgstr "Renseignements sur la façon de partager les caches entre processeurs."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ADDRESS>"
msgstr "B<ADDRESS>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The physical address of a CPU."
msgstr "L’adresse physique d'un processeur."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ONLINE>"
msgstr "B<ONLINE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Indicator that shows whether the Linux instance currently makes use of the "
"CPU."
msgstr ""
"Indicateur montrant si l’instance Linux utilise en ce moment le processeur."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<CONFIGURED>"
msgstr "B<CONFIGURED>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Indicator that shows if the hypervisor has allocated the CPU to the virtual "
"hardware on which the Linux instance runs.  CPUs that are configured can be "
"set online by the Linux instance.  This column contains data only if your "
"hardware system and hypervisor support dynamic CPU resource allocation."
msgstr ""
"Indicateur montrant si l’hyperviseur a alloué le processeur au matériel "
"virtuel sur lequel l’instance Linux est exécutée. Les processeurs configurés "
"peuvent être définis en ligne par l’instance Linux. Cette colonne ne "
"contient des données que si le système matériel et l’hyperviseur permettent "
"l’allocation dynamique de ressource processeur."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<POLARIZATION>"
msgstr "B<POLARIZATION>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This column contains data for Linux instances that run on virtual hardware "
"with a hypervisor that can switch the CPU dispatching mode (polarization).  "
"The polarization can be:"
msgstr ""
"Cette colonne contient des données pour les instances Linux exécutées sur du "
"matériel virtuel avec un hyperviseur pouvant modifier le mode distribué de "
"processeur (polarisation). La polarisation peut être une des suivantes."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<horizontal>"
msgstr "B<horizontal>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The workload is spread across all available CPUs."
msgstr "La charge est partagée entre tous les processeurs disponibles."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<vertical>"
msgstr "B<vertical>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The workload is concentrated on few CPUs."
msgstr "La charge est concentrée sur peu de processeurs."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"For vertical polarization, the column also shows the degree of "
"concentration, high, medium, or low.  This column contains data only if your "
"hardware system and hypervisor support CPU polarization."
msgstr ""
"Pour le mode B<vertical> de polarisation, la colonne montre aussi le degré "
"de concentration : B<high>, B<medium> ou B<low>. Cette colonne ne contient "
"des données que si le système matériel et l’hyperviseur permettent la "
"polarisation processeur."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
msgid "B<MAXMHZ>"
msgstr "B<MMHZ>"

# NOTE: s/megaherz/megahertz/ and s/cpu/CPU/
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"Maximum megahertz value for the CPU. Useful when B<lscpu> is used as "
"hardware inventory information gathering tool.  Notice that the megahertz "
"value is dynamic, and driven by CPU governor depending on current resource "
"need."
msgstr ""
"La valeur maximale en mégahertz pour le processeur. Utile quand B<lscpu> est "
"utilisé comme outil de collecte de renseignements pour l’inventaire "
"matériel. Remarquez que la valeur en mégahertz est dynamique et pilotée par "
"l’état du processeur (« CPU governor ») en fonction du besoin actuel de "
"ressources."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
msgid "B<MINMHZ>"
msgstr "B<MMHZ>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Minimum megahertz value for the CPU."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-a>,B< --all>"
msgstr "B<-a>,B< --all>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Include lines for online and offline CPUs in the output (default for B<-"
"e>).  This option may only be specified together with option B<-e> or B<-p>."
msgstr ""
"Inclure les lignes pour les processeurs en ligne et hors ligne dans la "
"sortie (par défaut pour B<-e>). Cette option ne peut être indiquée qu’avec "
"les options B<-e> ou B<-p>."

#. type: TP
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
msgid "B<-B>,B< --bytes>"
msgstr "B<-b>, B<--bytes>"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr ""
"Afficher la taille (colonne SIZE) en octet plutôt qu'en format lisible."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-b>,B< --online>"
msgstr "B<-b>, B<--online>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Limit the output to online CPUs (default for B<-p>).  This option may only "
"be specified together with option B<-e> or B<-p>."
msgstr ""
"Limiter la sortie aux processeurs en ligne (par défaut pour B<-p>). Cette "
"option ne peut être indiquée qu’avec les options B<-e> ou B<-p>."

#. type: TP
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
msgid "B<-C>,B< --caches>[=I<list>]"
msgstr "B<-p>, B<--parse>[B<=>I<liste>]"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Display details about CPU caches.  For details about available information "
"see B<--help> output."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"If the I<list> argument is omitted, all columns for which data is available "
"are included in the command output."
msgstr ""
"Si l’argument I<liste> est omis, toutes les colonnes ayant des données "
"disponibles sont incluses dans la sortie de la commande."

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"When specifying the I<list> argument, the string of option, equal sign (=), "
"and I<list> must not contain any blanks or other whitespace.  Examples: 'B<-"
"C=NAME,ONE-SIZE>' or 'B<--caches=NAME,ONE-SIZE>'."
msgstr ""
"Quand l’argument I<liste> est indiqué, la chaîne d’option, le signe égal (=) "
"et I<liste> ne doivent pas contenir d’espace. Par exemple : « B<-p=cpu,"
"node> » ou « B<--parse=cpu,node> »."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-c>,B< --offline>"
msgstr "B<-c>, B<--offline>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Limit the output to offline CPUs.  This option may only be specified "
"together with option B<-e> or B<-p>."
msgstr ""
"Limiter la sortie aux processeurs hors ligne. Cette option ne peut être "
"indiquée qu’avec les options B<-e> ou B<-p>."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-e>,B< --extended>[=I<list>]"
msgstr "B<-e>, B<--extended>[B<=>I<liste>]"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid "Display the CPU information in human-readable format."
msgstr "Afficher les renseignements sur le processeur au format lisible."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"When specifying the I<list> argument, the string of option, equal sign (=), "
"and I<list> must not contain any blanks or other whitespace.  Examples: 'B<-"
"e=cpu,node>' or 'B<--extended=cpu,node>'."
msgstr ""
"Quand l’argument I<liste> est indiqué, la chaîne d’option, le signe égal (=) "
"et I<liste> ne doivent pas contenir d’espace. Par exemple : « B<-e=cpu,"
"node> » ou « B<--extended=cpu,node> »."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Display help text and exit."
msgstr "Afficher l’aide-mémoire puis quitter."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-J>,B< --json>"
msgstr "B<-J>,B< --json>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Use JSON output format for the default summary or extended output (see B<--"
"extended>)."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-p>,B< --parse>[=I<list>]"
msgstr "B<-p>, B<--parse>[B<=>I<liste>]"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Optimize the command output for easy parsing."
msgstr "Optimiser la sortie de la commande pour faciliter l’analyse."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"If the I<list> argument is omitted, the command output is compatible with "
"earlier versions of B<lscpu>.  In this compatible format, two commas are "
"used to separate CPU cache columns.  If no CPU caches are identified the "
"cache column is omitted."
msgstr ""
"En absence d'argument I<liste>, la sortie de la commande est compatible avec "
"les versions précédentes de B<lscpu>. Dans ce format compatible, deux "
"virgules séparent les colonnes de cache de processeur. Si aucun cache de "
"processeur n'est identifié, la colonne est omise."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"If the I<list> argument is used, cache columns are separated with a colon "
"(:)."
msgstr ""
"Si l'argument I<liste> est utilisé, les colonnes de cache sont séparées par "
"des deux-points (:)."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"When specifying the I<list> argument, the string of option, equal sign (=), "
"and I<list> must not contain any blanks or other whitespace.  Examples: 'B<-"
"p=cpu,node>' or 'B<--parse=cpu,node>'."
msgstr ""
"Quand l’argument I<liste> est indiqué, la chaîne d’option, le signe égal (=) "
"et I<liste> ne doivent pas contenir d’espace. Par exemple : « B<-p=cpu,"
"node> » ou « B<--parse=cpu,node> »."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-s>,B< --sysroot >I<directory>"
msgstr "B<-s>, B<--sysroot> I<répertoire>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Gather CPU data for a Linux instance other than the instance from which the "
"B<lscpu> command is issued.  The specified I<directory> is the system root "
"of the Linux instance to be inspected."
msgstr ""
"Collecter les données de processeur pour une autre instance Linux que celle "
"utilisée pour la commande B<lscpu>. Le I<répertoire> indiqué est la racine "
"du système de l’instance Linux à inspecter."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-x>,B< --hex>"
msgstr "B<-x>,B< --hex>"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"Use hexadecimal masks for CPU sets (for example \"ff\").  The default is to "
"print the sets in list format (for example 0,1).  Note that before version "
"2.30 the mask has been printed with 0x prefix."
msgstr ""
"Utiliser des masques hexadécimaux pour les ensembles de processeurs (par "
"exemple 0x3). Par défaut, l'affichage est au format liste (par exemple 0,1)."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
msgid "B<-y>,B< --physical>"
msgstr "B<-p>, B<--paths>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Display physical IDs for all columns with topology elements (core, socket, "
"etc.).  Other than logical IDs, which are assigned by B<lscpu>, physical IDs "
"are platform-specific values that are provided by the kernel. Physical IDs "
"are not necessarily unique and they might not be arranged sequentially.  If "
"the kernel could not retrieve a physical ID for an element B<lscpu> prints "
"the dash (-) character."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid "The CPU logical numbers are not affected by this option."
msgstr ""
"Le numéro de processeur logique d’un processeur tel qu’utilisé par le noyau "
"Linux."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr "B<-V>,B< --version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Display version information and exit."
msgstr "Afficher le nom et la version du logiciel et quitter."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--output-all>"
msgstr "B<--output-all>"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Output all available columns.  This option must be combined with either B<--"
"extended>, B<--parse> or B<--caches>."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The basic overview of CPU family, model, etc. is always based on the first "
"CPU only."
msgstr ""
"La vue d'ensemble de base de la famille, du modèle, etc., de processeur "
"n’est toujours relative qu’au premier processeur."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Sometimes in Xen Dom0 the kernel reports wrong data."
msgstr "Quelques fois sous Xen Dom0, les résultats du noyau sont incorrects."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "On virtual hardware the number of cores per socket, etc. can be wrong."
msgstr ""
"Sur matériel virtuel, le nombre de cœurs par socket, etc., peut être faux."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"Cai Qian E<lt>qcai@redhat.comE<gt>\n"
"Karel Zak E<lt>kzak@redhat.comE<gt>\n"
"Heiko Carstens E<lt>heiko.carstens@de.ibm.comE<gt>\n"
msgstr ""
"Cai Qian E<lt>I<qcai@redhat.com>E<gt>\n"
"Karel Zak E<lt>I<kzak@redhat.com>E<gt>\n"
"Heiko Carstens E<lt>I<heiko.carstens@de.ibm.com>E<gt>\n"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<chcpu>(8)"
msgstr "B<chcpu>(8)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"The lscpu command is part of the util-linux package and is available from "
"https://www.kernel.org/pub/linux/utils/util-linux/."
msgstr ""
"La commande B<lscpu> fait partie du paquet util-linux, elle est disponible "
"sur E<lt>I<ftp://ftp.kernel.org/pub/linux/utils/util-linux/>E<gt>."

#. type: TH
#: debian-buster
#, no-wrap
msgid "November 2015"
msgstr "Novembre 2015"

# s/TYPE/type
#. type: Plain text
#: debian-buster
#, fuzzy
msgid ""
"B<lscpu> [B<-a>|B<-b>|B<-c>|B<-J>] [B<-x>] [B<-y>] [B<-s> I<directory>] [B<-"
"e>[=I<list>]|B<-p>[=I<list>]]"
msgstr ""
"B<lscpu> [B<-a>|B<-b>|B<-c>] [B<-x>] [B<-s> I<répertoire>] [B<-"
"e>[B<=>I<liste>]|B<-p>[B<=>I<liste>]]"

#. type: Plain text
#: debian-buster
msgid "B<lscpu> B<-h>|B<-V>"
msgstr "B<lscpu> B<-h>|B<-V>"

#. type: Plain text
#: debian-buster
msgid ""
"Use hexadecimal masks for CPU sets (for example 0x3).  The default is to "
"print the sets in list format (for example 0,1)."
msgstr ""
"Utiliser des masques hexadécimaux pour les ensembles de processeurs (par "
"exemple 0x3). Par défaut, l'affichage est au format liste (par exemple 0,1)."

#. type: Plain text
#: debian-buster
msgid ""
"Output all available columns.  This option must be combined with either B<--"
"extended> or B<--parse>."
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"The default output formatting on terminal maybe optimized for better "
"readability.  The output for non-terminals (e.g. pipes) is never affected by "
"this optimization and it is always in \"Field: data\\en\" format."
msgstr ""
