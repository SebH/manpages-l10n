# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Nicolas François <nicolas.francois@centraliens.net>, 2008-2010.
# Bastien Scher <bastien0705@gmail.com>, 2011-2013.
# David Prévot <david@tilapin.org>, 2012-2014.
msgid ""
msgstr ""
"Project-Id-Version: coreutils manpages\n"
"POT-Creation-Date: 2020-04-04 20:34+02:00\n"
"PO-Revision-Date: 2014-10-30 13:28-0400\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bits\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SHA224SUM"
msgstr "SHA224SUM"

#. type: TH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "March 2020"
msgstr "Mars 2020"

#. type: TH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "sha224sum - compute and check SHA224 message digest"
msgstr "sha224sum - Calculer et vérifier le sommaire d'un message SHA224"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid "B<sha224sum> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<sha224sum> [I<\\,OPTION\\/>] ... [I<\\,FICHIER\\/>] ..."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid "Print or check SHA224 (224-bit) checksums."
msgstr ""
"Afficher ou vérifier des sommes de contrôle SHA224 (224\\ bits). L'entrée "
"standard est lue quand I<FICHIER> est omis ou quand I<FICHIER> vaut « - »."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""
"L'entrée standard est lue quand I<FICHIER> est omis ou quand I<FICHIER> vaut "
"« - »."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-b>, B<--binary>"
msgstr "B<-b>, B<--binary>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "read in binary mode"
msgstr "lire en mode binaire"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-c>, B<--check>"
msgstr "B<-c>, B<--check>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "read SHA224 sums from the FILEs and check them"
msgstr "lire les sommes de contrôle SHA224 des I<FICHIER>s et les vérifier"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--tag>"
msgstr "B<--tag>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "create a BSD-style checksum"
msgstr "créer une somme de contrôle de type BSD"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-t>, B<--text>"
msgstr "B<-t>, B<--text>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "read in text mode (default)"
msgstr "lire en mode texte (par défaut)"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-z>, B<--zero>"
msgstr "B<-z>, B<--zero>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"end each output line with NUL, not newline, and disable file name escaping"
msgstr ""
"terminer chaque ligne produite par un octet NULL final plutôt que par un "
"changement de ligne et désactiver l'échappement des noms de fichier"

#. type: SS
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "The following five options are useful only when verifying checksums:"
msgstr "Les cinq options suivantes ne sont utiles que lors de la vérification des sommes de contrôle\\ :"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--ignore-missing>"
msgstr "B<--ignore-missing>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "don't fail or report status for missing files"
msgstr "ne pas échouer ou signaler l’état pour les fichiers manquants"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--quiet>"
msgstr "B<--quiet>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "don't print OK for each successfully verified file"
msgstr "ne pas afficher OK pour chaque fichier vérifié avec succès"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--status>"
msgstr "B<--status>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "don't output anything, status code shows success"
msgstr "ne rien afficher, le code d'état montre le succès"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--strict>"
msgstr "B<--strict>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "exit non-zero for improperly formatted checksum lines"
msgstr ""
"quitter avec un code de retour différent de zéro en cas de formatage "
"incorrect des lignes de sommes de contrôle"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-w>, B<--warn>"
msgstr "B<-w>, B<--warn>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "warn about improperly formatted checksum lines"
msgstr "avertir lorsque les lignes de sommes de contrôle sont mal formatées"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "display this help and exit"
msgstr "afficher l'aide-mémoire et quitter"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "output version information and exit"
msgstr "afficher le nom et la version du logiciel et quitter"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"The sums are computed as described in RFC 3874.  When checking, the input "
"should be a former output of this program.  The default mode is to print a "
"line with checksum, a space, a character indicating input mode ('*' for "
"binary, \\&' ' for text or where binary is insignificant), and name for each "
"FILE."
msgstr ""
"Les sommes sont calculées selon la méthode décrite dans la RFC\\ 3874. Lors "
"de la vérification, l'entrée doit être une sortie précédente du programme. "
"Le mode par défaut affiche la somme de contrôle, un caractère indiquant le "
"type («\\ *\\ » pour binaire, une espace «\\ \\ \\ » pour texte) et le nom "
"de chaque I<FICHIER>."

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Note: There is no difference between binary mode and text mode on GNU "
"systems."
msgstr ""
"Note : il n'y a pas de différences entre le mode binaire et le mode texte "
"sur les systèmes GNU."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Written by Ulrich Drepper, Scott Miller, and David Madore."
msgstr "Écrit par Ulrich Drepper, Scott Miller et David Madore."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Aide en ligne de GNU coreutils : E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Signaler toute erreur de traduction à E<lt>https://translationproject.org/"
"team/fr.htmlE<gt>"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ce programme est un logiciel libre. Vous pouvez le modifier et le "
"redistribuer. Il n'y a AUCUNE GARANTIE dans la mesure autorisée par la loi."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/sha224sumE<gt>"
msgstr ""
"Documentation complète : E<lt>I<https://www.gnu.org/software/coreutils/"
"sha224sum>E<gt>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) sha2 utilities\\(aq"
msgstr ""
"aussi disponible localement à l’aide de la commande : info \\(aq(coreutils) "
"sha2 utilities\\(aq"

#. type: TH
#: debian-buster
#, no-wrap
msgid "February 2019"
msgstr "Février 2019"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr "GNU coreutils 8.30"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Report sha224sum translation bugs to E<lt>https://translationproject.org/"
"team/E<gt>"
msgstr ""
"Signaler toute erreur de traduction de sha224sum à E<lt>I<https://"
"translationproject.org/team/fr>E<gt>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2018 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/"
"sha224sumE<gt>"
msgstr ""
"Documentation complète : E<lt>I<https://www.gnu.org/software/coreutils/"
"sha224sum>E<gt>"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr "Août 2019"

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "B<--color>"
msgstr "B<--color>"

#. type: Plain text
#: mageia-cauldron
msgid "print with colors"
msgstr "afficher en couleurs"
