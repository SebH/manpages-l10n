# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-07 13:26+01:00\n"
"PO-Revision-Date: 2020-03-27 12:16+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "FLOCK"
msgstr "FLOCK"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "July 2014"
msgstr "Juillet 2014"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "flock - manage locks from shell scripts"
msgstr "flock - Gérer des verrous depuis des scripts d'interpréteur"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<flock> [options] I<file>|I<directory command >[I<arguments>]"
msgstr "B<flock> [I<options>] I<fichier>|I<répertoire commande> [I<arguments>]"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<flock> [options] I<file>|I<directory> B<-c>I< command>"
msgstr "B<flock> [I<options>] I<fichier>|I<répertoire> B<-c> I<commande>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<flock> [options]I< number>"
msgstr "B<flock> [I<options>] I<numéro>"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This utility manages B<flock>(2)  locks from within shell scripts or from "
"the command line."
msgstr ""
"Cet utilitaire gère les verrous B<flock>(2) à partir de scripts "
"d'interpréteur ou de la ligne de commande."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The first and second of the above forms wrap the lock around the execution "
"of a I<command>, in a manner similar to B<su>(1)  or B<newgrp>(1).  They "
"lock a specified I<file> or I<directory>, which is created (assuming "
"appropriate permissions) if it does not already exist.  By default, if the "
"lock cannot be immediately acquired, B<flock> waits until the lock is "
"available."
msgstr ""
"Les première et deuxième formes précédentes enveloppent l'exécution d'une "
"I<commande> par un verrou, de façon similaire à B<su>(1) ou B<newgrp>(1). "
"Elles verrouillent soit le I<fichier>, soit le I<répertoire> indiqué, qui "
"est créé (en supposant que vous ayez les droits adéquats) s'il n'existe pas "
"déjà. Par défaut, si le verrou ne peut pas être obtenu immédiatement, "
"B<flock> attend jusqu’à ce que le verrou soit disponible."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The third form uses an open file by its file descriptor I<number>.  See the "
"examples below for how that can be used."
msgstr ""
"La troisième forme utilise un fichier ouvert par son I<numéro> de "
"descripteur de fichier. Consultez les exemples suivants montrant comment "
"l’utiliser."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-c>,B< --command >I<command>"
msgstr "B<-c>, B<--command> I<commande>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Pass a single I<command>, without arguments, to the shell with B<-c>."
msgstr ""
"Passer une seule I<commande>, sans argument, à l’interpréteur de commandes "
"avec B<-c>."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-E>,B< --conflict-exit-code >I<number>"
msgstr "B<-E>, B<--conflict-exit-code> I<numéro>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The exit code used when the B<-n> option is in use, and the conflicting lock "
"exists, or the B<-w> option is in use, and the timeout is reached.  The "
"default value is B<1>."
msgstr ""
"Le code de retour utilisé quand l’option B<-n> est utilisée et que le verrou "
"en conflit existe, ou que l’option B<-w> est utilisée et que le délai est "
"atteint. La valeur par défaut est B<1>."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-F>,B< --no-fork>"
msgstr "B<-F>,B< --no-fork >"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Do not fork before executing I<command>.  Upon execution the flock process "
"is replaced by I<command> which continues to hold the lock. This option is "
"incompatible with B<--close> as there would otherwise be nothing left to "
"hold the lock."
msgstr ""
"Ne pas forker avant d'exécuter I<commande>. Pendant l'exécution, le "
"processus B<flock> est remplacé par I<commande> qui garde le verrou. Cette "
"option est incompatible avec B<--close>, sans quoi plus rien ne conserverait "
"le verrou."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-e>,B< -x>,B< --exclusive>"
msgstr "B<-e>, B<-x>, B<--exclusive>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Obtain an exclusive lock, sometimes called a write lock.  This is the "
"default."
msgstr ""
"Obtenir un verrou exclusif, parfois appelé verrou en écriture. C'est "
"l'option par défaut."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-n>,B< --nb>,B< --nonblock>"
msgstr "B<-n>, B<--nb>, B<--nonblock>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Fail rather than wait if the lock cannot be immediately acquired.  See the "
"B<-E> option for the exit code used."
msgstr ""
"Échouer plutôt qu’attendre si le verrou ne peut pas être obtenu "
"immédiatement. Consultez l’option B<-E> pour le code de retour utilisé."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-o>,B< --close>"
msgstr "B<-o>, B<--close>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Close the file descriptor on which the lock is held before executing "
"I<command>.  This is useful if I<command> spawns a child process which "
"should not be holding the lock."
msgstr ""
"Fermer le descripteur de fichier sur lequel le verrou est maintenu avant "
"l'exécution de I<commande>. C'est utile si I<commande> lance un processus "
"enfant qui ne devrait pas détenir le verrou."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-s>,B< --shared>"
msgstr "B<-s>, B<--shared>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Obtain a shared lock, sometimes called a read lock."
msgstr "Obtenir un verrou partagé, parfois appelé verrou en lecture."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-u>,B< --unlock>"
msgstr "B<-u>, B<--unlock>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Drop a lock.  This is usually not required, since a lock is automatically "
"dropped when the file is closed.  However, it may be required in special "
"cases, for example if the enclosed command group may have forked a "
"background process which should not be holding the lock."
msgstr ""
"Supprimer un verrou. Ce n'est généralement pas nécessaire, puisqu'un verrou "
"est automatiquement supprimé lorsque le fichier est fermé. Cependant, il "
"peut être nécessaire dans des cas particuliers, par exemple si le groupe de "
"commandes inclus a engendré un processus en arrière-plan qui ne devrait pas "
"détenir le verrou."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-w>,B< --wait>,B< --timeout >I<seconds>"
msgstr "B<-w>, B<--wait>, B<--timeout> I<délai>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Fail if the lock cannot be acquired within I<seconds>.  Decimal fractional "
"values are allowed.  See the B<-E> option for the exit code used. The zero "
"number of I<seconds> is interpreted as B<--nonblock>."
msgstr ""
"Échouer si le verrou ne peut pas être obtenu en I<délai> secondes. Les "
"valeurs en fractions décimales sont permises. Consultez l’option B<-E> pour "
"le code de retour utilisé. Un I<délai> nul est interprété comme B<--"
"nonblock>."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--verbose>"
msgstr "B<--verbose>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Report how long it took to acquire the lock, or why the lock could not be "
"obtained."
msgstr ""
"Renvoyer la durée d'acquisition du verrou ou la raison pour laquelle il n'a "
"pas pu être obtenu."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr "B<-V>,B< --version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Display version information and exit."
msgstr "Afficher le nom et la version du logiciel et quitter."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Display help text and exit."
msgstr "Afficher l’aide-mémoire puis quitter."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "shell1E<gt> flock /tmp -c cat"
msgstr "shell1E<gt> flock /tmp -c cat"

#. type: TQ
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "shell2E<gt> flock -w .007 /tmp -c echo; /bin/echo $?"
msgstr "shell2E<gt> flock -w .007 /tmp -c echo; /bin/echo $?"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Set exclusive lock to directory /tmp and the second command will fail."
msgstr ""
"Définir un verrou exclusif sur le répertoire I</tmp> et la seconde commande "
"échouera."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "shell1E<gt> flock -s /tmp -c cat"
msgstr "shell1E<gt> flock -s /tmp -c cat"

#. type: TQ
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "shell2E<gt> flock -s -w .007 /tmp -c echo; /bin/echo $?"
msgstr "shell2E<gt> flock -s -w .007 /tmp -c echo; /bin/echo $?"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Set shared lock to directory /tmp and the second command will not fail.  "
"Notice that attempting to get exclusive lock with second command would fail."
msgstr ""
"Définir un verrou partagé sur le répertoire I</tmp> et la seconde commande "
"n’échouera pas. Remarquez que la tentative d’obtenir un verrou exclusif avec "
"la seconde commande aurait échoué."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "shellE<gt> flock -x local-lock-file echo 'a b c'"
msgstr "shellE<gt> flock -x fichier-verrou-local echo 'a b c'"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Grab the exclusive lock \"local-lock-file\" before running echo with 'a b c'."
msgstr ""
"Récupérer le verrou exclusif « fichier-verrou-local » avant d’exécuter "
"B<echo> 'a b c'."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "("
msgstr "("

#. type: TQ
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "  flock -n 9 || exit 1"
msgstr "  flock -n 9 || exit 1"

#. type: TQ
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "  # ... commands executed under lock ..."
msgstr "  # ... commandes exécutées sous un verrou ..."

#. type: TQ
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ") 9E<gt>/var/lock/mylockfile"
msgstr ") 9E<gt>/var/lock/monfichierverrou"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The form is convenient inside shell scripts.  The mode used to open the file "
"doesn't matter to B<flock>; using I<E<gt>> or I<E<gt>E<gt>> allows the "
"lockfile to be created if it does not already exist, however, write "
"permission is required.  Using I<E<lt>> requires that the file already "
"exists but only read permission is required."
msgstr ""
"Cette forme est pratique dans les scripts d’interpréteur de commandes. Le "
"mode utilisé pour ouvrir le fichier n'est pas important pour B<flock> ; "
"utiliser B<E<gt>> ou B<E<gt>E<gt>> permet de créer le fichier de "
"verrouillage s'il n'existe pas déjà, cependant, le droit d'écriture est "
"nécessaire. En utilisant B<E<lt>>, le fichier doit déjà exister, mais seul "
"le droit de lecture est nécessaire."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "[ \"${FLOCKER}\" != \"$0\" ] && exec env FLOCKER=\"$0\" flock -en \"$0\" \"$0\" \"$@\" || :"
msgstr "[ \"${FLOCKER}\" != \"$0\" ] && exec env FLOCKER=\"$0\" flock -en \"$0\" \"$0\" \"$@\" || :"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This is useful boilerplate code for shell scripts.  Put it at the top of the "
"shell script you want to lock and it'll automatically lock itself on the "
"first run.  If the env var $FLOCKER is not set to the shell script that is "
"being run, then execute flock and grab an exclusive non-blocking lock (using "
"the script itself as the lock file) before re-execing itself with the right "
"arguments.  It also sets the FLOCKER env var to the right value so it "
"doesn't run again."
msgstr ""
"C’est un code passe-partout utile pour les scripts d’interpréteur. Placez-le "
"au début du script d’interpréteur que vous voulez verrouiller et il se "
"verrouillera lui-même automatiquement lors de la première exécution. Si la "
"variable d’environnement B<$FLOCKER> n’est pas définie pour le script "
"d’interpréteur en cours d’exécution, alors B<flock> est exécuté et un verrou "
"non bloquant exclusif est récupéré (en utilisant le script lui-même comme "
"fichier de verrouillage) avant que le script ne s’exécute de nouveau avec "
"les bons arguments. La variable d’environnement B<FLOCKER> est aussi définie "
"à la bonne valeur pour que le script ne s’exécute pas de nouveau."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "EXIT STATUS"
msgstr "CODE DE RETOUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The command uses B<sysexits.h> return values for everything, except when "
"using either of the options B<-n> or B<-w> which report a failure to acquire "
"the lock with a return value given by the B<-E> option, or 1 by default."
msgstr ""
"La commande utilise les valeurs de retour de B<sysexits.h> pour tout, sauf "
"quand les options B<-n> ou B<-w> sont utilisées. Elles signalent un échec "
"d’obtention du verrou avec une valeur de retour donnée par l’option B<-E> ou "
"B<1> par défaut."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"When using the I<command> variant, and executing the child worked, then the "
"exit status is that of the child command."
msgstr ""
"En utilisant la variante I<commande> et si l’exécution de l’enfant a "
"fonctionné, le code de retour est celui de la commande enfant."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "E<.UR hpa@zytor.com> H. Peter Anvin E<.UE>"
msgstr "E<.UR hpa@zytor.com> H. Peter Anvin E<.UE>"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Copyright \\(co 2003-2006 H. Peter Anvin."
msgstr "Copyright \\(co 2003-2006 H. Peter Anvin."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This is free software; see the source for copying conditions.  There is NO "
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
msgstr ""
"C'est un logiciel libre ; consultez les sources pour les conditions de "
"copie. Il n'y a AUCUNE garantie ; même pas de VALEUR MARCHANDE ou "
"d'ADÉQUATION À UNE UTILISATION PARTICULIÈRE."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<flock>(2)"
msgstr "B<flock>(2)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The flock command is part of the util-linux package and is available from E<."
"UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> Linux "
"Kernel Archive E<.UE .>"
msgstr ""
"La commande B<flock> fait partie du paquet util-linux, elle est disponible "
"sur E<.UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> "
"l’archive du noyau LinuxE<.UE .>"
