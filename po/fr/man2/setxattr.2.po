# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2020-04-18 20:20+02:00\n"
"PO-Revision-Date: 2018-09-10 20:55+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <https://translate.holcroft.fr/projects/man-pages-fr/"
"filesystem/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SETXATTR"
msgstr "SETXATTR"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2019-08-02"
msgstr "2 août 2019"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "setxattr, lsetxattr, fsetxattr - set an extended attribute value"
msgstr "setxattr, lsetxattr, fsetxattr - Définir les attributs étendus"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/xattr.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/xattr.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"B<int setxattr(const char\\ *>I<path>B<, const char\\ *>I<name>B<,>\n"
"B<              const void\\ *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
"B<int lsetxattr(const char\\ *>I<path>B<, const char\\ *>I<name>B<,>\n"
"B<              const void\\ *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
"B<int fsetxattr(int >I<fd>B<, const char\\ *>I<name>B<,>\n"
"B<              const void\\ *>I<value>B<, size_t >I<size>B<, int >I<flags>B<);>\n"
msgstr ""
"B<int setxattr (const char\\ *>I<chemin>B<, const char\\ *>I<nom>B<,>\n"
"B<              const void\\ *>I<valeur>B<, size_t >I<taille>B<, int >I<options>B<);>\n"
"B<int lsetxattr (const char\\ *>I<chemin>B<, const char\\ *>I<nom>B<,>\n"
"B<               const void\\ *>I<valeur>B<, size_t >I<taille>B<, int >I<options>B<);>\n"
"B<int fsetxattr (int >I<descripteur>B<, const char\\ *>I<nom>B<,>\n"
"B<               const void\\ *>I<valeur>B<, size_t >I<taille>B<, int >I<options>B<);>\n"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Extended attributes are I<name>:I<value> pairs associated with inodes "
"(files, directories, symbolic links, etc.).  They are extensions to the "
"normal attributes which are associated with all inodes in the system (i.e., "
"the B<stat>(2)  data).  A complete overview of extended attributes concepts "
"can be found in B<xattr>(7)."
msgstr ""
"Les attributs étendus sont des paires I<nom>:I<valeur> associées aux inœuds "
"(fichiers, répertoires, liens symboliques, etc.). Ce sont des extensions des "
"attributs normaux qui sont associés avec tous les inœuds du système (les "
"informations renvoyées par B<stat>(2). Une description complète des concepts "
"d'attributs étendus est disponible dans B<xattr>(7)."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"B<setxattr>()  sets the I<value> of the extended attribute identified by "
"I<name> and associated with the given I<path> in the filesystem.  The "
"I<size> argument specifies the size (in bytes) of I<value>; a zero-length "
"value is permitted."
msgstr ""
"B<setxattr>()  écrit la I<valeur> dans l'attribut étendu identifié par le "
"I<nom> et associé avec le I<chemin> dans le système de fichiers La I<taille> "
"de la I<valeur> doit être indiquée"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<lsetxattr>()  is identical to B<setxattr>(), except in the case of a "
"symbolic link, where the extended attribute is set on the link itself, not "
"the file that it refers to."
msgstr ""
"B<lsetxattr>()  est identique à B<setxattr>(), sauf dans le cas d'un lien "
"symbolique, où il traite le lien lui-même et non le fichier qu'il vise."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<fsetxattr>()  is identical to B<setxattr>(), only the extended attribute "
"is set on the open file referred to by I<fd> (as returned by B<open>(2))  in "
"place of I<path>."
msgstr ""
"B<fsetxattr>()  est identique à B<setxattr>(), seulement on écrit l'attribut "
"du fichier ouvert indiqué par le I<descripteur> (renvoyé par B<open>(2))  "
"plutôt que par un I<chemin>."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"An extended attribute name is a null-terminated string.  The I<name> "
"includes a namespace prefix; there may be several, disjoint namespaces "
"associated with an individual inode.  The I<value> of an extended attribute "
"is a chunk of arbitrary textual or binary data of specified length."
msgstr ""
"Le I<nom> d'un attribut étendu est une simple chaîne terminée par un octet "
"nul. Le nom inclut un préfixe d'espace de noms - il peut y avoir plusieurs "
"espaces de noms disjoints associés avec un inœud donné. La valeur d'un "
"attribut étendu est un bloc de données littérales ou binaires d'une longueur "
"donnée."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"By default (i.e., I<flags> is zero), the extended attribute will be created "
"if it does not exist, or the value will be replaced if the attribute already "
"exists.  To modify these semantics, one of the following values can be "
"specified in I<flags>:"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
msgid "B<XATTR_CREATE>"
msgstr "B<SPU_CREATE_GANG>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Perform a pure create, which fails if the named attribute exists already."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<XATTR_REPLACE>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Perform a pure replace operation, which fails if the named attribute does "
"not already exist."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"On success, zero is returned.  On failure, -1 is returned and I<errno> is "
"set appropriately."
msgstr ""
"S'ils réussissent, ces appels renvoient zéro. En cas d'échec, ils renvoient "
"-1 et I<errno> contient le code d'erreur."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EDQUOT>"
msgstr "B<EDQUOT>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Disk quota limits meant that there is insufficient space remaining to store "
"the extended attribute."
msgstr ""
"Les limites de quota de disque indiquaient un espace insuffisant pour "
"stocker l'attribut étendu."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<XATTR_CREATE> was specified, and the attribute exists already."
msgstr "B<XATTR_CREATE> a été indiqué et l'attribut existe déjà."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENODATA>"
msgstr "B<ENODATA>"

#.  .RB ( ENOATTR
#.  is defined to be a synonym for
#.  .BR ENODATA
#.  in
#.  .IR <attr/attributes.h> .)
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid "B<XATTR_REPLACE> was specified, and the attribute does not exist."
msgstr "B<XATTR_CREATE> a été indiqué et l'attribut existe déjà."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "There is insufficient space remaining to store the extended attribute."
msgstr "Il n'y a pas suffisamment d'espace pour stocker l'attribut étendu."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENOTSUP>"
msgstr "B<ENOTSUP>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid "The namespace prefix of I<name> is not valid."
msgstr "Un élément du préfixe du chemin I<pathname> n'est pas un répertoire."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"Extended attributes are not supported by the filesystem, or are disabled,"
msgstr ""
"Les attributs étendus ne sont pas pris en charge par le système de fichiers "
"ou sont désactivés."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The file is marked immutable or append-only.  (See B<ioctl_iflags>(2).)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "In addition, the errors documented in B<stat>(2)  can also occur."
msgstr ""
"De plus, les erreurs documentées dans B<stat>(2) peuvent aussi survenir."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The size of I<name> or I<value> exceeds a filesystem-specific limit."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"These system calls have been available on Linux since kernel 2.4; glibc "
"support is provided since version 2.3."
msgstr ""
"Ces appels système sont disponibles sous Linux depuis le noyau\\ 2.4\\ ; la "
"glibc les prend en charge depuis la version\\ 2.3."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#.  .SH AUTHORS
#.  Andreas Gruenbacher,
#.  .RI < a.gruenbacher@computer.org >
#.  and the SGI XFS development team,
#.  .RI < linux-xfs@oss.sgi.com >.
#.  Please send any bug reports or comments to these addresses.
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "These system calls are Linux-specific."
msgstr "Ces appels système sont spécifiques à Linux."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"B<getfattr>(1), B<setfattr>(1), B<getxattr>(2), B<listxattr>(2), B<open>(2), "
"B<removexattr>(2), B<stat>(2), B<symlink>(7), B<xattr>(7)"
msgstr ""
"B<getfattr>(1), B<setfattr>(1), B<getxattr>(2), B<listxattr>(2), B<open>(2), "
"B<removexattr>(2), B<stat>(2), B<attr>(5), B<symlink>(7)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.06 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.06 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page, peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: debian-buster
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
