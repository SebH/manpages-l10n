# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2020-04-18 20:19+02:00\n"
"PO-Revision-Date: 2018-11-12 17:11+0100\n"
"Last-Translator: Thomas Vincent <tvincent@debian.org>\n"
"Language-Team: French <https://translate.holcroft.fr/projects/man-pages-fr/"
"semaphore/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 1.8.11\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEMOP"
msgstr "SEMOP"

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2020-04-11"
msgstr "11 avril 2020"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "semop, semtimedop - System V semaphore operations"
msgstr "semop, semtimedop - Opérations sur les sémaphores System V"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/ipc.hE<gt>>\n"
"B<#include E<lt>sys/sem.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/ipc.hE<gt>>\n"
"B<#include E<lt>sys/sem.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<int semop(int >I<semid>B<, struct sembuf *>I<sops>B<, size_t >I<nsops>B<);>\n"
msgstr "B<int semop(int >I<semid>B<, struct sembuf *>I<sops>B<, size_t >I<nsops>B<);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"B<int semtimedop(int >I<semid>B<, struct sembuf *>I<sops>B<, size_t >I<nsops>B<,>\n"
"B<               const struct timespec *>I<timeout>B<);>\n"
msgstr ""
"B<int semtimedop(int >I<semid>B<, struct sembuf *>I<sops>B<, size_t >I<nsops>B<,>\n"
"B<               const struct timespec *>I<timeout>B<);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<semtimedop>(): _GNU_SOURCE"
msgstr "B<semtimedop>() : _GNU_SOURCE"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Each semaphore in a System\\ V semaphore set has the following associated "
"values:"
msgstr ""
"Chaque sémaphore dans un ensemble de sémaphores System V se voit associer "
"les valeurs suivantes\\ :"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"unsigned short  semval;   /* semaphore value */\n"
"unsigned short  semzcnt;  /* # waiting for zero */\n"
"unsigned short  semncnt;  /* # waiting for increase */\n"
"pid_t           sempid;   /* PID of process that last\n"
msgstr ""
"unsigned short  semval;   /* Valeur du sémaphore   */\n"
"unsigned short  semzcnt;  /* # Attente pour zéro   */\n"
"unsigned short  semncnt;  /* # Attente d'incrément */\n"
"pid_t           sempid;   /* PID du dernier processus agissant */\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<semop>()  performs operations on selected semaphores in the set indicated "
"by I<semid>.  Each of the I<nsops> elements in the array pointed to by "
"I<sops> is a structure that specifies an operation to be performed on a "
"single semaphore.  The elements of this structure are of type I<struct "
"sembuf>, containing the following members:"
msgstr ""
"La fonction B<semop>() effectue des opérations sur les membres de l'ensemble "
"de sémaphores identifié par I<semid>. Chacun des I<nsops> éléments dans le "
"tableau pointé par I<sops> indique une opération à effectuer sur un "
"sémaphore en utilisant une structure I<struct sembuf> contenant les membres "
"suivants\\ :"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"unsigned short sem_num;  /* semaphore number */\n"
"short          sem_op;   /* semaphore operation */\n"
"short          sem_flg;  /* operation flags */\n"
msgstr ""
"unsigned short sem_num;  /* Numéro du sémaphore        */\n"
"short          sem_op;   /* Opération sur le sémaphore */\n"
"short          sem_flg;  /* Options pour l'opération   */\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Flags recognized in I<sem_flg> are B<IPC_NOWAIT> and B<SEM_UNDO>.  If an "
"operation specifies B<SEM_UNDO>, it will be automatically undone when the "
"process terminates."
msgstr ""
"Les options possibles pour I<sem_flg> sont B<IPC_NOWAIT> et B<SEM_UNDO>. Si "
"une opération indique l'option B<SEM_UNDO>, elle sera annulée lorsque le "
"processus se terminera."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The set of operations contained in I<sops> is performed in I<array order>, "
"and I<atomically>, that is, the operations are performed either as a "
"complete unit, or not at all.  The behavior of the system call if not all "
"operations can be performed immediately depends on the presence of the "
"B<IPC_NOWAIT> flag in the individual I<sem_flg> fields, as noted below."
msgstr ""
"L'ensemble des opérations contenues dans I<sops> est effectué I<dans "
"l'ordre> et I<atomiquement>. Les opérations sont toutes réalisées en même "
"temps, et seulement si elles peuvent toutes être effectuées. Le comportement "
"de l'appel système, si toutes les opérations ne sont pas réalisables, dépend "
"de la présence de l'attribut B<IPC_NOWAIT> dans les champs I<sem_flg> "
"décrits plus bas."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Each operation is performed on the I<sem_num>-th semaphore of the semaphore "
"set, where the first semaphore of the set is numbered 0.  There are three "
"types of operation, distinguished by the value of I<sem_op>."
msgstr ""
"Chaque opération est effectuée sur le I<sem_num>-ième sémaphore de "
"l'ensemble. Le premier sémaphore est le numéro 0. Pour chaque sémaphore, "
"l'opération est l'une des trois décrites ci-dessous."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"If I<sem_op> is a positive integer, the operation adds this value to the "
"semaphore value (I<semval>).  Furthermore, if B<SEM_UNDO> is specified for "
"this operation, the system subtracts the value I<sem_op> from the semaphore "
"adjustment (I<semadj>)  value for this semaphore.  This operation can always "
"proceed\\(emit never forces a thread to wait.  The calling process must have "
"alter permission on the semaphore set."
msgstr ""
"Si l'argument I<sem_op> est un entier positif, la fonction ajoute cette "
"valeur à I<semval>. De plus si B<SEM_UNDO> est demandé, le système soustrait "
"la valeur I<sem_op> à la valeur de mise à jour de sémaphore (I<semadj>). "
"Cette opération n'est jamais bloquante. Le processus appelant doit avoir "
"l'autorisation de modification sur le jeu de sémaphores."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"If I<sem_op> is zero, the process must have read permission on the semaphore "
"set.  This is a \"wait-for-zero\" operation: if I<semval> is zero, the "
"operation can immediately proceed.  Otherwise, if B<IPC_NOWAIT> is specified "
"in I<sem_flg>, B<semop>()  fails with I<errno> set to B<EAGAIN> (and none of "
"the operations in I<sops> is performed).  Otherwise, I<semzcnt> (the count "
"of threads waiting until this semaphore's value becomes zero)  is "
"incremented by one and the thread sleeps until one of the following occurs:"
msgstr ""
"Si I<sem_op> vaut zéro le processus doit avoir l'autorisation de lecture sur "
"l'ensemble de sémaphores. Le processus attend que I<semval> soit nul\\ : si "
"I<semval> vaut zéro, l'appel système continue immédiatement. Sinon, si l'on "
"a réclamé B<IPC_NOWAIT> dans I<sem_flg>, l'appel système B<semop>() échoue "
"et I<errno> contient le code d'erreur B<EAGAIN> (et aucune des opérations de "
"I<sops> n'est réalisée). Sinon, I<semzcnt> (le décompte de threads en "
"attente jusqu'à ce que cette valeur de sémaphore devienne zéro) est "
"incrémenté d'un et le thread s'endort jusqu'à ce que l'un des événements "
"suivants se produise\\ :"

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"I<semval> becomes 0, at which time the value of I<semzcnt> is decremented."
msgstr "I<semval> devient égal à 0, alors I<semzcnt> est décrémenté."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The semaphore set is removed: B<semop>()  fails, with I<errno> set to "
"B<EIDRM>."
msgstr ""
"Le jeu de sémaphores est supprimé. L'appel système échoue et I<errno> "
"contient le code d'erreur B<EIDRM>."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The calling thread catches a signal: the value of I<semzcnt> is decremented "
"and B<semop>()  fails, with I<errno> set to B<EINTR>."
msgstr ""
"Le thread reçoit un signal à intercepter, la valeur de I<semzcnt> est "
"décrémentée et l'appel système échoue avec I<errno> contenant le code "
"d'erreur B<EINTR>."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"If I<sem_op> is less than zero, the process must have alter permission on "
"the semaphore set.  If I<semval> is greater than or equal to the absolute "
"value of I<sem_op>, the operation can proceed immediately: the absolute "
"value of I<sem_op> is subtracted from I<semval>, and, if B<SEM_UNDO> is "
"specified for this operation, the system adds the absolute value of "
"I<sem_op> to the semaphore adjustment (I<semadj>)  value for this "
"semaphore.  If the absolute value of I<sem_op> is greater than I<semval>, "
"and B<IPC_NOWAIT> is specified in I<sem_flg>, B<semop>()  fails, with "
"I<errno> set to B<EAGAIN> (and none of the operations in I<sops> is "
"performed).  Otherwise, I<semncnt> (the counter of threads waiting for this "
"semaphore's value to increase)  is incremented by one and the thread sleeps "
"until one of the following occurs:"
msgstr ""
"Si I<sem_op> est inférieur à zéro, le processus appelant doit avoir "
"l'autorisation de modification sur le jeu de sémaphores. Si I<semval> est "
"supérieur ou égal à la valeur absolue de I<sem_op>, la valeur absolue de "
"I<sem_op> est soustraite de I<semval>, et si B<SEM_UNDO> est indiqué, le "
"système ajoute la valeur absolue de I<sem_op> à la valeur de mise à jour de "
"sémaphore (I<semadj>) pour ce sémaphore. Si la valeur absolue de I<sem_op> "
"est plus grande que I<semval>, et si l'on a réclamé B<IPC_NOWAIT> dans "
"I<sem_flg>, l'appel système échoue et I<errno> contient le code d'erreur "
"B<EAGAIN> (et aucune des opérations de I<sops> n'est réalisée). Sinon, "
"I<semncnt> (le décompte de threads en attente jusqu'à ce que cette valeur de "
"sémaphore soit incrémenté) est incrémenté de un et le thread s'endort "
"jusqu'à ce que l'un des événements suivants se produise\\ :"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"I<semval> becomes greater than or equal to the absolute value of I<sem_op>: "
"the operation now proceeds, as described above."
msgstr ""
"I<semval> devient supérieur ou égal à la valeur absolue de I<sem_op> : "
"l'opération est effectuée comme décrit ci-dessus."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The semaphore set is removed from the system: B<semop>()  fails, with "
"I<errno> set to B<EIDRM>."
msgstr ""
"Le jeu de sémaphores est supprimé. L'appel système échoue et I<errno> "
"contient le code d'erreur B<EIDRM>."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The calling thread catches a signal: the value of I<semncnt> is decremented "
"and B<semop>()  fails, with I<errno> set to B<EINTR>."
msgstr ""
"Le thread reçoit un signal à intercepter, la valeur de I<semncnt> est "
"décrémentée et l'appel système échoue avec I<errno> contenant le code "
"d'erreur B<EINTR>."

#.  and
#.  .I sem_ctime
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"On successful completion, the I<sempid> value for each semaphore specified "
"in the array pointed to by I<sops> is set to the caller's process ID.  In "
"addition, the I<sem_otime> is set to the current time."
msgstr ""
"En cas de succès, le membre I<sempid> de chacun des sémaphores indiqués dans "
"le tableau pointé par I<sops> est rempli avec le PID de l'appelant. Enfin "
"I<sem_otime> est défini à l'heure actuelle."

#. type: SS
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "semtimedop()"
msgstr "semtimedop()"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<semtimedop>()  behaves identically to B<semop>()  except that in those "
"cases where the calling thread would sleep, the duration of that sleep is "
"limited by the amount of elapsed time specified by the I<timespec> structure "
"whose address is passed in the I<timeout> argument.  (This sleep interval "
"will be rounded up to the system clock granularity, and kernel scheduling "
"delays mean that the interval may overrun by a small amount.)  If the "
"specified time limit has been reached, B<semtimedop>()  fails with I<errno> "
"set to B<EAGAIN> (and none of the operations in I<sops> is performed).  If "
"the I<timeout> argument is NULL, then B<semtimedop>()  behaves exactly like "
"B<semop>()."
msgstr ""
"La fonction B<semtimedop>() se comporte comme B<semop>() sauf que dans le "
"cas où le thread doit dormir, la durée maximale du sommeil est limitée par "
"la valeur spécifiée dans la structure I<timespec> dont l'adresse est "
"transmise dans le paramètre I<timeout> (cet intervalle de sommeil sera "
"arrondi à la granularité de l'horloge système, et les délais "
"d'ordonnancement du noyau font que cette valeur peut être légèrement "
"dépassée). Si la limite indiquée a été atteinte, l'appel système échoue avec "
"I<errno> contenant B<EAGAIN> (et aucune opération de I<sops> n'est "
"réalisée). Si le paramètre I<timeout> est NULL, alors B<semtimedop>() se "
"comporte exactement comme B<semop>()."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Note that if B<semtimedop>()  is interrupted by a signal, causing the call "
"to fail with the error B<EINTR>, the contents of I<timeout> are left "
"unchanged."
msgstr ""
"Si B<semtimedop>() est interrompu par un signal, causant l'échec de l'appel "
"avec l'erreur B<EINTR>, les valeurs contenues dans I<timeout> restent "
"inchangées."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"If successful, B<semop>()  and B<semtimedop>()  return 0; otherwise they "
"return -1 with I<errno> indicating the error."
msgstr ""
"En cas de réussite, B<semop>() et B<semtimedop>() renvoient 0. Sinon ils "
"renvoient -1 et I<errno> contient le code d'erreur."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "On failure, I<errno> is set to one of the following:"
msgstr "En cas d'erreur, I<errno> prend l'une des valeurs suivantes\\ :"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<E2BIG>"
msgstr "B<E2BIG>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The argument I<nsops> is greater than B<SEMOPM>, the maximum number of "
"operations allowed per system call."
msgstr ""
"l'argument I<nsops> est supérieur à B<SEMOPM>, le nombre maximal "
"d'opérations par appel système."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The calling process does not have the permissions required to perform the "
"specified semaphore operations, and does not have the B<CAP_IPC_OWNER> "
"capability in the user namespace that governs its IPC namespace."
msgstr ""
"Le processus appelant n'a pas les permissions nécessaires pour effectuer les "
"opérations sur les sémaphores spécifiés et n'a pas la capacité "
"B<CAP_IPC_OWNER> dans l'espace de noms utilisateur qui régit son espace de "
"noms IPC."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"An operation could not proceed immediately and either B<IPC_NOWAIT> was "
"specified in I<sem_flg> or the time limit specified in I<timeout> expired."
msgstr ""
"Une opération ne pouvait pas être effectuée immédiatement et B<IPC_NOWAIT> a "
"été indiqué dans l'argument I<sem_flg>, ou la durée limite indiquée dans "
"I<timeout> a expiré."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"An address specified in either the I<sops> or the I<timeout> argument isn't "
"accessible."
msgstr ""
"I<sops> ou I<timeout> pointent en dehors de l'espace d'adressage accessible."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EFBIG>"
msgstr "B<EFBIG>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"For some operation the value of I<sem_num> is less than 0 or greater than or "
"equal to the number of semaphores in the set."
msgstr ""
"La valeur de I<sem_num> est inférieure à 0 ou supérieure ou égale au nombre "
"de sémaphores dans l'ensemble."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EIDRM>"
msgstr "B<EIDRM>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The semaphore set was removed."
msgstr "Le jeu de sémaphores a été supprimé."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"While blocked in this system call, the thread caught a signal; see "
"B<signal>(7)."
msgstr "Un signal a été reçu pendant l'attente ; consultez B<signal>(7)."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The semaphore set doesn't exist, or I<semid> is less than zero, or I<nsops> "
"has a nonpositive value."
msgstr ""
"L'ensemble de sémaphores n'existe pas ou I<semid> est inférieur à zéro, ou "
"I<nsops> n'est pas strictement positive."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The I<sem_flg> of some operation specified B<SEM_UNDO> and the system does "
"not have enough memory to allocate the undo structure."
msgstr ""
"L'argument I<sem_flg> de certaines opérations demande B<SEM_UNDO> et le "
"système n'a pas assez de mémoire pour allouer les structures nécessaires."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"For some operation I<sem_op+semval> is greater than B<SEMVMX>, the "
"implementation dependent maximum value for I<semval>."
msgstr ""
"I<sem_op+semval> est supérieur à B<SEMVMX> (la valeur maximale de I<semval> "
"autorisée par l'implémentation) pour l'une des opérations."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

# NOTE: rétroporté ?
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<semtimedop>()  first appeared in Linux 2.5.52, and was subsequently "
"backported into kernel 2.4.22.  Glibc support for B<semtimedop>()  first "
"appeared in version 2.3.3."
msgstr ""
"B<semtimedop>() est apparu pour la première fois dans Linux 2.5.52, puis a "
"été rétroporté au noyau 2.4.22. La gestion de B<semtimedop>() dans la glibc "
"date de la version 2.3.3."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#.  SVr4 documents additional error conditions EINVAL, EFBIG, ENOSPC.
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, SVr4."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#.  Like Linux, the FreeBSD man pages still document
#.  the inclusion of these header files.
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The inclusion of I<E<lt>sys/types.hE<gt>> and I<E<lt>sys/ipc.hE<gt>> isn't "
"required on Linux or by any version of POSIX.  However, some old "
"implementations required the inclusion of these header files, and the SVID "
"also documented their inclusion.  Applications intended to be portable to "
"such old systems may need to include these header files."
msgstr ""
"L'inclusion de I<E<lt>sys/types.hE<gt>> et I<E<lt>sys/ipc.hE<gt>> n'est pas "
"nécessaire sous Linux et n'est exigée par aucune version de POSIX. "
"Cependant, certaines implémentations anciennes nécessitent l'inclusion de "
"ces fichiers d'en-tête, et le SVID documente aussi leur inclusion. Les "
"applications ayant pour but d'être portables pourraient inclure ces fichiers "
"d'en-tête."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The I<sem_undo> structures of a process aren't inherited by the child "
"produced by B<fork>(2), but they are inherited across an B<execve>(2)  "
"system call."
msgstr ""
"Les structures I<sem_undo> d'un processus ne sont pas héritées par ses "
"enfants lors d'un B<fork>(2), mais elles le sont lors d'un appel système "
"B<execve>(2)."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<semop>()  is never automatically restarted after being interrupted by a "
"signal handler, regardless of the setting of the B<SA_RESTART> flag when "
"establishing a signal handler."
msgstr ""
"B<semop>() n'est jamais relancé automatiquement après avoir été interrompu "
"par un gestionnaire de signal quelque soit l'attribut B<SA_RESTART> durant "
"l'installation du gestionnaire."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"A semaphore adjustment (I<semadj>)  value is a per-process, per-semaphore "
"integer that is the negated sum of all operations performed on a semaphore "
"specifying the B<SEM_UNDO> flag.  Each process has a list of I<semadj> values"
"\\(emone value for each semaphore on which it has operated using "
"B<SEM_UNDO>.  When a process terminates, each of its per-semaphore I<semadj> "
"values is added to the corresponding semaphore, thus undoing the effect of "
"that process's operations on the semaphore (but see BUGS below).  When a "
"semaphore's value is directly set using the B<SETVAL> or B<SETALL> request "
"to B<semctl>(2), the corresponding I<semadj> values in all processes are "
"cleared.  The B<clone>(2)  B<CLONE_SYSVSEM> flag allows more than one "
"process to share a I<semadj> list; see B<clone>(2)  for details."
msgstr ""
"Une valeur de mise à jour de sémaphore (I<semadj>) est un entier propre à un "
"processus et sémaphore, qui représente le compte négatif des opérations "
"utilisant l'attribut B<SEM_UNDO>. Chaque processus dispose d'une liste de "
"valeurs I<semadj> \\(em une valeur pour chaque sémaphore où B<SEM_UNDO> a "
"été utilisé. Quand un processus se termine, chacune des valeurs I<semadj>  "
"de ses sémaphores est ajoutée au sémaphore correspondant, annulant ainsi "
"l'effet des opérations du processus sur le sémaphore (voir la section BOGUES "
"ci-dessous). Quand la valeur d'un sémaphore est définie directement par une "
"requête B<SETVAL> ou B<SETALL> de B<semctl>(2), la valeur I<semadj> "
"correspondante est effacée dans tous les processus. L'option "
"B<CLONE_SYSVSEM> de B<clone>(2) permet à plusieurs processus de partager une "
"liste de I<semadj>. Consultez B<clone>(2) pour plus d'informations."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The I<semval>, I<sempid>, I<semzcnt>, and I<semnct> values for a semaphore "
"can all be retrieved using appropriate B<semctl>(2)  calls."
msgstr ""
"Les valeurs I<semval>, I<sempid>, I<semzcnt>, et I<semnct> pour un sémaphore "
"peuvent être retrouvées avec des appels B<semctl>(2) spécifiques."

#. type: SS
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Semaphore limits"
msgstr "Limites des sémaphores"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The following limits on semaphore set resources affect the B<semop>()  call:"
msgstr ""
"Les limites suivantes de ressources concernent l'appel système B<semop>()\\ :"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<SEMOPM>"
msgstr "B<SEMOPM>"

#.  commit e843e7d2c88b7db107a86bd2c7145dc715c058f4
#.  This /proc file is not available in Linux 2.2 and earlier -- MTK
#.  See comment in Linux 3.19 source file include/uapi/linux/sem.h
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Maximum number of operations allowed for one B<semop>()  call.  Before Linux "
"3.19, the default value for this limit was 32.  Since Linux 3.19, the "
"default value is 500.  On Linux, this limit can be read and modified via the "
"third field of I</proc/sys/kernel/sem>.  I<Note>: this limit should not be "
"raised above 1000, because of the risk of that B<semop>()  fails due to "
"kernel memory fragmentation when allocating memory to copy the I<sops> array."
msgstr ""
"Nombre maximal d'opérations pour un appel à B<semop>. Avant Linux 3.19, la "
"valeur par défaut pour cette limite était de 32. Depuis Linux 3.19, cette "
"valeur est de 500. Sous Linux, cette limite peut être lue et modifiée via le "
"troisième champ du fichier I</proc/sys/kernel/sem>. I<Note> : cette limite "
"ne devrait pas être augmentée au-delà de 1000 à cause du risque que "
"B<semop>() échoue en raison de la fragmentation de la mémoire du noyau "
"pendant l'allocation de mémoire pour copier le tableau I<sops>."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<SEMVMX>"
msgstr "B<SEMVMX>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Maximum allowable value for I<semval>: implementation dependent (32767)."
msgstr ""
"Valeur maximale pour I<semval>\\ : dépendante de l'implémentation (32767)."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The implementation has no intrinsic limits for the adjust on exit maximum "
"value (B<SEMAEM>), the system wide maximum number of undo structures "
"(B<SEMMNU>)  and the per-process maximum number of undo entries system "
"parameters."
msgstr ""
"L'implémentation n'a pas de limites intrinsèques pour la valeur maximale "
"d'effacement en sortie (B<SEMAEM>), le nombre de structures d'annulation sur "
"le système (B<SEMMNU>), et le nombre maximal de structures d'annulation pour "
"un processus."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"When a process terminates, its set of associated I<semadj> structures is "
"used to undo the effect of all of the semaphore operations it performed with "
"the B<SEM_UNDO> flag.  This raises a difficulty: if one (or more) of these "
"semaphore adjustments would result in an attempt to decrease a semaphore's "
"value below zero, what should an implementation do? One possible approach "
"would be to block until all the semaphore adjustments could be performed.  "
"This is however undesirable since it could force process termination to "
"block for arbitrarily long periods.  Another possibility is that such "
"semaphore adjustments could be ignored altogether (somewhat analogously to "
"failing when B<IPC_NOWAIT> is specified for a semaphore operation).  Linux "
"adopts a third approach: decreasing the semaphore value as far as possible "
"(i.e., to zero) and allowing process termination to proceed immediately."
msgstr ""
"Quand un processus se termine, l'ensemble des structures I<semadj> qui lui "
"sont associées servent à annuler les effets de toutes les opérations sur les "
"sémaphores réalisées avec l'attribut B<SEM_UNDO>. Cela pose un problème\\ : "
"si l'une (ou plusieurs) des modifications sur les sémaphores demande une "
"descente du compteur d'un sémaphore au-dessous de zéro, que doit faire "
"l'implémentation\\ ? Une approche possible consiste à bloquer jusqu'à ce que "
"la modification du sémaphore soit possible. C'est néanmoins peu désirable "
"car la terminaison du processus peut alors bloquer pendant une période "
"arbitrairement longue. Une autre possibilité est d'ignorer la modification "
"du sémaphore (comme un échec lorsque B<IPC_NOWAIT> est spécifié durant une "
"opération). Linux adopte une troisième approche\\ : décroître la valeur du "
"sémaphore autant que possible (jusqu'à zéro) et permettre au processus de se "
"terminer immédiatement."

#.  The bug report:
#.  http://marc.theaimsgroup.com/?l=linux-kernel&m=110260821123863&w=2
#.  the fix:
#.  http://marc.theaimsgroup.com/?l=linux-kernel&m=110261701025794&w=2
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"In kernels 2.6.x, x E<lt>= 10, there is a bug that in some circumstances "
"prevents a thread that is waiting for a semaphore value to become zero from "
"being woken up when the value does actually become zero.  This bug is fixed "
"in kernel 2.6.11."
msgstr ""
"Dans les noyaux 2.6.x (x E<lt>= 10) un bogue peut, dans certaines "
"circonstances, empêcher un thread, attendant que la valeur d'un sémaphore "
"s'annule, d'être réveillé quand cette valeur atteint 0. Ce bogue est corrigé "
"dans le noyau 2.6.11."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The following code segment uses B<semop>()  to atomically wait for the value "
"of semaphore 0 to become zero, and then increment the semaphore value by one."
msgstr ""
"Le bout de code suivant utilise B<semop>() pour attendre de façon atomique "
"que la valeur du sémaphore 0 vaille zéro, puis incrémente la valeur du "
"sémaphore de un."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"struct sembuf sops[2];\n"
"int semid;\n"
msgstr ""
"struct sembuf sops[2];\n"
"int semid;\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "/* Code to set I<semid> omitted */\n"
msgstr "/* Le code pour configurer I<semid> est omis */\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"sops[0].sem_num = 0;        /* Operate on semaphore 0 */\n"
"sops[0].sem_op = 0;         /* Wait for value to equal 0 */\n"
"sops[0].sem_flg = 0;\n"
msgstr ""
"sops[0].sem_num = 0;        /* Agir sur le sémaphore 0 */\n"
"sops[0].sem_op = 0;         /* Attendre que la valeur soit égale à 0 */\n"
"sops[0].sem_flg = 0;\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"sops[1].sem_num = 0;        /* Operate on semaphore 0 */\n"
"sops[1].sem_op = 1;         /* Increment value by one */\n"
"sops[1].sem_flg = 0;\n"
msgstr ""
"sops[1].sem_num = 0;        /* Agir sur le sémaphore 0 */\n"
"sops[1].sem_op = 1;         /* Incrémenter la valeur de un */\n"
"sops[1].sem_flg = 0;\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"if (semop(semid, sops, 2) == -1) {\n"
"    perror(\"semop\");\n"
"    exit(EXIT_FAILURE);\n"
"}\n"
msgstr ""
"if (semop(semid, sops, 2) == -1) {\n"
"    perror(\"semop\");\n"
"    exit(EXIT_FAILURE);\n"
"}\n"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"A further example of the use of B<semop>()  can be found in B<shmop>(2)."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"B<clone>(2), B<semctl>(2), B<semget>(2), B<sigaction>(2), "
"B<capabilities>(7), B<sem_overview>(7), B<sysvipc>(7), B<time>(7)"
msgstr ""
"B<clone>(2), B<semctl>(2), B<semget>(2), B<sigaction>(2), "
"B<capabilities>(7), B<sem_overview>(7), B<svipc>(7), B<time>(7)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.06 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.06 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page, peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: debian-buster
#, no-wrap
msgid "2019-08-02"
msgstr "2 août 2019"

#. type: Plain text
#: debian-buster
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
