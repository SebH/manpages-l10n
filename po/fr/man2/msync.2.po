# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999,2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2012-2014.
# Denis Barbier <barbier@debian.org>, 2006,2010.
# David Prévot <david@tilapin.org>, 2010-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2020-04-18 20:18+02:00\n"
"PO-Revision-Date: 2018-09-10 20:55+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <https://translate.holcroft.fr/projects/man-pages-fr/"
"memory/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "MSYNC"
msgstr "MSYNC"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "msync - synchronize a file with a memory map"
msgstr "msync - Synchroniser un fichier et une projection en mémoire"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<#include E<lt>sys/mman.hE<gt>>"
msgstr "B<#include E<lt>sys/mman.hE<gt>>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<int msync(void *>I<addr>B<, size_t >I<length>B<, int >I<flags>B<);>"
msgstr "B<int msync(void *>I<addr>B<, size_t >I<length>B<, int >I<flags>B<);>"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"B<msync>()  flushes changes made to the in-core copy of a file that was "
"mapped into memory using B<mmap>(2)  back to the filesystem.  Without use of "
"this call, there is no guarantee that changes are written back before "
"B<munmap>(2)  is called.  To be more precise, the part of the file that "
"corresponds to the memory area starting at I<addr> and having length "
"I<length> is updated."
msgstr ""
"B<msync>() écrit sur le système de fichiers les modifications qui ont été "
"effectuées sur la copie d'un fichier qui est projeté en mémoire par "
"B<mmap>(2). Si l'on n'utilise pas cette fonction, rien ne garantit que les "
"changements soient écrits avant la suppression de la projection par "
"B<munmap>(2). Pour être plus précis, la portion du fichier correspondant à "
"la zone mémoire commençant en I<addr> et ayant une longueur de I<length> est "
"mise à jour."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The I<flags> argument should specify exactly one of B<MS_ASYNC> and "
"B<MS_SYNC>, and may additionally include the B<MS_INVALIDATE> bit.  These "
"bits have the following meanings:"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
msgid "B<MS_ASYNC>"
msgstr "MSYNC"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Specifies that an update be scheduled, but the call returns immediately."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
msgid "B<MS_SYNC>"
msgstr "MSYNC"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Requests an update and waits for it to complete."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
msgid "B<MS_INVALIDATE>"
msgstr "B<MAP_PRIVATE>"

#.  Since Linux 2.4, this seems to be a no-op (other than the
#.  EBUSY check for VM_LOCKED).
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Asks to invalidate other mappings of the same file (so that they can be "
"updated with the fresh values just written)."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"En cas de succès, zéro est renvoyé. En cas d'erreur, B<-1> est renvoyé et "
"I<errno> reçoit une valeur adéquate."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EBUSY>"
msgstr "B<EBUSY>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<MS_INVALIDATE> was specified in I<flags>, and a memory lock exists for the "
"specified address range."
msgstr ""
"B<MS_INVALIDATE> était positionné dans I<flags>, mais un verrouillage "
"mémoire existe pour l'intervalle indiqué."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"I<addr> is not a multiple of PAGESIZE; or any bit other than B<MS_ASYNC> | "
"B<MS_INVALIDATE> | B<MS_SYNC> is set in I<flags>; or both B<MS_SYNC> and "
"B<MS_ASYNC> are set in I<flags>."
msgstr ""
"I<addr> n'est pas aligné sur une frontière de page (un multiple de "
"PAGESIZE), ou d'autres bits que B<MS_ASYNC> | B<MS_INVALIDATE> | B<MS_SYNC> "
"sont à 1 dans I<flags>. Ou bien B<MS_SYNC> et B<MS_ASYNC> sont tous les deux "
"spécifiés dans I<flags>."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The indicated memory (or part of it) was not mapped."
msgstr ""
"La zone mémoire indiquée (ou une partie de cette zone) n'est pas une "
"projection."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This call was introduced in Linux 1.3.21, and then used B<EFAULT> instead of "
"B<ENOMEM>.  In Linux 2.4.19, this was changed to the POSIX value B<ENOMEM>."
msgstr ""
"Cet appel a été introduit dans Linux 1.3.21, et utilisait B<EFAULT> au lieu "
"de B<ENOMEM>. Dans Linux 2.4.19, ceci a été remplacé par la valeur POSIX "
"B<ENOMEM>."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#.  POSIX.1-2001: It shall be defined to -1 or 0 or 200112L.
#.  -1: unavailable, 0: ask using sysconf().
#.  glibc defines them to 1.
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"On POSIX systems on which B<msync>()  is available, both "
"B<_POSIX_MAPPED_FILES> and B<_POSIX_SYNCHRONIZED_IO> are defined in "
"I<E<lt>unistd.hE<gt>> to a value greater than 0.  (See also B<sysconf>(3).)"
msgstr ""
"Sur les systèmes conformes à la spécification POSIX sur lequel B<msync>() "
"est disponible, les constantes symboliques B<_POSIX_MAPPED_FILES> et "
"B<_POSIX_SYNCHRONIZED_IO> sont définies dans I<E<lt>unistd.hE<gt>> comme "
"étant des valeurs supérieures à 0. (Consultez aussi B<sysconf>(3).)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#.  commit 204ec841fbea3e5138168edbc3a76d46747cc987
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"According to POSIX, either B<MS_SYNC> or B<MS_ASYNC> must be specified in "
"I<flags>, and indeed failure to include one of these flags will cause "
"B<msync>()  to fail on some systems.  However, Linux permits a call to "
"B<msync>()  that specifies neither of these flags, with semantics that are "
"(currently) equivalent to specifying B<MS_ASYNC>.  (Since Linux 2.6.19, "
"B<MS_ASYNC> is in fact a no-op, since the kernel properly tracks dirty pages "
"and flushes them to storage as necessary.)  Notwithstanding the Linux "
"behavior, portable, future-proof applications should ensure that they "
"specify either B<MS_SYNC> or B<MS_ASYNC> in I<flags>."
msgstr ""
"POSIX spécifie qu'au moins B<MS_SYNC> ou B<MS_ASYNC> doit être indiqué dans "
"I<flags>, et que si aucun des deux n'est indiqué B<msync>() échouera sur "
"certains systèmes. Cependant, Linux autorise d'appeler B<msync>() sans "
"indiquer aucun des deux attributs, avec une sémantique actuellement "
"équivalente à indiquer B<MS_ASYNC>. (Depuis Linux 2.6.19, B<MS_ASYNC> est "
"sans effet, puisque le noyau suit correctement les pages modifiées et les "
"transfère vers le disque si besoin.) Malgré le comportement de Linux, les "
"applications portables et destinées à être pérennes doivent indiquer au "
"moins B<MS_SYNC> ou B<MS_ASYNC> dans I<flags>."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<mmap>(2)"
msgstr "B<mmap>(2)"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid "B.O. Gallmeister, POSIX.4, O'Reilly, pp. 128\\(en129 and 389\\(en391."
msgstr "B.O. Gallmeister, POSIX.4, O'Reilly, pp. 128\\(en129 et 389\\(en391."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.06 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.06 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page, peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: debian-buster
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
