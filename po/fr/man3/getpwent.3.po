# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012, 2013.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2020-03-15 09:36+01:00\n"
"PO-Revision-Date: 2018-09-10 20:55+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <https://translate.holcroft.fr/projects/man-pages-fr/"
"pwdgrp/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GETPWENT"
msgstr "GETPWENT"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "getpwent, setpwent, endpwent - get password file entry"
msgstr ""
"getpwent, setpwent, endpwent - Lire un enregistrement du fichier des mots de "
"passe"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>pwd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>pwd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<struct passwd *getpwent(void);>\n"
msgstr "B<struct passwd *getpwent(void);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<void setpwent(void);>\n"
msgstr "B<void setpwent(void);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<void endpwent(void);>\n"
msgstr "B<void endpwent(void);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<getpwent>(), B<setpwent>(), B<endpwent>():"
msgstr "B<getpwent>(), B<setpwent>(), B<endpwent>() :"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The B<getpwent>()  function returns a pointer to a structure containing the "
"broken-out fields of a record from the password database (e.g., the local "
"password file I</etc/passwd>, NIS, and LDAP).  The first time B<getpwent>()  "
"is called, it returns the first entry; thereafter, it returns successive "
"entries."
msgstr ""
"La fonction B<getpwent>() renvoie un pointeur sur une structure contenant "
"les divers champs d'un enregistrement de la base de données des mots de "
"passe (par exemple, le fichier de mots de passe local I</etc/passwd>, NIS ou "
"LDAP). Au premier appel, B<getpwent>() renvoie le premier enregistrement, "
"puis les enregistrements suivants lors des appels suivants."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The B<setpwent>()  function rewinds to the beginning of the password "
"database."
msgstr ""
"La fonction B<setpwent>() ramène le pointeur de fichier au début de base de "
"données des mots de passe."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The B<endpwent>()  function is used to close the password database after all "
"processing has been performed."
msgstr ""
"La fonction B<endpwent>() est utilisée pour fermer la base de données des "
"mots de passe après que toutes les actions ont été réalisées."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The I<passwd> structure is defined in I<E<lt>pwd.hE<gt>> as follows:"
msgstr ""
"La structure I<passwd> est définie dans I<E<lt>pwd.hE<gt>> comme ceci\\ :"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"struct passwd {\n"
"    char   *pw_name;       /* username */\n"
"    char   *pw_passwd;     /* user password */\n"
"    uid_t   pw_uid;        /* user ID */\n"
"    gid_t   pw_gid;        /* group ID */\n"
"    char   *pw_gecos;      /* user information */\n"
"    char   *pw_dir;        /* home directory */\n"
"    char   *pw_shell;      /* shell program */\n"
"};\n"
msgstr ""
"struct passwd {\n"
"    char   *pw_name;       /* Nom d'utilisateur */\n"
"    char   *pw_passwd;     /* Mot de passe de l'utilisateur */\n"
"    uid_t   pw_uid;        /* ID de l'utilisateur */\n"
"    gid_t   pw_gid;        /* ID du groupe */\n"
"    char   *pw_gecos;      /* Information utilisateur */\n"
"    char   *pw_dir;        /* Répertoire personnel */\n"
"    char   *pw_shell;      /* Interpréteur de commande */\n"
"};\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"For more information about the fields of this structure, see B<passwd>(5)."
msgstr ""
"Pour plus d'informations à propos des champs de cette structure, consultez "
"B<passwd>(5)."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The B<getpwent>()  function returns a pointer to a I<passwd> structure, or "
"NULL if there are no more entries or an error occurred.  If an error occurs, "
"I<errno> is set appropriately.  If one wants to check I<errno> after the "
"call, it should be set to zero before the call."
msgstr ""
"La fonction B<getpwent>() renvoie un pointeur sur une structure I<passwd>, "
"ou NULL si une erreur s'est produite ou s'il n'y a plus d'enregistrement. En "
"cas d'erreur, I<errno> est positionnée en conséquence. Si vous souhaitez "
"vérifier I<errno> après l'appel, celle-ci doit être positionnée à zéro avant "
"l'appel."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The return value may point to a static area, and may be overwritten by "
"subsequent calls to B<getpwent>(), B<getpwnam>(3), or B<getpwuid>(3).  (Do "
"not pass the returned pointer to B<free>(3).)"
msgstr ""
"La valeur de retour peut pointer vers une zone statique et donc être écrasée "
"par des appels successifs à B<getpwent>(), B<getpwnam>(3) ou B<getpwuid>(3). "
"(Ne pas passer le pointeur renvoyé à B<free>(3).)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "A signal was caught; see B<signal>(7)."
msgstr "Un signal a été intercepté ; consultez B<signal>(7)."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "I/O error."
msgstr "Erreur d'entrée-sortie."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""
"La limite du nombre de descripteurs de fichiers par processus a été atteinte."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""
"La limite du nombre total de fichiers ouverts pour le système entier a été "
"atteinte."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#.  not in POSIX
#.  to allocate the passwd structure, or to allocate buffers
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Insufficient memory to allocate I<passwd> structure."
msgstr "Pas assez de mémoire pour allouer la structure I<passwd>."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Insufficient buffer space supplied."
msgstr "L'espace tampon fourni est insuffisant."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</etc/passwd>"
msgstr "I</etc/passwd>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "local password database file"
msgstr "Base de données des mots de passe locaux"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
msgid "B<getpwent>()"
msgstr "B<setgrent>() :"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "MT-Unsafe race:pwent\n"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "race:pwentbuf locale"
msgstr ""

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
msgid "B<setpwent>(),\n"
msgstr "B<setgrent>() :"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
msgid "B<endpwent>()"
msgstr "B<setgrent>() :"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "MT-Unsafe race:pwent locale"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"In the above table, I<pwent> in I<race:pwent> signifies that if any of the "
"functions B<setpwent>(), B<getpwent>(), or B<endpwent>()  are used in "
"parallel in different threads of a program, then data races could occur."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD.  The I<pw_gecos> field is not "
"specified in POSIX, but is present on most implementations."
msgstr ""
"SVr4, 4.3BSD, POSIX.1-2001. Le champ I<pw_gecos> n'est pas spécifié dans "
"POSIX, mais il est présent sur la plupart des implémentations."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"B<fgetpwent>(3), B<getpw>(3), B<getpwent_r>(3), B<getpwnam>(3), "
"B<getpwuid>(3), B<putpwent>(3), B<passwd>(5)"
msgstr ""
"B<fgetpwent>(3), B<getpw>(3), B<getpwent_r>(3), B<getpwnam>(3), "
"B<getpwuid>(3), B<putpwent>(3), B<passwd>(5)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"This page is part of release 5.05 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.05 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"When B<shadow>(5)  passwords are enabled (which is default on many GNU/Linux "
"installations) the content of I<pw_passwd> is usually not very useful.  In "
"such a case most passwords are stored in a separate file."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"The variable I<pw_shell> may be empty, in which case the system will execute "
"the default shell (B</bin/sh>)  for the user."
msgstr ""

#.  Next line rejected upstream
#. type: Plain text
#: debian-buster debian-unstable
#, fuzzy
msgid ""
"B<fgetpwent>(3), B<getpw>(3), B<getpwent_r>(3), B<getpwnam>(3), "
"B<getpwuid>(3), B<putpwent>(3), B<shadow>(5), B<passwd>(5)"
msgstr ""
"B<fgetpwent>(3), B<getpw>(3), B<getpwent_r>(3), B<getpwnam>(3), "
"B<getpwuid>(3), B<putpwent>(3), B<passwd>(5)"

#. type: Plain text
#: debian-buster fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
