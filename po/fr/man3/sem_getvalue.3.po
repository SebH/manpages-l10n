# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <http://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2019-11-29 12:35+01:00\n"
"PO-Revision-Date: 2018-11-12 17:11+0100\n"
"Last-Translator: Thomas Vincent <tvincent@debian.org>\n"
"Language-Team: French <https://translate.holcroft.fr/projects/man-pages-fr/"
"semaphore/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 1.8.11\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEM_GETVALUE"
msgstr "SEM_GETVALUE"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "sem_getvalue - get the value of a semaphore"
msgstr "sem_getvalue - Obtenir la valeur d'un sémaphore"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>semaphore.hE<gt>>\n"
msgstr "B<#include E<lt>semaphore.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<int sem_getvalue(sem_t *>I<sem>B<, int *>I<sval>B<);>\n"
msgstr "B<int sem_getvalue(sem_t *>I<sem>B<, int *>I<sval>B<);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Link with I<-pthread>."
msgstr "Effectuez l'édition des liens avec l'option B<-pthread>."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<sem_getvalue>()  places the current value of the semaphore pointed to "
"I<sem> into the integer pointed to by I<sval>."
msgstr ""
"B<sem_getvalue>() place la valeur actuelle du sémaphore pointé par I<sem> "
"dans l'entier pointé par I<sval>."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"If one or more processes or threads are blocked waiting to lock the "
"semaphore with B<sem_wait>(3), POSIX.1 permits two possibilities for the "
"value returned in I<sval>: either 0 is returned; or a negative number whose "
"absolute value is the count of the number of processes and threads currently "
"blocked in B<sem_wait>(3).  Linux adopts the former behavior."
msgstr ""
"Si un ou plusieurs processus ou threads sont bloqués en attente de "
"verrouiller le sémaphore avec B<sem_wait>(3), POSIX.1 permet deux valeurs de "
"retour possibles pour I<sval>\\ : soit 0 est renvoyé, soit une valeur "
"négative dont la valeur absolue est le nombre de processus et de threads "
"actuellement bloqués dans B<sem_wait>(3). Linux adopte le premier "
"comportement."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<sem_getvalue>()  returns 0 on success; on error, -1 is returned and "
"I<errno> is set to indicate the error."
msgstr ""
"B<sem_getvalue>() renvoie 0 s'il réussit. S'il échoue, il renvoie -1 et "
"écrit I<errno> en conséquence."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "I<sem> is not a valid semaphore."
msgstr "I<sem> n'est pas un sémaphore valable."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<sem_getvalue>()"
msgstr "B<sem_getvalue>()"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The value of the semaphore may already have changed by the time "
"B<sem_getvalue>()  returns."
msgstr ""
"La valeur du sémaphore peut déjà avoir été modifiée lorsque "
"B<sem_getvalue>() renvoie."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<sem_post>(3), B<sem_wait>(3), B<sem_overview>(7)"
msgstr "B<sem_post>(3), B<sem_wait>(3), B<sem_overview>(7)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-buster debian-unstable
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.04 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This page is part of release 5.01 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.01 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page, peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
