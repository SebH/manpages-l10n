# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2020-04-18 20:21+02:00\n"
"PO-Revision-Date: 2019-11-07 19:16+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: Dutch <debian-l10n-german@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 19.08.2\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "WRITE"
msgstr "WRITE"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2019-10-10"
msgstr "10 oktober 2019"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux Programmeurs Handleiding"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "write - write to a file descriptor"
msgstr "write - schrijf naar een bestandindicator"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<ssize_t write(int >I<fd>B<, const void *>I<buf>B<, size_t >I<count>B<);>"
msgstr ""
"B<ssize_t write(int >I<bi>B<, const void *>I<buf>B<, size_t >I<tel>B<);>"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<write>()  writes up to I<count> bytes from the buffer starting at I<buf> "
"to the file referred to by the file descriptor I<fd>."
msgstr ""
"B<write>() schrijft tot I<tel> bytes naar het bestand waar I<bi> naar wijst, "
"van de buffer die begint op I<buf>."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The number of bytes written may be less than I<count> if, for example, there "
"is insufficient space on the underlying physical medium, or the "
"B<RLIMIT_FSIZE> resource limit is encountered (see B<setrlimit>(2)), or the "
"call was interrupted by a signal handler after having written less than "
"I<count> bytes.  (See also B<pipe>(7).)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"For a seekable file (i.e., one to which B<lseek>(2)  may be applied, for "
"example, a regular file)  writing takes place at the file offset, and the "
"file offset is incremented by the number of bytes actually written.  If the "
"file was B<open>(2)ed with B<O_APPEND>, the file offset is first set to the "
"end of the file before writing.  The adjustment of the file offset and the "
"write operation are performed as an atomic step."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"POSIX requires that a B<read>(2)  that can be proved to occur after a "
"B<write>()  has returned will return the new data.  Note that not all "
"filesystems are POSIX conforming."
msgstr ""
"POSIX eist dat een B<read>(2) die bewijsbaar na een B<write>() plaatsvindt "
"de nieuwe gegevens oplevert. Merk op dat niet alle bestandsystemen voldoen "
"aan POSIX."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"According to POSIX.1, if I<count> is greater than B<SSIZE_MAX>, the result "
"is implementation-defined; see NOTES for the upper limit on Linux."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"On success, the number of bytes written is returned.  On error, -1 is "
"returned, and I<errno> is set to indicate the cause of the error."
msgstr ""
"Bij success wordt het aantal geschreven bytes teruggegeven (nul betekend "
"niks werd geschreven). Bij falen wordt -1 teruggegeven en I<errno> wordt "
"naar behoren gezet."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Note that a successful B<write>()  may transfer fewer than I<count> bytes.  "
"Such partial writes can occur for various reasons; for example, because "
"there was insufficient space on the disk device to write all of the "
"requested bytes, or because a blocked B<write>()  to a socket, pipe, or "
"similar was interrupted by a signal handler after it had transferred some, "
"but before it had transferred all of the requested bytes.  In the event of a "
"partial write, the caller can make another B<write>()  call to transfer the "
"remaining bytes.  The subsequent call will either transfer further bytes or "
"may result in an error (e.g., if the disk is now full)."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"If I<count> is zero and I<fd> refers to a regular file, then B<write>()  may "
"return a failure status if one of the errors below is detected.  If no "
"errors are detected, or error detection is not performed, 0 will be returned "
"without causing any other effect.  If I<count> is zero and I<fd> refers to a "
"file other than a regular file, the results are not specified."
msgstr ""
"Als I<tel> nul is en de bestandindicator wijst naar een normaal bestand dan "
"wordt 0 teruggegeven zonder verdere gevolgen te veroorzaken. Voor een "
"speciaal bestand zijn de resultaten niet overdraagbaar."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"The file descriptor I<fd> refers to a file other than a socket and has been "
"marked nonblocking (B<O_NONBLOCK>), and the write would block.  See "
"B<open>(2)  for further details on the B<O_NONBLOCK> flag."
msgstr ""
"Niet-blokkerende In/Uit werd gekozen met B<O_NONBLOCK>, maar er was geen "
"ruimte in de pijp of socket verbonden met I<bi> om de gegevens direct weg te "
"schrijven."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EAGAIN> or B<EWOULDBLOCK>"
msgstr "B<EAGAIN> of B<EWOULDBLOCK>"

#.  Actually EAGAIN on Linux
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The file descriptor I<fd> refers to a socket and has been marked nonblocking "
"(B<O_NONBLOCK>), and the write would block.  POSIX.1-2001 allows either "
"error to be returned for this case, and does not require these constants to "
"have the same value, so a portable application should check for both "
"possibilities."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "I<fd> is not a valid file descriptor or is not open for writing."
msgstr "I<bi> is geen geldige bestandsindicator, of is niet open voor lezen."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EDESTADDRREQ>"
msgstr "B<EDESTADDRREQ>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"I<fd> refers to a datagram socket for which a peer address has not been set "
"using B<connect>(2)."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EDQUOT>"
msgstr "B<EDQUOT>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"The user's quota of disk blocks on the filesystem containing the file "
"referred to by I<fd> has been exhausted."
msgstr ""
"Het apparaat dat het bestand bevat waar I<bi> naar wijst heeft geen ruimte "
"voor de gegevens."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "I<buf> is outside your accessible address space."
msgstr "I<buf> ligt buiten de door u toegankelijke adres ruimte."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EFBIG>"
msgstr "B<EFBIG>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"An attempt was made to write a file that exceeds the implementation-defined "
"maximum file size or the process's file size limit, or to write at a "
"position past the maximum allowed offset."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The call was interrupted by a signal before any data was written; see "
"B<signal>(7)."
msgstr ""
"De aanroep werd onderbroken door een signaal voordat gegevens werden "
"geschreven; zie B<signal>(7)."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"I<fd> is attached to an object which is unsuitable for writing; or the file "
"was opened with the B<O_DIRECT> flag, and either the address specified in "
"I<buf>, the value specified in I<count>, or the file offset is not suitably "
"aligned."
msgstr "I<bi> wijst naar iets dat ongeschikt is om naar te schrijven."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#.  commit 088737f44bbf6378745f5b57b035e57ee3dc4750
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"A low-level I/O error occurred while modifying the inode.  This error may "
"relate to the write-back of data written by an earlier B<write>(), which may "
"have been issued to a different file descriptor on the same file.  Since "
"Linux 4.13, errors from write-back come with a promise that they I<may> be "
"reported by subsequent.  B<write>()  requests, and I<will> be reported by a "
"subsequent B<fsync>(2)  (whether or not they were also reported by "
"B<write>()).  An alternate cause of B<EIO> on networked filesystems is when "
"an advisory lock had been taken out on the file descriptor and this lock has "
"been lost.  See the I<Lost locks> section of B<fcntl>(2)  for further "
"details."
msgstr "Een laag-niveau In/Uit fout trad op terwijl de inode veranderd werd."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The device containing the file referred to by I<fd> has no room for the data."
msgstr ""
"Het apparaat dat het bestand bevat waar I<bi> naar wijst heeft geen ruimte "
"voor de gegevens."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The operation was prevented by a file seal; see B<fcntl>(2)."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EPIPE>"
msgstr "B<EPIPE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"I<fd> is connected to a pipe or socket whose reading end is closed.  When "
"this happens the writing process will also receive a B<SIGPIPE> signal.  "
"(Thus, the write return value is seen only if the program catches, blocks or "
"ignores this signal.)"
msgstr ""
"I<bi> is verbonden met een pijp of socket waarvan de lees-uitgang gesloten "
"is. Wanneer dit gebeurd ontvangt het schrijvende proces een B<SIGPIPE> "
"signaal; als het proces dat signaal afvangt, blokkeert of negeert danwordt "
"B<EPIPE> teruggegeven."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Other errors may occur, depending on the object connected to I<fd>."
msgstr ""
"Andere fouten kunnen optreden afhankelijk van dat wat verbonden is met I<bi>."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "VOLDOET AAN"

#.  SVr4 documents additional error
#.  conditions EDEADLK, ENOLCK, ENOLNK, ENOSR, ENXIO, or ERANGE.
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "SVr4, 4.3BSD, POSIX.1-2001."
msgstr "SVr4, 4.3BSD, POSIX.1-2001."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Under SVr4 a write may be interrupted and return B<EINTR> at any point, not "
"just before any data is written."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The types I<size_t> and I<ssize_t> are, respectively, unsigned and signed "
"integer data types specified by POSIX.1."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"A successful return from B<write>()  does not make any guarantee that data "
"has been committed to disk.  On some filesystems, including NFS, it does not "
"even guarantee that space has successfully been reserved for the data.  In "
"this case, some errors might be delayed until a future B<write>(), "
"B<fsync>(2), or even B<close>(2).  The only way to be sure is to call "
"B<fsync>(2)  after you are done writing all your data."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"If a B<write>()  is interrupted by a signal handler before any bytes are "
"written, then the call fails with the error B<EINTR>; if it is interrupted "
"after at least one byte has been written, the call succeeds, and returns the "
"number of bytes written."
msgstr ""

#.  commit e28cc71572da38a5a12c1cfe4d7032017adccf69
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"On Linux, B<write>()  (and similar system calls) will transfer at most "
"0x7ffff000 (2,147,479,552) bytes, returning the number of bytes actually "
"transferred.  (This is true on both 32-bit and 64-bit systems.)"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"An error return value while performing B<write>()  using direct I/O does not "
"mean the entire write has failed. Partial data may be written and the data "
"at the file offset on which the B<write>()  was attempted should be "
"considered inconsistent."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"According to POSIX.1-2008/SUSv4 Section XSI 2.9.7 (\"Thread Interactions "
"with Regular File Operations\"):"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"All of the following functions shall be atomic with respect to each other in "
"the effects specified in POSIX.1-2008 when they operate on regular files or "
"symbolic links: ..."
msgstr ""

#
#.  http://thread.gmane.org/gmane.linux.kernel/1649458
#.     From: Michael Kerrisk (man-pages <mtk.manpages <at> gmail.com>
#.     Subject: Update of file offset on write() etc. is non-atomic with I/O
#.     Date: 2014-02-17 15:41:37 GMT
#.     Newsgroups: gmane.linux.kernel, gmane.linux.file-systems
#.  commit 9c225f2655e36a470c4f58dbbc99244c5fc7f2d4
#.     Author: Linus Torvalds <torvalds@linux-foundation.org>
#.     Date:   Mon Mar 3 09:36:58 2014 -0800
#.         vfs: atomic f_pos accesses as per POSIX
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Among the APIs subsequently listed are B<write>()  and B<writev>(2).  And "
"among the effects that should be atomic across threads (and processes)  are "
"updates of the file offset.  However, on Linux before version 3.14, this was "
"not the case: if two processes that share an open file description (see "
"B<open>(2))  perform a B<write>()  (or B<writev>(2))  at the same time, then "
"the I/O operations were not atomic with respect updating the file offset, "
"with the result that the blocks of data output by the two processes might "
"(incorrectly) overlap.  This problem was fixed in Linux 3.14."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<close>(2), B<fcntl>(2), B<fsync>(2), B<ioctl>(2), B<lseek>(2), B<open>(2), "
"B<pwrite>(2), B<read>(2), B<select>(2), B<writev>(2), B<fwrite>(3)"
msgstr ""
"B<close>(2), B<fcntl>(2), B<fsync>(2), B<ioctl>(2), B<lseek>(2), B<open>(2), "
"B<pwrite>(2), B<read>(2), B<select>(2), B<writev>(2), B<fwrite>(3)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.06 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 5.06 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#. type: Plain text
#: debian-buster
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 5.04 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."
