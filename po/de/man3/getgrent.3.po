# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2012.
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2012.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2020-03-15 09:36+01:00\n"
"PO-Revision-Date: 2019-08-24 07:15+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GETGRENT"
msgstr "GETGRENT"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "getgrent, setgrent, endgrent - get group file entry"
msgstr "getgrent, setgrent, endgrent - holt einen Eintrag aus der Gruppendatei"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>grp.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>grp.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<struct group *getgrent(void);>\n"
msgstr "B<struct group *getgrent(void);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<void setgrent(void);>\n"
msgstr "B<void setgrent(void);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<void endgrent(void);>\n"
msgstr "B<void endgrent(void);>\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr "Mit Glibc erforderliche Makros (siehe B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<setgrent>():"
msgstr "B<setgrent>():"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Glibc seit 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc-Versionen E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<getgrent>(), B<endgrent>():"
msgstr "B<getgrent>(), B<endgrent>():"

#.         || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"Since glibc 2.22:\n"
"    _XOPEN_SOURCE\\ E<gt>=\\ 500 ||\n"
"        _DEFAULT_SOURCE\n"
msgstr ""
"Seit Glibc 2.22:\n"
"    _XOPEN_SOURCE\\ E<gt>=\\ 500 ||\n"
"        _DEFAULT_SOURCE\n"

#.         || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"Glibc 2.21 and earlier\n"
"    _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"        || /* Since glibc 2.12: */ _POSIX_C_SOURCE\\ E<gt>=\\ 200809L\n"
"        || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"Glibc 2.21 und älter\n"
"    _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"        || /* Seit Glibc 2.12: */ _POSIX_C_SOURCE\\ E<gt>=\\ 200809L\n"
"        || /* Glibc-Versionen E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME: explain|reword broken-out or skip it
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The B<getgrent>()  function returns a pointer to a structure containing the "
"broken-out fields of a record in the group database (e.g., the local group "
"file I</etc/group>, NIS, and LDAP).  The first time B<getgrent>()  is "
"called, it returns the first entry; thereafter, it returns successive "
"entries."
msgstr ""
"Die Funktion B<getgrent>() gibt einen Zeiger auf eine Struktur zurück, "
"welche die herausgenommenen Felder eines Eintrags in der Gruppendatenbank "
"enthält (z.B. die lokale Gruppendatei I</etc/group>, NIS und LDAP). Beim "
"ersten Aufruf gibt sie den ersten Eintrag zurück, danach bei jedem weiteren "
"Aufruf die folgenden Einträge."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The B<setgrent>()  function rewinds to the beginning of the group database, "
"to allow repeated scans."
msgstr ""
"Die Funktion B<setgrent>() setzt den Dateizeiger auf den Anfang der "
"Gruppendatenbank zurück, um wiederholte Abfragen zu ermöglichen."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The B<endgrent>()  function is used to close the group database after all "
"processing has been performed."
msgstr ""
"Die Funktion B<endgrent>() wird dazu verwendet, die Gruppendatenbank zu "
"schließen, nachdem die gesamte Verarbeitung durchgeführt wurde."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The I<group> structure is defined in I<E<lt>grp.hE<gt>> as follows:"
msgstr "Die Struktur I<group> wird in I<E<lt>grp.hE<gt>> wie folgt definiert:"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"struct group {\n"
"    char   *gr_name;        /* group name */\n"
"    char   *gr_passwd;      /* group password */\n"
"    gid_t   gr_gid;         /* group ID */\n"
"    char  **gr_mem;         /* NULL-terminated array of pointers\n"
"                               to names of group members */\n"
"};\n"
msgstr ""
"struct group {\n"
"    char   *gr_name;        /* Gruppenname */\n"
"    char   *gr_passwd;      /* Gruppenpasswort */\n"
"    gid_t   gr_gid;         /* Gruppenkennung */\n"
"    char  **gr_mem;         /* NULL-terminiertes Feld von Zeigern auf\n"
"                               Namen von Gruppenmitgliedern */\n"
"};\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"For more information about the fields of this structure, see B<group>(5)."
msgstr ""
"Weitere Informationen zu den Feldern dieser Struktur finden Sie in "
"B<group>(5)."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The B<getgrent>()  function returns a pointer to a I<group> structure, or "
"NULL if there are no more entries or an error occurs."
msgstr ""
"Die Funktion B<getgrent>() gibt einen Zeiger auf eine I<group>-Struktur "
"zurück oder NULL, falls es keine weiteren Einträge mehr gibt oder ein Fehler "
"auftritt."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Upon error, I<errno> may be set.  If one wants to check I<errno> after the "
"call, it should be set to zero before the call."
msgstr ""
"Im Fehlerfall kann I<errno> gesetzt werden. Wenn Sie I<errno> nach dem "
"Aufruf auswerten wollen, sollten Sie die Variable vorher auf Null setzen."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The return value may point to a static area, and may be overwritten by "
"subsequent calls to B<getgrent>(), B<getgrgid>(3), or B<getgrnam>(3).  (Do "
"not pass the returned pointer to B<free>(3).)"
msgstr ""
"Der Rückgabewert kann auf einen statischen Bereich zeigen und kann durch "
"anschließende Aufrufe von B<getgrent>(), B<getgrgid>(3) oder B<getgrnam>(3) "
"überschrieben werden. (Übergeben Sie den zurückgegebenen Zeiger nicht an "
"B<free>(3).)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The service was temporarily unavailable; try again later.  For NSS backends "
"in glibc this indicates a temporary error talking to the backend.  The error "
"may correct itself, retrying later is suggested."
msgstr ""
"Der Dienst war vorübergehend nicht erreichbar, bitte versuchen Sie es später "
"erneut. Für NSS-Backends in der Glibc weist dies auf einen temporären Fehler "
"bei der Kommunikation mit dem Backend hin. Der Fehler kann sich unter "
"Umständen selbst korrigieren, ein erneuter Versuch wird empfohlen."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "A signal was caught; see B<signal>(7)."
msgstr "Ein Signal wurde abgefangen; siehe B<signal>(7)."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "I/O error."
msgstr "E/A-Fehler (engl. I/O)."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""
"Die Beschränkung pro Prozess der Anzahl offener Datei-Deskriptoren wurde "
"erreicht."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""
"Die systemweite Beschränkung für die Gesamtzahl offener Dateien wurde "
"erreicht."

#.  not in POSIX
#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"A necessary input file cannot be found.  For NSS backends in glibc this "
"indicates the backend is not correctly configured."
msgstr ""
"Eine erforderliche Eingabedatei konnte nicht gefunden werden. Für NSS-"
"Backends in der Glibc weist dies darauf hin, dass das Backend nicht korrekt "
"eingerichtet wurde."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#.  not in POSIX
#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Insufficient memory to allocate I<group> structure."
msgstr ""
"Es ist nicht ausreichend Speicher für die Bereitstellung einer I<group>-"
"Struktur vorhanden."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Insufficient buffer space supplied."
msgstr "Zu wenig Pufferspeicher bereitgestellt."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</etc/group>"
msgstr "I</etc/group>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "local group database file"
msgstr "lokale Gruppendatenbank-Datei"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<getgrent>()"
msgstr "B<getgrent>()"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "MT-Unsafe race:grent\n"
msgstr "MT-Unsafe race:grent\n"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "race:grentbuf locale"
msgstr "race:grentbuf locale"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"B<setgrent>(),\n"
"B<endgrent>()"
msgstr ""
"B<setgrent>(),\n"
"B<endgrent>()"

#. type: tbl table
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "MT-Unsafe race:grent locale"
msgstr "MT-Unsafe race:grent locale"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"In the above table, I<grent> in I<race:grent> signifies that if any of the "
"functions B<setgrent>(), B<getgrent>(), or B<endgrent>()  are used in "
"parallel in different threads of a program, then data races could occur."
msgstr ""
"In der obigen Tabelle bedeutet I<grent> in I<race:grent>, dass, falls eine "
"der Funktionen B<setgrent>(), B<getgrent>() oder B<endgrent>() in "
"verschiedenen Threads eines Programms parallel verwandt werden, "
"konkurrierende Zugriffe auf Daten (»data races«) auftreten könnten."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<fgetgrent>(3), B<getgrent_r>(3), B<getgrgid>(3), B<getgrnam>(3), "
"B<getgrouplist>(3), B<putgrent>(3), B<group>(5)"
msgstr ""
"B<fgetgrent>(3), B<getgrent_r>(3), B<getgrgid>(3), B<getgrnam>(3), "
"B<getgrouplist>(3), B<putgrent>(3), B<group>(5)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"This page is part of release 5.05 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.05 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: debian-buster fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.04 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.04 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."
