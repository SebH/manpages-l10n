# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2019-08-03 18:01+02:00\n"
"PO-Revision-Date: 2019-01-19 08:46+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "MKFS.MINIX"
msgstr "MKFS.MINIX"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "June 2015"
msgstr "Juni 2015"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "System Administration"
msgstr "System-Administration"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "mkfs.minix - make a Minix filesystem"
msgstr "mkfs.minix - ein Minix-Dateisystem erstellen"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<mkfs.minix> [options] I<device> [I<size-in-blocks>]"
msgstr "B<mkfs.minix> [Optionen] I<Gerät> [I<Größe-in-Blöcken>]"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<mkfs.minix> creates a Linux MINIX filesystem on a device (usually a disk "
"partition)."
msgstr ""
"B<mkfs.minix> erstellt ein Linux-MINIX-Dateisystem auf einem Gerät "
"(üblicherweise einer Laufwerkspartition)."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The I<device> is usually of the following form:"
msgstr "Das I<Gerät> hat für gewöhnlich die folgende Form:"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid ""
"/dev/hda[1\\(en8] (IDE disk 1)\n"
"/dev/hdb[1\\(en8] (IDE disk 2)\n"
"/dev/sda[1\\(en8] (SCSI disk 1)\n"
"/dev/sdb[1\\(en8] (SCSI disk 2)\n"
msgstr ""
"/dev/hda[1–8] (IDE-Platte 1)\n"
"/dev/hdb[1–8] (IDE-Platte 2)\n"
"/dev/sda[1–8] (SCSI-Platte 1)\n"
"/dev/sdb[1–8] (SCSI-Platte 2)\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The device may be a block device or an image file of one, but this is not "
"enforced.  Expect not much fun on a character device :-)."
msgstr ""
"Das Gerät kann ein blockorientiertes Gerät oder eine Abbilddatei davon sein, "
"aber das ist nicht zwingend erforderlich. Doch erwarten Sie nicht zu viel "
"von einem zeichenorientierten Gerät :-)."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The I<size-in-blocks> parameter is the desired size of the file system, in "
"blocks.  It is present only for backwards compatibility.  If omitted the "
"size will be determined automatically.  Only block counts strictly greater "
"than 10 and strictly less than 65536 are allowed."
msgstr ""
"Der Parameter I<Größe_in_Blöcken> ist die gewünschte Größe des Dateisystems "
"in Blöcken. Dies ist nur aus Gründen der Abwärtskompatibilität vorhanden. "
"Falls nicht angegeben, wird die Größe automatisch ermittelt. Es sind nur "
"Blockanzahlen unbedingt größer als 10 und kleiner als 65536 erlaubt."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-c>, B<--check>"
msgstr "B<-c>, B<--check>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Check the device for bad blocks before creating the filesystem.  If any are "
"found, the count is printed."
msgstr ""
"überprüft das Gerät auf fehlerhafte Blöcke, bevor das Dateisystem erstellt "
"wird. Falls welche gefunden werden, wird deren Anzahl ausgegeben."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-n>, B<--namelength> I<length>"
msgstr "B<-n>, B<--namelength> I<Länge>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Specify the maximum length of filenames.  Currently, the only allowable "
"values are 14 and 30 for file system versions 1 and 2.  Version 3 allows "
"only value 60.  The default is 30."
msgstr ""
"gibt die maximal zulässige Länge für Dateinamen an. Gegenwärtig sind nur 14 "
"und 30 für die Dateisystemversionen 1 und 2 als Werte zulässig. Version 3 "
"erlaubt lediglich den Wert 60. Der voreingestellte Wert ist 30."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-i>, B<--inodes> I<number>"
msgstr "B<-i>, B<--inodes> I<Zahl>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Specify the number of inodes for the filesystem."
msgstr "gibt die Anzahl der Inodes für das Dateisystem an."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-l>, B<--badblocks> I<filename>"
msgstr "B<-l>, B<--badblocks> I<Dateiname>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Read the list of bad blocks from I<filename>.  The file has one bad-block "
"number per line.  The count of bad blocks read is printed."
msgstr ""
"liest die Liste der fehlerhaften Blöcke aus I<Dateiname>. Die Datei muss pro "
"Zeile eine Nummer eines fehlerhaften Blocks enthalten. Die Anzahl der "
"gelesenen fehlerhaften Blöcke wird ausgegeben."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-1>"
msgstr "B<-1>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Make a Minix version 1 filesystem.  This is the default."
msgstr ""
"erstellt ein Minix-Dateisystem der Version 1. Dies ist die Voreinstellung."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-2>,B< -v>"
msgstr "B<-2>,B< -v>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Make a Minix version 2 filesystem."
msgstr "erstellt ein Minix-Dateisystem der Version 2."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-3>"
msgstr "B<-3>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Make a Minix version 3 filesystem."
msgstr "erstellt ein Minix-Dateisystem der Version 3."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Display version information and exit.  The long option cannot be combined "
"with other options."
msgstr ""
"zeigt Versionsinformationen an und beendet das Programm. Die Langform dieser "
"Option kann nicht mit anderen Optionen kombiniert werden."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EXIT CODES"
msgstr "EXIT-CODES"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The exit code returned by B<mkfs.minix> is one of the following:"
msgstr "B<mkfs.minix> gibt einen der folgenden Exit-Codes zurück:"

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "0"
msgstr "0"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "No errors"
msgstr "Keine Fehler"

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "8"
msgstr "8"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Operational error"
msgstr "Betriebsfehler"

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "16"
msgstr "16"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Usage or syntax error"
msgstr "Aufruf- oder Syntaxfehler"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<fsck>(8), B<mkfs>(8), B<reboot>(8)"
msgstr "B<fsck>(8), B<mkfs>(8), B<reboot>(8)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The mkfs.minix command is part of the util-linux package and is available "
"from https://www.kernel.org/pub/linux/utils/util-linux/."
msgstr ""
"Der Befehl mkfs.minix ist Teil des Pakets util-linux und kann von https://"
"www.kernel.org/pub/linux/utils/util-linux/ heruntergeladen werden."
