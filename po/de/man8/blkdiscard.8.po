# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014-2015.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2019-08-03 18:01+02:00\n"
"PO-Revision-Date: 2018-08-09 21:19+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "BLKDISCARD"
msgstr "BLKDISCARD"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "July 2014"
msgstr "Juli 2014"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "System Administration"
msgstr "System-Administration"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "blkdiscard - discard sectors on a device"
msgstr "blkdiscard - Sektoren auf einem Gerät verwerfen"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<blkdiscard> [options] [B<-o> I<offset>] [B<-l> I<length>] I<device>"
msgstr "B<blkdiscard> [Optionen] [B<-o> I<Versatz>] [B<-l> I<Länge>] I<Gerät>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<blkdiscard> is used to discard device sectors.  This is useful for solid-"
"state drivers (SSDs) and thinly-provisioned storage.  Unlike B<fstrim>(8), "
"this command is used directly on the block device."
msgstr ""
"B<blkdiscard> wird zum Verwerfen von Gerätesektoren verwendet. Dies ist für "
"Solid State Drives (SSDs) und sonstige Geräte mit geringem Speicherplatz "
"nützlich. Im Gegensatz zu B<fstrim>(8) wird dieser Befehl direkt auf dem "
"blockorientierten Gerät angewendet."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"By default, B<blkdiscard> will discard all blocks on the device.  Options "
"may be used to modify this behavior based on range or size, as explained "
"below."
msgstr ""
"Per Vorgabe verwirft B<blkdiscard> alle Blöcke eines Gerätes. Zum Anpassen "
"des Verhaltens sind Optionen verfügbar, die auf Bereichen oder Größenangaben "
"basieren, wie nachfolgend erläutert."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The I<device> argument is the pathname of the block device."
msgstr "Das Argument I<Gerät> ist der Pfadname zu dem blockorientierten Gerät."

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<WARNING: All data in the discarded region on the device will be lost!>"
msgstr ""
"B<WARNUNG: Alle Data im verworfenen Bereich des Gerätes gehen verloren!>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The I<offset> and I<length> arguments may be followed by the multiplicative "
"suffixes KiB (=1024), MiB (=1024*1024), and so on for GiB, TiB, PiB, EiB, "
"ZiB and YiB (the \"iB\" is optional, e.g., \"K\" has the same meaning as "
"\"KiB\") or the suffixes KB (=1000), MB (=1000*1000), and so on for GB, TB, "
"PB, EB, ZB and YB."
msgstr ""
"Die Argumente für I<Länge> und I<Versatz> können durch die Binärsuffixe "
"KiB=1024, MiB1024*1024 und so weiter für GiB, TiB, PiB und EiB ergänzt "
"werden, wobei das »iB« nicht erforderlich ist. Zum Beispiel ist »K« "
"gleichbedeutend mit »KiB«. Möglich sind außerdem die Dezimalsuffixe KB=1000, "
"MB=1000*1000, GB, PB und EB."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-o>,B< --offset >I<offset>"
msgstr "B<-o>,B< --offset >I<Versatz>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Byte offset into the device from which to start discarding.  The provided "
"value will be aligned to the device sector size.  The default value is zero."
msgstr ""
"ist der Byte-Versatz des Gerätes, wo mit der Verwerfung begonnen werden "
"soll. Der angegebene Wert wird an der Sektorengröße des Gerätes "
"ausgerichtet. Der Vorgabewert ist 0."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-l>,B< --length >I<length>"
msgstr "B<-l>,B< --length >I<Länge>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The number of bytes to discard (counting from the starting point).  The "
"provided value will be aligned to the device sector size.  If the specified "
"value extends past the end of the device, B<blkdiscard> will stop at the "
"device size boundary.  The default value extends to the end of the device."
msgstr ""
"bezeichnet die Anzahl der zu verwerfenden Bytes (vom Anfangspunkt gezählt). "
"Der angegebene Wert wird an der Sektorengröße des Gerätes ausgerichtet. "
"Falls dieser Wert das Ende des Gerätes überschreitet, stoppt B<blkdiscard>, "
"wenn das Ende der Gerätegröße erreicht ist. Der Vorgabewert ist das Ende des "
"Gerätes."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-p>,B< --step >I<length>"
msgstr "B<-p>,B< --step >I<Länge>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The number of bytes to discard within one iteration. The default is to "
"discard all by one ioctl call."
msgstr ""
"gibt die während eines Durchlaufs zu verwerfenden Bytes an. In der "
"Voreinstellung werden alle Bytes mit einem Ioctl-Aufruf verworfen."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-s>,B< --secure>"
msgstr "B<-s>,B< --secure>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Perform a secure discard.  A secure discard is the same as a regular discard "
"except that all copies of the discarded blocks that were possibly created by "
"garbage collection must also be erased.  This requires support from the "
"device."
msgstr ""
"führt ein sicheres Verwerfen aus. Das ist das gleiche wie ein reguläres "
"Verwerfen, außer dass alle Kopien der verworfenen Blöcke, die möglicherweise "
"in der Müll-Sammlung erzeugt wurden, auch verworfen werden müssen. Dieser "
"Vorgang muss vom jeweiligen Gerät unterstützt werden."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-z>,B< --zeroout>"
msgstr "B<-z>,B< --zeroout>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Zero-fill rather than discard."
msgstr "füllt mit Nullen, statt zu verwerfen."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-v>,B< --verbose>"
msgstr "B<-v>,B< --verbose>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Display the aligned values of I<offset> and I<length>.  If the B<--step> "
"option is specified, it prints the discard progress every second."
msgstr ""
"zeigt die ausgerichteten Werte für I<Versatz> und I<Länge> an. Wenn die "
"Option B<--step> angegeben ist, wird der Fortschritt des Verwerfungsvorgangs "
"im Sekundentakt angezeigt."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr "B<-V>,B< --version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display version information and exit."
msgstr "zeigt Versionsinformationen an und beendet das Programm."

#. type: TP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "E<.MT lczerner@redhat.com> Lukas Czerner E<.ME>"
msgstr "E<.MT lczerner@redhat.com> Lukas Czerner E<.ME>"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<fstrim>(8)"
msgstr "B<fstrim>(8)"

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"The blkdiscard command is part of the util-linux package and is available E<."
"UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> Linux "
"Kernel Archive E<.UE .>"
msgstr ""
"Der Befehl blkdiscard ist Teil des Pakets util-linux, welches aus dem E<.UR "
"https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> Linux "
"Kernel-Archiv E<.UE .> heruntergeladen werden kann."
