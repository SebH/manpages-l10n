# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2020-03-07 13:26+01:00\n"
"PO-Revision-Date: 2016-11-15 13:51+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "IONICE"
msgstr "IONICE"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "July 2011"
msgstr "Juli 2011"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "ionice - set or get process I/O scheduling class and priority"
msgstr ""
"ionice - setzt oder ermittelt die E/A-Scheduling-Klasse und -Priorität eines "
"Prozesses"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<ionice> [B<-c> I<class>] [B<-n> I<level>] [B<-t>] B<-p> I<PID>..."
msgstr "B<ionice> [B<-c> I<Klasse>] [B<-n> I<Stufe>] [B<-t>] B<-p> I<PID>…"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<ionice> [B<-c> I<class>] [B<-n> I<level>] [B<-t>] B<-P> I<PGID>..."
msgstr "B<ionice> [B<-c> I<Klasse>] [B<-n> I<Stufe>] [B<-t>] B<-P> I<PGID>…"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<ionice> [B<-c> I<class>] [B<-n> I<level>] [B<-t>] B<-u> I<UID>..."
msgstr "B<ionice> [B<-c> I<Klasse>] [B<-n> I<Stufe>] [B<-t>] B<-u> I<UID>…"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<ionice> [B<-c> I<class>] [B<-n> I<level>] [B<-t>] I<command "
">[I<argument>...]"
msgstr ""
"B<ionice> [B<-c> I<Klasse>] [B<-n> I<Stufe>] [B<-t>] I<Befehl>[I<Argument>…]"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This program sets or gets the I/O scheduling class and priority for a "
"program.  If no arguments or just B<-p> is given, B<ionice> will query the "
"current I/O scheduling class and priority for that process."
msgstr ""
"Dieses Programm setzt oder ermittelt die E/A-Scheduling-Klasse und -"
"Priorität eines Prozesses. Falls keine Argumente oder nur B<-p> angegeben "
"ist, fragt B<ionice> die aktuelle E/A-Scheduling-Klasse und -Priorität "
"dieses Prozesses ab. "

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"When I<command> is given, B<ionice> will run this command with the given "
"arguments.  If no I<class> is specified, then I<command> will be executed "
"with the \"best-effort\" scheduling class.  The default priority level is 4."
msgstr ""
"Wenn ein I<Befehl> angegeben ist, führt B<ionice> diesen Befehl mit den "
"übergebenen Argumenten aus. Falls keine I<Klasse> angegeben wird, dann wird "
"der I<Befehl> mit der »Best-effort«-Scheduling-Klasse ausgeführt. Die "
"vorgegebene Prioritätsstufe ist 4."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"As of this writing, a process can be in one of three scheduling classes:"
msgstr ""
"Derzeit kann ein Prozess in eine dieser drei Scheduling-Klassen eingeordnet "
"werden: "

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<Idle>"
msgstr "B<Idle>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"A program running with idle I/O priority will only get disk time when no "
"other program has asked for disk I/O for a defined grace period.  The impact "
"of an idle I/O process on normal system activity should be zero.  This "
"scheduling class does not take a priority argument.  Presently, this "
"scheduling class is permitted for an ordinary user (since kernel 2.6.25)."
msgstr ""
"Ein Programm, das mit der E/A-Priorität »Idle« ausgeführt wird, erhält nur "
"Zeit für Festplattenzugriffe zugewiesen, wenn kein weiteres Programm dies "
"für einen bestimmten Zeitraum angefordert hat. Der Einfluss eines Idle-E/A-"
"Prozesses auf die normalen Systemaktivitäten sollte Null sein. Diese "
"Scheduling-Klasse akzeptiert kein Prioritätsargument. Derzeit ist die "
"Vergabe dieser Scheduling-Klasse einem gewöhnlichen Benutzer erlaubt (seit "
"Kernel 2.6.25)."

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<Best-effort>"
msgstr "B<Best-effort>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This is the effective scheduling class for any process that has not asked "
"for a specific I/O priority.  This class takes a priority argument from "
"I<0-7>, with a lower number being higher priority.  Programs running at the "
"same best-effort priority are served in a round-robin fashion."
msgstr ""
"Dies ist die effektive Scheduling-Klasse für jeden Prozess, der keine "
"spezifische E/A-Priorität angefordert hat. Diese Klasse akzeptiert die "
"Prioritäts-Argumente I<0-7>, wobei die niedrigere Zahl eine höhere Priorität "
"ausdrückt. Programme, die mit der gleichen Best-Effort-Priorität laufen, "
"werden im Rundlauf-Verfahren bedient."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Note that before kernel 2.6.26 a process that has not asked for an I/O "
"priority formally uses \"B<none>\" as scheduling class, but the I/O "
"scheduler will treat such processes as if it were in the best-effort class.  "
"The priority within the best-effort class will be dynamically derived from "
"the CPU nice level of the process: io_priority = (cpu_nice + 20) / 5."
msgstr ""
"Beachten Sie, dass mit Kerneln älter als 2.6.26 ein Prozess, der keine E/A-"
"Priorität angefordert hat, formell »B<none>« als Scheduling-Klasse "
"verwendet. Doch wird der E/A-Scheduler solche Prozesse so betrachten, als "
"gehörten sie der Best-Effort-Klasse an. Die Priorität innerhalb der Best-"
"Effort-Klasse wird dynamisch aus der CPU-Nice-Stufe des Prozesses "
"abgeleitet: E/A-Priorität = (CPU-Nice-Klasse + 20) / 5."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"For kernels after 2.6.26 with the CFQ I/O scheduler, a process that has not "
"asked for an I/O priority inherits its CPU scheduling class.  The I/O "
"priority is derived from the CPU nice level of the process (same as before "
"kernel 2.6.26)."
msgstr ""
"Mit Kerneln der Version 2.6.26 oder neuer mit CFQ-E/A-Scheduler erbt ein "
"Prozess, der keine E/A-Priorität angefordert hat, dessen CPU-Scheduling-"
"Klasse. Die E/A-Priorität wird aus der CPU-Nice-Stufe des Prozesses "
"abgeleitet (wie auch in Kernels vor 2.6.26)."

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<Realtime>"
msgstr "B<Realtime>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The RT scheduling class is given first access to the disk, regardless of "
"what else is going on in the system.  Thus the RT class needs to be used "
"with some care, as it can starve other processes.  As with the best-effort "
"class, 8 priority levels are defined denoting how big a time slice a given "
"process will receive on each scheduling window.  This scheduling class is "
"not permitted for an ordinary (i.e., non-root) user."
msgstr ""
"Die Scheduling-Klasse »Realtime« hat Vorrang vor anderen Prozessen bei "
"Festplattenzugriffen, ganz gleich welche anderen Vorgänge im System "
"ablaufen. Deshalb sollte die Realtime-Klasse mit gewisser Vorsicht verwendet "
"werden, weil sie andere Prozesse regelrecht »aushungern« kann. Wie auch bei "
"der Best-Effort-Klasse sind 8 Prioritätsstufen verfügbar, welche angeben, "
"wie groß die Zeitscheibe ist, die ein angegebener Prozess in jedem "
"Scheduling-Zeitfenster erhält. Diese Scheduling-Klasse ist für einen "
"gewöhnlichen Benutzer ohne Administratorrechte verboten."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-c>,B< --class >I<class>"
msgstr "B<-c>,B< --class >I<Klasse>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Specify the name or number of the scheduling class to use; I<0> for none, "
"I<1> for realtime, I<2> for best-effort, I<3> for idle."
msgstr ""
"gibt den Namen oder die Nummer der gewünschten Scheduling-Klasse an. I<0> "
"für keine, I<1> für Realtime, I<2> für Best-Effort, I<3> für Idle."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-n>,B< --classdata >I<level>"
msgstr "B<-n>,B< --classdata >I<Stufe>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Specify the scheduling class data.  This only has an effect if the class "
"accepts an argument.  For realtime and best-effort, I<0-7> are valid data "
"(priority levels), and I<0> represents the highest priority level."
msgstr ""
"gibt die Scheduling-Klassendaten an. Dies ist nur wirksam, wenn die Klasse "
"ein Argument akzeptiert. Für »Realtime« und »Best-Effort« sind I<0-7> "
"möglich (als Prioritätsstufen) und I<0> repräsentiert die höchste "
"Prioritätsstufe."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-p>,B< --pid >I<PID>..."
msgstr "B<-p>,B< --pid >I<PID>…"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Specify the process IDs of running processes for which to get or set the "
"scheduling parameters."
msgstr ""
"gibt die Prozesskennungen (PIDs) der laufenden Prozesse an, für die die "
"Scheduling-Parameter gesetzt oder ermittelt werden sollen."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-P>,B< --pgid >I<PGID>..."
msgstr "B<-P>,B< --pgid >I<PGID>…"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Specify the process group IDs of running processes for which to get or set "
"the scheduling parameters."
msgstr ""
"gibt die Prozessgruppenkennungen (PGIDs) der laufenden Prozesse an, für die "
"die Scheduling-Parameter gesetzt oder ermittelt werden sollen."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-t>,B< --ignore>"
msgstr "B<-t>,B< --ignore>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Ignore failure to set the requested priority.  If I<command> was specified, "
"run it even in case it was not possible to set the desired scheduling "
"priority, which can happen due to insufficient privileges or an old kernel "
"version."
msgstr ""
"ignoriert fehlgeschlagenes Setzen der angeforderten Priorität. Falls ein "
"I<Befehl> angegeben wurde, wird dieser selbst dann ausgeführt, wenn es nicht "
"möglich war, die gewünschte Scheduling-Priorität zu setzen. Dies kann bei "
"unzureichenden Zugriffsrechten oder alten Kernel-Versionen passieren."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-u>,B< --uid >I<UID>..."
msgstr "B<-u>,B< --uid >I<UID>…"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Specify the user IDs of running processes for which to get or set the "
"scheduling parameters."
msgstr ""
"gibt die Benutzerkennungen (UIDs) der laufenden Prozesse an, für die die "
"Scheduling-Parameter gesetzt oder ermittelt werden sollen."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr "B<-V>,B< --version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Display version information and exit."
msgstr "zeigt Versionsinformationen an und beendet das Programm."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "# B<ionice> -c 3 -p 89"
msgstr "# B<ionice> -c 3 -p 89"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Sets process with PID 89 as an idle I/O process."
msgstr "setzt den Prozess mit der PID 89 als Idle-E/A-Prozess."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "# B<ionice> -c 2 -n 0 bash"
msgstr "# B<ionice> -c 2 -n 0 bash"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Runs 'bash' as a best-effort program with highest priority."
msgstr "führt »bash« als Best-Effort-Programm mit höchster Priorität aus."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "# B<ionice> -p 89 91"
msgstr "# B<ionice> -p 89 91"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Prints the class and priority of the processes with PID 89 and 91."
msgstr "gibt die Klasse und Priorität der Prozesse mit den PIDs 89 und 91 aus."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Linux supports I/O scheduling priorities and classes since 2.6.13 with the "
"CFQ I/O scheduler."
msgstr ""
"Linux unterstützt E/A-Scheduling-Prioritäten und -Klassen seit Version "
"2.6.13 mit dem CFQ-E/A-Scheduler."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"Jens Axboe E<lt>jens@axboe.dkE<gt>\n"
"Karel Zak E<lt>kzak@redhat.comE<gt>\n"
msgstr ""
"Jens Axboe E<lt>jens@axboe.dkE<gt>\n"
"Karel Zak E<lt>kzak@redhat.comE<gt>\n"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<ioprio_set>(2)"
msgstr "B<ioprio_set>(2)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The ionice command is part of the util-linux package and is available from "
"https://www.kernel.org/pub/linux/utils/util-linux/."
msgstr ""
"Der Befehl ionice ist Teil des Pakets util-linux und kann von https://www."
"kernel.org/pub/linux/utils/util-linux/ heruntergeladen werden."
