# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Frank Stähr <der-storch-85@gmx.net>, 2013.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2013-2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2019-08-03 17:58+02:00\n"
"PO-Revision-Date: 2016-03-09 21:09+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "VORBISTAGEDIT"
msgstr "VORBISTAGEDIT"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "2006-11-17"
msgstr "17. November 2006"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "1.1.1"
msgstr "1.1.1"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "VORBIS-TOOLS"
msgstr "VORBIS-TOOLS"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-buster debian-unstable
msgid "vorbistagedit - allows batch editing of vorbis comments with an editor"
msgstr ""
"vorbistagedit - ermöglicht die Stapelverarbeitung von Vorbis-Kommentaren mit "
"einem Editor"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"B<vorbistagedit> I<file1> B<[>I<\\|file2>B<\\ \\|[>I<\\|file3\\ ...>B<\\|]>I<"
"\\|>B<]>"
msgstr ""
"B<vorbistagedit> I<Datei1> B<[>I<\\|Datei2>B<\\ \\|[>I<\\|Datei3\\ …>B<"
"\\|]>I<\\|>B<]>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<vorbistagedit> B<[>\\|--versionB<|>-VB<|>-v\\|B<]>"
msgstr "B<vorbistagedit> B<[>\\|--versionB<|>-VB<|>-v\\|B<]>"

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<vorbistagedit> B<[>\\|--helpB<|>-h\\|B<]>"
msgstr "B<vorbistagedit> B<[>\\|--helpB<|>-h\\|B<]>"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"B<vorbistagedit> allows batch editing of vorbis comments with an editor.\\ "
"If more than one OGG Vorbis file is specified, B<vorbistagedit> opens a "
"unique editor for all files given. The supported file extensions for "
"B<vorbistagedit> are .oga, .ogg, .ogv, .ogx, and .spx."
msgstr ""
"B<vorbistagedit> ermöglicht die Stapelverarbeitung von Vorbis-Kommentaren "
"mit einem Editor.\\ Falls mehr als eine OGG-Vorbis-Datei angegeben wird, "
"dann öffnet B<vorbistagedit> nur einen Editor für alle Dateien. Von "
"B<vorbistagedit> werden die Dateiendungen .oga, .ogg, .ogv, .ogx und .spx "
"unterstützt."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: TP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-v, -V, --version >"
msgstr "B<-v, -V, --version >"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Show the version of B<vorbistagedit>."
msgstr "zeigt die Version von B<vorbistagedit> an."

#. type: TP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<-h, --help >"
msgstr "B<-h, --help >"

#. type: Plain text
#: debian-buster debian-unstable
msgid "Show a short help message."
msgstr "zeigt eine kurze Hilfe an."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-buster debian-unstable
msgid "I<vorbiscomment>(1), I<ogginfo>(1)"
msgstr "I<vorbiscomment>(1), I<ogginfo>(1)"

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: TP
#: debian-buster debian-unstable
#, no-wrap
msgid "B<EDITOR>"
msgstr "B<EDITOR>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Defines the default editor.\\ If it's not defined, then I<sensible-"
"editor>(1)  is used."
msgstr ""
"legt den vorgegebenen Editor fest.\\ Falls dieser nicht definiert ist, wird "
"I<sensible-editor>(1) verwendet."

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"B<vorbistagedit> was written by Martin F. Krafft E<lt>vorbistagedit@pobox."
"madduck.netE<gt>."
msgstr ""
"B<vorbistagedit> wurde von Martin F. Krafft E<lt>vorbistagedit@pobox.madduck."
"netE<gt> geschrieben."

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"This manual page was written by Francois Wendling E<lt>frwendling@free."
"frE<gt> for the Debian project (but may be used by others)."
msgstr ""
"Diese Handbuchseite wurde von Francois Wendling E<lt>frwendling@free.frE<gt> "
"für das Debian-Projekt geschrieben (aber darf von anderen verwendet werden)."
