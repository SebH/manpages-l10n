# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Karl Eichwalder <ke@suse.de>
# Lutz Behnke <lutz.behnke@gmx.de>
# Michael Piefel <piefel@debian.org>
# Michael Schmidt <michael@guug.de>
# Dr. Tobias Quathamer <toddy@debian.org>, 2010, 2012, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2020-04-04 20:33+02:00\n"
"PO-Revision-Date: 2019-03-22 22:06+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NICE"
msgstr "NICE"

#. type: TH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "March 2020"
msgstr "März 2020"

#. type: TH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU Coreutils 8.32"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "nice - run a program with modified scheduling priority"
msgstr ""
"nice - ein Programm mit einer veränderten Zeitplanungspriorität ausführen"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<nice> [I<\\,OPTION\\/>] [I<\\,COMMAND \\/>[I<\\,ARG\\/>]...]"
msgstr "B<nice> [I<\\,OPTION\\/>] [I<\\,BEFEHL \\/>[I<\\,ARGUMENT\\/>]…]"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Run COMMAND with an adjusted niceness, which affects process scheduling.  "
"With no COMMAND, print the current niceness.  Niceness values range from "
"B<-20> (most favorable to the process) to 19 (least favorable to the "
"process)."
msgstr ""
"BEFEHL mit angepasstem Nice-Wert ausführen, die beeinflusst, wann die "
"Prozessorzeit zugeteilt wird. Ohne BEFEHL wird der aktuelle Nice-Wert "
"ausgeben. Die Werte für den Nice-Wert reichen dabei von B<-20> "
"(vorteilhafteste Zuteilung für den Prozess) bis 19 (unvorteilhafteste "
"Zuteilung für den Prozess)."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Die obligatorischen Argumente für Optionen sind für deren Kurz- und Langform "
"gleich."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-n>, B<--adjustment>=I<\\,N\\/>"
msgstr "B<-n>, B<--adjustment>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "add integer N to the niceness (default 10)"
msgstr "Die Ganzzahl N zum Nice-Wert addieren (Voreinstellung 10)"

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "display this help and exit"
msgstr "zeigt Hilfeinformationen an und beendet das Programm."

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "output version information and exit"
msgstr "gibt Versionsinformationen aus und beendet das Programm."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"NOTE: your shell may have its own version of nice, which usually supersedes "
"the version described here.  Please refer to your shell's documentation for "
"details about the options it supports."
msgstr ""
"HINWEIS: Ihre Shell hat eventuell eine eigene Version von nice, die "
"üblicherweise die hier beschriebene Version ersetzt. Bitte lesen Sie die "
"Dokumentation Ihrer Shell für Details über die Optionen, die sie unterstützt."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Written by David MacKenzie."
msgstr "Geschrieben von David MacKenzie."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Onlinehilfe für GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Melden Sie Fehler in der Übersetzung an E<lt>https://translationproject.org/"
"team/de.htmlE<gt>"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Lizenz GPLv3+: GNU GPL "
"Version 3 oder neuer E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dies ist freie Software: Sie können sie verändern und weitergeben. Es gibt "
"KEINE GARANTIE, soweit gesetzlich zulässig."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "nice(2), renice(1)"
msgstr "nice(2), renice(1)"

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/niceE<gt>"
msgstr ""
"Vollständige Dokumentation unter: E<lt>https://www.gnu.org/software/"
"coreutils/niceE<gt>"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) nice invocation\\(aq"
msgstr "oder lokal verfügbar mit: info \\(aq(coreutils) nice invocation\\(aq"

#. type: TH
#: debian-buster
#, no-wrap
msgid "February 2019"
msgstr "Februar 2019"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr "GNU Coreutils 8.30"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Report nice translation bugs to E<lt>https://translationproject.org/team/"
"E<gt>"
msgstr ""
"Berichten Sie Fehler in der Übersetzung von nice an E<lt>https://"
"translationproject.org/team/de.htmlE<gt>"

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2018 Free Software Foundation, Inc. Lizenz GPLv3+: GNU GPL "
"Version 3 oder neuer E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/niceE<gt>"
msgstr ""
"Vollständige Dokumentation unter: E<lt>https://www.gnu.org/software/"
"coreutils/niceE<gt>"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr "August 2019"
