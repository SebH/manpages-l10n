# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-03-07 13:32+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "MINGETTY"
msgstr ""

#. type: TH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "6 Apr 1996"
msgstr ""

#. type: TH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Debian-Local"
msgstr ""

#. type: TH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: SH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "mingetty - minimal getty for consoles"
msgstr ""

#. type: SH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<mingetty> [--noclear] [--nonewline] [--noissue] [--nohangup] [--"
"nohostname] [--long-hostname] [--loginprog=/bin/login] [--nice=10] [--"
"delay=5] [--chdir=/home] [--chroot=/chroot] [--autologin username] [--"
"loginpause] I<tty>"
msgstr ""

#. type: SH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<mingetty> is a minimal getty for use on virtual consoles.  Unlike "
"B<agetty>(8), B<mingetty> is not suitable for serial lines.  I recommend "
"using B<mgetty>(8)  for this purpose."
msgstr ""

#. type: SH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--noclear>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Do not clear the screen before prompting for the login name (the screen is "
"normally cleared)."
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--nonewline>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Do not print a newline before writing out /etc/issue."
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--noissue>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Do not output /etc/issue."
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--nohangup>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Do not call vhangup() to disable writing to this tty by other applications."
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--nohostname>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Do not print the hostname before the login prompt."
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--long-hostname>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"By default the hostname is only printed until the first dot.  With this "
"option enabled, the full text from gethostname() is shown."
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--loginprog /bin/login>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Change the login app."
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--nice 10>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Change the priority by calling nice()."
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--delay 5>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Sleep this many seconds after startup of mingetty."
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--chdir /home>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Change into this directory before calling the login prog."
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--chroot /chroot>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Call chroot() with this directory name."
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--autologin username>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Log the specified user automatically in without asking for a login name and "
"password. Check the -f option from B</bin/login> for this."
msgstr ""

#. type: TP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--loginpause>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Wait for any key before dropping to the login prompt.  Can be combined with "
"B<--autologin> to save memory by lazily spawning shells."
msgstr ""

#. type: SH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ISSUE ESCAPES"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<mingetty> recognizes the following escapes sequences which might be "
"embedded in the I</etc/issue> file:"
msgstr ""

#. type: IP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<\\ed>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "insert current day (localtime),"
msgstr ""

#. type: IP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<\\el>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "insert line on which B<mingetty> is running,"
msgstr ""

#. type: IP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<\\em>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "inserts machine architecture (uname -m),"
msgstr ""

#. type: IP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<\\en>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "inserts machine's network node hostname (uname -n),"
msgstr ""

#. type: IP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<\\eo>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "inserts domain name,"
msgstr ""

#. type: IP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<\\er>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "inserts operating system release (uname -r),"
msgstr ""

#. type: IP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<\\et>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "insert current time (localtime),"
msgstr ""

#. type: IP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<\\es>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "inserts operating system name,"
msgstr ""

#. type: IP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<\\eu>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"resp. B<\\eU> the current number of users which are currently logged in.  "
"\\eU inserts \"I<n> users\", where as \\eu only inserts \"I<n>\"."
msgstr ""

#. type: IP
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<\\ev>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "inserts operating system version (uname -v)."
msgstr ""

#. type: SH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"\"B<Linux\\ eos\\ i386\\ #1\\ Tue\\ Mar\\ 19\\ 21:54:09\\ MET\\ 1996>\" was "
"produced by putting \"B<\\es\\ \\en\\ \\em\\ \\ev>\" into I</etc/issue>."
msgstr ""

#. type: SH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "I</etc/issue>, I</var/run/utmp>."
msgstr ""

#. type: SH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<mgetty>(8), B<agetty>(8)."
msgstr ""

#. type: SH
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Copyright \\(co 1996 Florian La Roche E<lt>laroche@redhat.comE<gt>.  Man-"
"page written by David Frey E<lt>David.Frey@eos.lugs.chE<gt> and Florian La "
"Roche."
msgstr ""
