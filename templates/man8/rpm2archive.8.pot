# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-11-24 15:40+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "rpm2archive"
msgstr ""

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "11 January 2001"
msgstr ""

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "Red Hat, Inc."
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"rpm2archive - Extract archive archive from RPM Package Manager (RPM) package."
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<rpm2archive> [filename]"
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"B<rpm2archive> converts the .rpm file specified as a single argument to a "
"archive archive on standard out. If a '-' argument is given, an rpm stream "
"is read from standard in."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<rpm2archive rpm-1.1-1.i386.rpm>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<rpm2archive - E<lt> glint-1.0-1.i386.rpm>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "B<rpm2archive glint-1.0-1.i386.rpm | tar tvf ->"
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid "I<rpm>(8)"
msgstr ""

#. type: SH
#: debian-buster debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
#, no-wrap
msgid "Erik Troan E<lt>ewt@redhat.comE<gt>\n"
msgstr ""
