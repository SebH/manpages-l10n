# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-10 17:54+02:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB-MKCONFIG"
msgstr ""

#. type: TH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Wed Feb 26 2014"
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<grub-mkconfig> \\(em Generate a GRUB configuration file."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<grub-mkconfig> [-o | --output=I<FILE>]"
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<grub-mkconfig> generates a configuration file for GRUB."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--output>=I<FILE>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "Write generated output to I<FILE>."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--no-grubenv-update>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "Do not update variables in the grubenv file."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<info grub>"
msgstr ""
