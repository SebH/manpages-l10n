# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-19 13:07+02:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DNSSEC-TRUST-ANCHORS\\&.D"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "systemd 245"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "dnssec-trust-anchors.d"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"dnssec-trust-anchors.d, systemd.positive, systemd.negative - DNSSEC trust "
"anchor configuration files"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "/etc/dnssec-trust-anchors\\&.d/*\\&.positive"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "/run/dnssec-trust-anchors\\&.d/*\\&.positive"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "/usr/lib/dnssec-trust-anchors\\&.d/*\\&.positive"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "/etc/dnssec-trust-anchors\\&.d/*\\&.negative"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "/run/dnssec-trust-anchors\\&.d/*\\&.negative"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "/usr/lib/dnssec-trust-anchors\\&.d/*\\&.negative"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The DNSSEC trust anchor configuration files define positive and negative "
"trust anchors B<systemd-resolved.service>(8)  bases DNSSEC integrity proofs "
"on\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "POSITIVE TRUST ANCHORS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Positive trust anchor configuration files contain DNSKEY and DS resource "
"record definitions to use as base for DNSSEC integrity proofs\\&. See "
"\\m[blue]B<RFC 4035, Section 4\\&.4>\\m[]\\&\\s-2\\u[1]\\d\\s+2 for more "
"information about DNSSEC trust anchors\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Positive trust anchors are read from files with the suffix \\&.positive "
"located in /etc/dnssec-trust-anchors\\&.d/, /run/dnssec-trust-anchors\\&.d/ "
"and /usr/lib/dnssec-trust-anchors\\&.d/\\&. These directories are searched "
"in the specified order, and a trust anchor file of the same name in an "
"earlier path overrides a trust anchor files in a later path\\&. To disable a "
"trust anchor file shipped in /usr/lib/dnssec-trust-anchors\\&.d/ it is "
"sufficient to provide an identically-named file in /etc/dnssec-trust-anchors"
"\\&.d/ or /run/dnssec-trust-anchors\\&.d/ that is either empty or a symlink "
"to /dev/null (\"masked\")\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Positive trust anchor files are simple text files resembling DNS zone files, "
"as documented in \\m[blue]B<RFC 1035, Section 5>\\m[]\\&\\s-2\\u[2]\\d\\s"
"+2\\&. One DS or DNSKEY resource record may be listed per line\\&. Empty "
"lines and lines starting with a semicolon (\";\") are ignored and considered "
"comments\\&. A DS resource record is specified like in the following example:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\&. IN DS 19036 8 2 49aac11d7b6f6446702e54a1607371607a1a41855200fd2ce1cdde32f24e8fb5\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The first word specifies the domain, use \"\\&.\" for the root domain\\&. "
"The domain may be specified with or without trailing dot, which is "
"considered equivalent\\&. The second word must be \"IN\" the third word \"DS"
"\"\\&. The following words specify the key tag, signature algorithm, digest "
"algorithm, followed by the hex-encoded key fingerprint\\&. See "
"\\m[blue]B<RFC 4034, Section 5>\\m[]\\&\\s-2\\u[3]\\d\\s+2 for details about "
"the precise syntax and meaning of these fields\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Alternatively, DNSKEY resource records may be used to define trust anchors, "
"like in the following example:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\&. IN DNSKEY 257 3 8 AwEAAagAIKlVZrpC6Ia7gEzahOR+9W29euxhJhVVLOyQbSEW0O8gcCjFFVQUTf6v58fLjwBd0YI0EzrAcQqBGCzh/RStIoO8g0NfnfL2MTJRkxoXbfDaUeVPQuYEhg37NZWAJQ9VnMVDxP/VHL496M/QZxkjf5/Efucp2gaDX6RS6CXpoY68LsvPVjR0ZSwzz1apAzvN9dlzEheX7ICJBBtuA6G3LQpzW5hOA2hzCTMjJPJ8LbqF6dsV6DoBQzgul0sGIcGOYl7OyQdXfZ57relSQageu+ipAdTTJ25AsRTAoub8ONGcLmqrAmRLKBP1dfwhYB4N7knNnulqQxA+Uk1ihz0=\n"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The first word specifies the domain again, the second word must be \"IN\", "
"followed by \"DNSKEY\"\\&. The subsequent words encode the DNSKEY flags, "
"protocol and algorithm fields, followed by the key data encoded in "
"Base64\\&. See \\m[blue]B<RFC 4034, Section 2>\\m[]\\&\\s-2\\u[4]\\d\\s+2 "
"for details about the precise syntax and meaning of these fields\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"If multiple DS or DNSKEY records are defined for the same domain (possibly "
"even in different trust anchor files), all keys are used and are considered "
"equivalent as base for DNSSEC proofs\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Note that systemd-resolved will automatically use a built-in trust anchor "
"key for the Internet root domain if no positive trust anchors are defined "
"for the root domain\\&. In most cases it is hence unnecessary to define an "
"explicit key with trust anchor files\\&. The built-in key is disabled as "
"soon as at least one trust anchor key for the root domain is defined in "
"trust anchor files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"It is generally recommended to encode trust anchors in DS resource records, "
"rather than DNSKEY resource records\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"If a trust anchor specified via a DS record is found revoked it is "
"automatically removed from the trust anchor database for the runtime\\&. See "
"\\m[blue]B<RFC 5011>\\m[]\\&\\s-2\\u[5]\\d\\s+2 for details about revoked "
"trust anchors\\&. Note that systemd-resolved will not update its trust "
"anchor database from DNS servers automatically\\&. Instead, it is "
"recommended to update the resolver software or update the new trust anchor "
"via adding in new trust anchor files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The current DNSSEC trust anchor for the Internet\\*(Aqs root domain is "
"available at the \\m[blue]B<IANA Trust Anchor and Keys>\\m[]\\&\\s-2\\u[6]\\d"
"\\s+2 page\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NEGATIVE TRUST ANCHORS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Negative trust anchors define domains where DNSSEC validation shall be "
"turned off\\&. Negative trust anchor files are found at the same location as "
"positive trust anchor files, and follow the same overriding rules\\&. They "
"are text files with the \\&.negative suffix\\&. Empty lines and lines whose "
"first character is \";\" are ignored\\&. Each line specifies one domain name "
"which is the root of a DNS subtree where validation shall be disabled\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Negative trust anchors are useful to support private DNS subtrees that are "
"not referenced from the Internet DNS hierarchy, and not signed\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"\\m[blue]B<RFC 7646>\\m[]\\&\\s-2\\u[7]\\d\\s+2 for details on negative "
"trust anchors\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"If no negative trust anchor files are configured a built-in set of well-"
"known private DNS zone domains is used as negative trust anchors\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"It is also possibly to define per-interface negative trust anchors using the "
"I<DNSSECNegativeTrustAnchors=> setting in B<systemd.network>(5)  files\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"B<systemd>(1), B<systemd-resolved.service>(8), B<resolved.conf>(5), "
"B<systemd.network>(5)"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "RFC 4035, Section 4.4"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "\\%https://tools.ietf.org/html/rfc4035#section-4.4"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid " 2."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "RFC 1035, Section 5"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "\\%https://tools.ietf.org/html/rfc1035#section-5"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid " 3."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "RFC 4034, Section 5"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "\\%https://tools.ietf.org/html/rfc4034#section-5"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid " 4."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "RFC 4034, Section 2"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "\\%https://tools.ietf.org/html/rfc4034#section-2"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid " 5."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "RFC 5011"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "\\%https://tools.ietf.org/html/rfc5011"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid " 6."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "IANA Trust Anchor and Keys"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "\\%https://data.iana.org/root-anchors/root-anchors.xml"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid " 7."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "RFC 7646"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "\\%https://tools.ietf.org/html/rfc7646"
msgstr ""

#. type: TH
#: debian-buster
#, no-wrap
msgid "systemd 244"
msgstr ""
