# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-04 20:32+02:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "LESSKEY"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide
#, no-wrap
msgid "Version 551: 11 Jun 2019"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "lesskey - specify key bindings for less"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<lesskey [-o output] [--] [input]>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<lesskey [--output=output] [--] [input]>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<lesskey -V>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<lesskey --version>"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"I<Lesskey> is used to specify a set of key bindings to be used by I<less.> "
"The input file is a text file which describes the key bindings.  If the "
"input file is \"-\", standard input is read.  If no input file is specified, "
"a standard filename is used as the name of the input file, which depends on "
"the system being used: On Unix systems, $HOME/.lesskey is used; on MS-DOS "
"systems, $HOME/_lesskey is used; and on OS/2 systems $HOME/lesskey.ini is "
"used, or $INIT/lesskey.ini if $HOME is undefined.  The output file is a "
"binary file which is used by I<less.> If no output file is specified, and "
"the environment variable LESSKEY is set, the value of LESSKEY is used as the "
"name of the output file.  Otherwise, a standard filename is used as the name "
"of the output file, which depends on the system being used: On Unix and OS-9 "
"systems, $HOME/.less is used; on MS-DOS systems, $HOME/_less is used; and on "
"OS/2 systems, $HOME/less.ini is used, or $INIT/less.ini if $HOME is "
"undefined.  If the output file already exists, I<lesskey> will overwrite it."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The -V or --version option causes I<lesskey> to print its version number and "
"immediately exit.  If -V or --version is present, other options and "
"arguments are ignored."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The input file consists of one or more I<sections.> Each section starts with "
"a line that identifies the type of section.  Possible sections are:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "#command"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Defines new command keys."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "#line-edit"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Defines new line-editing keys."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "#env"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Defines environment variables."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Blank lines and lines which start with a pound sign (#) are ignored, except "
"for the special section header lines."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COMMAND SECTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The command section begins with the line"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"If the command section is the first section in the file, this line may be "
"omitted.  The command section consists of lines of the form:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"  I<string> E<lt>whitespaceE<gt> I<action> [extra-string] E<lt>newlineE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Whitespace is any sequence of one or more spaces and/or tabs.  The I<string> "
"is the command key(s) which invoke the action.  The I<string> may be a "
"single command key, or a sequence of up to 15 keys.  The I<action> is the "
"name of the less action, from the list below.  The characters in the "
"I<string> may appear literally, or be prefixed by a caret to indicate a "
"control key.  A backslash followed by one to three octal digits may be used "
"to specify a character by its octal value.  A backslash followed by certain "
"characters specifies input characters as follows:"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\eb"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "BACKSPACE"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\ee"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "ESCAPE"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\en"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "NEWLINE"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\er"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "RETURN"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\et"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "TAB"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\eku"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "UP ARROW"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\ekd"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "DOWN ARROW"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\ekr"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "RIGHT ARROW"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\ekl"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "LEFT ARROW"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\ekU"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "PAGE UP"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\ekD"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "PAGE DOWN"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\ekh"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "HOME"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\eke"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "END"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\ekx"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "DELETE"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"A backslash followed by any other character indicates that character is to "
"be taken literally.  Characters which must be preceded by backslash include "
"caret, space, tab and the backslash itself."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"An action may be followed by an \"extra\" string.  When such a command is "
"entered while running I<less,> the action is performed, and then the extra "
"string is parsed, just as if it were typed in to I<less.> This feature can "
"be used in certain cases to extend the functionality of a command.  For "
"example, see the \"{\" and \":t\" commands in the example below.  The extra "
"string has a special meaning for the \"quit\" action: when I<less> quits, "
"first character of the extra string is used as its exit status."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The following input file describes the set of default command keys used by "
"less:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"\t#command\n"
"\t\\er\t\tforw-line \n"
"\t\\en\t\tforw-line \n"
"\te\t\tforw-line \n"
"\tj\t\tforw-line \n"
"\t\\ekd\t\tforw-line\n"
"\t^E\t\tforw-line \n"
"\t^N\t\tforw-line \n"
"\tk\t\tback-line \n"
"\ty\t\tback-line \n"
"\t^Y\t\tback-line \n"
"\t^K\t\tback-line \n"
"\t^P\t\tback-line \n"
"\tJ\t\tforw-line-force \n"
"\tK\t\tback-line-force \n"
"\tY\t\tback-line-force \n"
"\td\t\tforw-scroll \n"
"\t^D\t\tforw-scroll \n"
"\tu\t\tback-scroll \n"
"\t^U\t\tback-scroll \n"
"\t\\e40\t\tforw-screen \n"
"\tf\t\tforw-screen \n"
"\t^F\t\tforw-screen \n"
"\t^V\t\tforw-screen \n"
"\t\\ekD\t\tforw-screen\n"
"\tb\t\tback-screen \n"
"\t^B\t\tback-screen \n"
"\t\\eev\t\tback-screen \n"
"\t\\ekU\t\tback-screen\n"
"\tz\t\tforw-window \n"
"\tw\t\tback-window \n"
"\t\\ee\\e40\t\tforw-screen-force\n"
"\tF\t\tforw-forever \n"
"\t\\eeF\t\tforw-until-hilite\n"
"\tR\t\trepaint-flush \n"
"\tr\t\trepaint \n"
"\t^R\t\trepaint \n"
"\t^L\t\trepaint \n"
"\t\\eeu\t\tundo-hilite\n"
"\tg\t\tgoto-line \n"
"\t\\ekh\t\tgoto-line\n"
"\tE<lt>\t\tgoto-line \n"
"\t\\eeE<lt>\t\tgoto-line \n"
"\tp\t\tpercent \n"
"\t%\t\tpercent \n"
"\t\\ee[\t\tleft-scroll\n"
"\t\\ee]\t\tright-scroll\n"
"\t\\ee(\t\tleft-scroll\n"
"\t\\ee)\t\tright-scroll\n"
"\t\\ekl\t\tleft-scroll\n"
"\t\\ekr\t\tright-scroll\n"
"\t\\ee{\t\tno-scroll\n"
"\t\\ee}\t\tend-scroll\n"
"\t{\t\tforw-bracket {}\n"
"\t}\t\tback-bracket {}\n"
"\t(\t\tforw-bracket ()\n"
"\t)\t\tback-bracket ()\n"
"\t[\t\tforw-bracket []\n"
"\t]\t\tback-bracket []\n"
"\t\\ee^F\t\tforw-bracket \n"
"\t\\ee^B\t\tback-bracket \n"
"\tG\t\tgoto-end \n"
"\t\\eeE<gt>\t\tgoto-end \n"
"\tE<gt>\t\tgoto-end \n"
"\t\\eke\t\tgoto-end\n"
"\t\\eeG\t\tgoto-end-buffered\n"
"\t=\t\tstatus \n"
"\t^G\t\tstatus \n"
"\t:f\t\tstatus \n"
"\t/\t\tforw-search \n"
"\t?\t\tback-search \n"
"\t\\ee/\t\tforw-search *\n"
"\t\\ee?\t\tback-search *\n"
"\tn\t\trepeat-search \n"
"\t\\een\t\trepeat-search-all \n"
"\tN\t\treverse-search \n"
"\t\\eeN\t\treverse-search-all \n"
"\t&\t\tfilter\n"
"\tm\t\tset-mark \n"
"\tM\t\tset-mark-bottom\n"
"\t\\eem\t\tclear-mark\n"
"\t'\t\tgoto-mark \n"
"\t^X^X\t\tgoto-mark \n"
"\tE\t\texamine \n"
"\t:e\t\texamine \n"
"\t^X^V\t\texamine \n"
"\t:n\t\tnext-file \n"
"\t:p\t\tprev-file \n"
"\tt\t\tnext-tag\n"
"\tT\t\tprev-tag\n"
"\t:x\t\tindex-file \n"
"\t:d\t\tremove-file\n"
"\t-\t\ttoggle-option \n"
"\t:t\t\ttoggle-option t\n"
"\ts\t\ttoggle-option o\n"
"\t_\t\tdisplay-option \n"
"\t|\t\tpipe \n"
"\tv\t\tvisual \n"
"\t!\t\tshell \n"
"\t+\t\tfirstcmd \n"
"\tH\t\thelp \n"
"\th\t\thelp \n"
"\tV\t\tversion \n"
"\t0\t\tdigit\n"
"\t1\t\tdigit\n"
"\t2\t\tdigit\n"
"\t3\t\tdigit\n"
"\t4\t\tdigit\n"
"\t5\t\tdigit\n"
"\t6\t\tdigit\n"
"\t7\t\tdigit\n"
"\t8\t\tdigit\n"
"\t9\t\tdigit\n"
"\tq\t\tquit \n"
"\tQ\t\tquit \n"
"\t:q\t\tquit \n"
"\t:Q\t\tquit \n"
"\tZZ\t\tquit \n"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "PRECEDENCE"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Commands specified by I<lesskey> take precedence over the default commands.  "
"A default command key may be disabled by including it in the input file with "
"the action \"invalid\".  Alternatively, a key may be defined to do nothing "
"by using the action \"noaction\".  \"noaction\" is similar to \"invalid\", "
"but I<less> will give an error beep for an \"invalid\" command, but not for "
"a \"noaction\" command.  In addition, ALL default commands may be disabled "
"by adding this control line to the input file:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "#stop"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This will cause all default commands to be ignored.  The #stop line should "
"be the last line in that section of the file."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Be aware that #stop can be dangerous.  Since all default commands are "
"disabled, you must provide sufficient commands before the #stop line to "
"enable all necessary actions.  For example, failure to provide a \"quit\" "
"command can lead to frustration."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "LINE EDITING SECTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The line-editing section begins with the line:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This section specifies new key bindings for the line editing commands, in a "
"manner similar to the way key bindings for ordinary commands are specified "
"in the #command section.  The line-editing section consists of a list of "
"keys and actions, one per line as in the example below."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The following input file describes the set of default line-editing keys used "
"by less:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"\t#line-edit\n"
"\t\\et\t    \tforw-complete\n"
"\t\\e17\t\tback-complete\n"
"\t\\ee\\et\t\tback-complete\n"
"\t^L\t\texpand\n"
"\t^V\t\tliteral\n"
"\t^A\t\tliteral\n"
"   \t\\eel\t\tright\n"
"\t\\ekr\t\tright\n"
"\t\\eeh\t\tleft\n"
"\t\\ekl\t\tleft\n"
"\t\\eeb\t\tword-left\n"
"\t\\ee\\ekl\tword-left\n"
"\t\\eew\t\tword-right\n"
"\t\\ee\\ekr\tword-right\n"
"\t\\eei\t\tinsert\n"
"\t\\eex\t\tdelete\n"
"\t\\ekx\t\tdelete\n"
"\t\\eeX\t\tword-delete\n"
"\t\\eekx\t\tword-delete\n"
"\t\\ee\\eb\t\tword-backspace\n"
"\t\\ee0\t\thome\n"
"\t\\ekh\t\thome\n"
"\t\\ee$\t\tend\n"
"\t\\eke\t\tend\n"
"\t\\eek\t\tup\n"
"\t\\eku\t\tup\n"
"\t\\eej\t\tdown\n"
"\t^G\t\tabort\n"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "LESS ENVIRONMENT VARIABLES"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The environment variable section begins with the line"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Following this line is a list of environment variable assignments.  Each "
"line consists of an environment variable name, an equals sign (=)  and the "
"value to be assigned to the environment variable.  White space before and "
"after the equals sign is ignored.  Variables assigned in this way are "
"visible only to I<less.> If a variable is specified in the system "
"environment and also in a lesskey file, the value in the lesskey file takes "
"precedence.  Although the lesskey file can be used to override variables set "
"in the environment, the main purpose of assigning variables in the lesskey "
"file is simply to have all I<less> configuration information stored in one "
"file."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The following input file sets the -i option whenever I<less> is run, and "
"specifies the character set to be \"latin1\":"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"\t#env\n"
"\tLESS = -i\n"
"\tLESSCHARSET = latin1\n"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "less(1)"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "WARNINGS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"On MS-DOS and OS/2 systems, certain keys send a sequence of characters which "
"start with a NUL character (0).  This NUL character should be represented as "
"\\e340 in a lesskey file."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide
msgid "Copyright (C) 1984-2019 Mark Nudelman"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"less is part of the GNU project and is free software.  You can redistribute "
"it and/or modify it under the terms of either (1) the GNU General Public "
"License as published by the Free Software Foundation; or (2) the Less "
"License.  See the file README in the less distribution for more details "
"regarding redistribution.  You should have received a copy of the GNU "
"General Public License along with the source for less; see the file "
"COPYING.  If not, write to the Free Software Foundation, 59 Temple Place, "
"Suite 330, Boston, MA 02111-1307, USA.  You should also have received a copy "
"of the Less License; see the file LICENSE."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"less is distributed in the hope that it will be useful, but WITHOUT ANY "
"WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS "
"FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more "
"details."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Mark Nudelman"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Send bug reports or comments to E<lt>bug-less@gnu.orgE<gt>."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Version 557: 21 Mar 2020"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "Copyright (C) 1984-2020 Mark Nudelman"
msgstr ""
