# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-10 20:38+02:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB-EDITENV"
msgstr ""

#. type: TH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Wed Feb 26 2014"
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<grub-editenv> \\(em Manage the GRUB environment block."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<grub-editenv> [-v | --verbose] [I<FILE>]"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "E<lt>create | list | set I<NAME>=I<VALUE> | unset I<NAME>E<gt>"
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"B<grub-editenv> is a command line tool to manage GRUB's stored environment."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--verbose>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "Print verbose messages."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FILE>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "File name to use for grub environment.  Default is /boot/grub/grubenv ."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COMMANDS"
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<create>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "Create a blank environment block file."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<list>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "List the current variables."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<set> [I<NAME>=I<VALUE> ...]"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "Set variables."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<unset [>I<NAME> ...]"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "Delete variables."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<info grub>"
msgstr ""
