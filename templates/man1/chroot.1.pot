# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-04 20:30+02:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "CHROOT"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "March 2020"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "chroot - run command or interactive shell with special root directory"
msgstr ""

#. type: SH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"B<chroot> [I<\\,OPTION\\/>] I<\\,NEWROOT \\/>[I<\\,COMMAND \\/>[I<\\,ARG\\/"
">]...]"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "B<chroot> I<\\,OPTION\\/>"
msgstr ""

#. type: SH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "Run COMMAND with root directory set to NEWROOT."
msgstr ""

#. type: TP
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--groups>=I<\\,G_LIST\\/>"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "specify supplementary groups as g1,g2,..,gN"
msgstr ""

#. type: TP
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--userspec>=I<\\,USER\\/>:GROUP"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "specify user and group (ID or name) to use"
msgstr ""

#. type: TP
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--skip-chdir>"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "do not change working directory to '/'"
msgstr ""

#. type: TP
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "display this help and exit"
msgstr ""

#. type: TP
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "output version information and exit"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"If no command is given, run '\"$SHELL\" B<-i>' (default: '/bin/sh B<-i>')."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "Written by Roland McGrath."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#. type: SH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "chroot(2)"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/chrootE<gt>"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) chroot invocation\\(aq"
msgstr ""
