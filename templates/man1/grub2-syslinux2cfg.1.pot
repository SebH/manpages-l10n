# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-10 20:43+02:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB-SYSLINUX2CFG"
msgstr ""

#. type: TH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Wed Feb 26 2014"
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"B<grub-syslinux2cfg> \\(em Transform a syslinux config file into a GRUB "
"config."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"B<grub-syslinux2cfg> [-c | --cwd=DIRI<] [-r | --root=DIR>] [-v | --verbose]"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "[-t | --target-root=I<DIR>] [-T | --target-cwd=I<DIR>]"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "[-o | --output=I<FILE>] [[-i | --isolinux] |"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid " [-s | --syslinux] |\n"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid " [-p | --pxelinux]] I<FILE>\n"
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"B<grub-syslinux2cfg> builds a GRUB configuration file out of an existing "
"syslinux configuration file."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "--cwd=I<DIR>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"Set I<DIR> as syslinux's working directory.  The default is to use the "
"parent directory of the input file."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "--root=I<DIR>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"Set I<DIR> as the root directory of the syslinux disk.  The default value is "
"\"/\"."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "--verbose"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "Print verbose messages."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "--target-root=I<DIR>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"Root directory as it will be seen at runtime.  The default value is \"/\"."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "--target-cwd=I<DIR>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"Working directory of syslinux as it will be seen at runtime.  The default "
"value is the parent directory of the input file."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "--output=I<FILE>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"Write the new config file to I<FILE>.  The default value is standard output."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "--isolinux"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "Assume that the input file is an isolinux configuration file."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "--pxelinux"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "Assume that the input file is a pxelinux configuration file."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "--syslinux"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "Assume that the input file is a syslinux configuration file."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<info grub>"
msgstr ""
