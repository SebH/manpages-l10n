# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-03-14 21:45+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "BOOTCTL"
msgstr ""

#. type: TH
#: archlinux debian-unstable mageia-cauldron
#, no-wrap
msgid "systemd 245"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "bootctl"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "bootctl - Control the firmware and boot manager settings"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<bootctl> [OPTIONS...] {COMMAND}"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<bootctl> can check the EFI boot loader status, list available boot loaders "
"and boot loader entries, and install, update, or remove the B<systemd-"
"boot>(7)  boot loader on the current system\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "COMMANDS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<status>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Shows brief information about the system firmware, the boot loader that was "
"used to boot the system, the boot loaders currently available in the ESP, "
"the boot loaders listed in the firmware\\*(Aqs list of boot loaders and the "
"current default boot loader entry\\&. If no command is specified, this is "
"the implied default\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<install>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Installs B<systemd-boot> into the EFI system partition\\&. A copy of "
"B<systemd-boot> will be stored as the EFI default/fallback loader at I<ESP>/"
"EFI/BOOT/BOOT*\\&.EFI\\&. The boot loader is then added to the top of the "
"firmware\\*(Aqs boot loader list\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<update>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Updates all installed versions of B<systemd-boot>(7), if the available "
"version is newer than the version installed in the EFI system partition\\&. "
"This also includes the EFI default/fallback loader at I<ESP>/EFI/BOOT/BOOT*"
"\\&.EFI\\&. The boot loader is then added to end of the firmware\\*(Aqs boot "
"loader list if missing\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<remove>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Removes all installed versions of B<systemd-boot> from the EFI system "
"partition and the firmware\\*(Aqs boot loader list\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<is-installed>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Checks whether B<systemd-boot> is installed in the ESP\\&. Note that a "
"single ESP might host multiple boot loaders; this hence checks whether "
"B<systemd-boot> is one (of possibly many) installed boot loaders \\(em and "
"neither whether it is the default nor whether it is registered in any EFI "
"variables\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<random-seed>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Generates a random seed and stores it in the EFI System Partition, for use "
"by the B<systemd-boot> boot loader\\&. Also, generates a random \\*(Aqsystem "
"token\\*(Aq and stores it persistently as an EFI variable, if one has not "
"been set before\\&. If the boot loader finds the random seed in the ESP and "
"the system token in the EFI variable it will derive a random seed to pass to "
"the OS and a new seed to store in the ESP from the combination of both\\&. "
"The random seed passed to the OS is credited to the kernel\\*(Aqs entropy "
"pool by the system manager during early boot, and permits userspace to boot "
"up with an entropy pool fully initialized very early on\\&. Also see "
"B<systemd-boot-system-token.service>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"See \\m[blue]B<Random Seeds>\\m[]\\&\\s-2\\u[1]\\d\\s+2 for further "
"information\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<systemd-efi-options> [I<VALUE>]"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"When called without the optional argument, prints the current value of the "
"\"SystemdOptions\" EFI variable\\&. When called with an argument, sets the "
"variable to that value\\&. See B<systemd>(1)  for the meaning of that "
"variable\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<list>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Shows all available boot loader entries implementing the \\m[blue]B<Boot "
"Loader Specification>\\m[]\\&\\s-2\\u[2]\\d\\s+2, as well as any other "
"entries discovered or automatically generated by the boot loader\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<set-default> I<ID>, B<set-oneshot> I<ID>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Sets the default boot loader entry\\&. Takes a single boot loader entry ID "
"string as argument\\&. The B<set-oneshot> command will set the default entry "
"only for the next boot, the B<set-default> will set it persistently for all "
"future boots\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "The following options are understood:"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<--esp-path=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Path to the EFI System Partition (ESP)\\&. If not specified, /efi/, /boot/, "
"and /boot/efi/ are checked in turn\\&. It is recommended to mount the ESP "
"to /efi/, if possible\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<--boot-path=>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Path to the Extended Boot Loader partition, as defined in the "
"\\m[blue]B<Boot Loader Specification>\\m[]\\&\\s-2\\u[2]\\d\\s+2\\&. If not "
"specified, /boot/ is checked\\&. It is recommended to mount the Extended "
"Boot Loader partition to /boot/, if possible\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<-p>, B<--print-esp-path>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This option modifies the behaviour of B<status>\\&. Only prints the path to "
"the EFI System Partition (ESP) to standard output and exits\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<-x>, B<--print-boot-path>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"This option modifies the behaviour of B<status>\\&. Only prints the path to "
"the Extended Boot Loader partition if it exists, and the path to the ESP "
"otherwise to standard output and exit\\&. This command is useful to "
"determine where to place boot loader entries, as they are preferably placed "
"in the Extended Boot Loader partition if it exists and in the ESP otherwise"
"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Boot Loader Specification Type #1 entries should generally be placed in the "
"directory \"$(bootctl -x)/loader/entries/\"\\&. Existence of that directory "
"may also be used as indication that boot loader entry support is available "
"on the system\\&. Similarly, Boot Loader Specification Type #2 entries "
"should be placed in the directory \"$(bootctl -x)/EFI/Linux/\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Note that this option (similar to the B<--print-booth-path> option mentioned "
"above), is available independently from the boot loader used, i\\&.e\\&. "
"also without B<systemd-boot> being installed\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<--no-variables>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Do not touch the firmware\\*(Aqs boot loader list stored in EFI variables\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<--graceful>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Ignore failure when the EFI System Partition cannot be found, or when EFI "
"variables cannot be written\\&. Currently only applies to random seed "
"operations\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<--no-pager>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Do not pipe output into a pager\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Print a short version string and exit\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "On success, 0 is returned, a non-zero failure code otherwise\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"If I<$SYSTEMD_RELAX_ESP_CHECKS=1> is set the validation checks for the ESP "
"are relaxed, and the path specified with B<--esp-path=> may refer to any "
"kind of file system on any kind of partition\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Similarly, I<$SYSTEMD_RELAX_XBOOTLDR_CHECKS=1> turns off some validation "
"checks for the Extended Boot Loader partition\\&."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"B<systemd-boot>(7), \\m[blue]B<Boot Loader Specification>\\m[]\\&"
"\\s-2\\u[2]\\d\\s+2, \\m[blue]B<Boot Loader Interface>\\m[]\\&\\s-2\\u[3]\\d"
"\\s+2, B<systemd-boot-system-token.service>(8)"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Random Seeds"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "\\%https://systemd.io/RANDOM_SEEDS"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid " 2."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Boot Loader Specification"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "\\%https://systemd.io/BOOT_LOADER_SPECIFICATION"
msgstr ""

#. type: IP
#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid " 3."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "Boot Loader Interface"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable mageia-cauldron
msgid "\\%https://systemd.io/BOOT_LOADER_INTERFACE"
msgstr ""

#. type: TH
#: debian-buster
#, no-wrap
msgid "systemd 244"
msgstr ""
