# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-04 20:33+02:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "PWD"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "March 2020"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr ""

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "pwd - print name of current/working directory"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<pwd> [I<\\,OPTION\\/>]..."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Print the full filename of the current working directory."
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-L>, B<--logical>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "use PWD from environment, even if it contains symlinks"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-P>, B<--physical>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "avoid all symlinks"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "display this help and exit"
msgstr ""

#. type: TP
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "output version information and exit"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "If no option is specified, B<-P> is assumed."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"NOTE: your shell may have its own version of pwd, which usually supersedes "
"the version described here.  Please refer to your shell's documentation for "
"details about the options it supports."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Written by Jim Meyering."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "getcwd(3)"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/pwdE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "or available locally via: info \\(aq(coreutils) pwd invocation\\(aq"
msgstr ""

#. type: TH
#: debian-buster
#, no-wrap
msgid "February 2019"
msgstr ""

#. type: TH
#: debian-buster debian-unstable
#, no-wrap
msgid "GNU coreutils 8.30"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Report pwd translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: debian-buster debian-unstable
msgid ""
"Full documentation at: E<lt>https://www.gnu.org/software/coreutils/pwdE<gt>"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2019"
msgstr ""
