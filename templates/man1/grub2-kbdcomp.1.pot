# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-10 17:55+02:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB-KBDCOMP"
msgstr ""

#. type: TH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Wed Feb 26 2014"
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<grub-kbdcomp> \\(em Generate a GRUB keyboard layout file."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<grub-kbdcomp> E<lt>-o | --output=I<FILE>E<gt> I<CKBMAP_ARGUMENTS>"
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"B<grub-kbdcomp> processes an X keyboard layout description in B<keymaps>(5) "
"format into a format that can be used by GRUB's B<keymap> command."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--output>=I<FILE>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "Write output to I<FILE>."
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<info grub>"
msgstr ""
