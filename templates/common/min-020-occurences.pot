# Common msgids
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2020-04-18 20:26+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
#, no-wrap
msgid " 1."
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
#, no-wrap
msgid " 2."
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
#, no-wrap
msgid " 3."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \\e\n"
"                        } while (0)\n"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "* \\ \\ "
msgstr ""

#: fedora-rawhide archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "-?, B<--help>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ".br\n"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "0"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "1"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "1."
msgstr ""

#: debian-buster archlinux debian-unstable mageia-cauldron
#, no-wrap
msgid "10Dec18"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "160"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "162"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "163"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "164"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "166"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "167"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "170"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "171"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "172"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "173"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "175"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "176"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "177"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "187"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2."
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "200"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "201"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "202"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "204"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "205"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "206"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "207"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "209"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "211"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "212"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "213"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "214"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "215"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "216"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "217"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "218"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "224"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "225"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "226"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "227"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "228"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "229"
msgstr ""

#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "22Mar20"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "230"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "231"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "232"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "233"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "234"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "235"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "236"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "237"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "238"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "239"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "240"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "241"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "242"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "244"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "254"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "255"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "273"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "3."
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "340"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "341"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "342"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "344"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "345"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "346"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "347"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "350"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "351"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "352"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "353"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "354"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "355"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "356"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "357"
msgstr ""

#: archlinux fedora-rawhide debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "360"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "361"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "362"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "A0"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "A4"
msgstr ""

#: debian-buster debian-unstable archlinux mageia-cauldron
#, no-wrap
msgid "AC"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "AD"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<    ./configure; make dvi; dvips mtools.dvi>\n"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<    ./configure; make html>\n"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<    ./configure; make info>\n"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<#include E<lt>complex.hE<gt>>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

#: archlinux fedora-rawhide mageia-cauldron debian-buster debian-unstable
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<#include E<lt>sys/types.hE<gt>>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
msgid "B<--no-pager>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--usage>"
msgstr ""

#: debian-buster debian-unstable fedora-rawhide mageia-cauldron archlinux
#, no-wrap
msgid "B<-V>"
msgstr ""

#: archlinux fedora-rawhide mageia-cauldron debian-buster debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-a>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-a>, B<--all>"
msgstr ""

#: debian-buster debian-unstable fedora-rawhide mageia-cauldron archlinux
#, no-wrap
msgid "B<-b>"
msgstr ""

#: debian-buster debian-unstable fedora-rawhide mageia-cauldron archlinux
#, no-wrap
msgid "B<-c>"
msgstr ""

#: debian-buster debian-unstable fedora-rawhide mageia-cauldron archlinux
#, no-wrap
msgid "B<-d>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-f>"
msgstr ""

#: debian-buster debian-unstable archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-h, --help>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-h>"
msgstr ""

#: archlinux fedora-rawhide mageia-cauldron debian-buster debian-unstable
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr ""

#: debian-buster debian-unstable fedora-rawhide mageia-cauldron archlinux
#, no-wrap
msgid "B<-l>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-n>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-p>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-q>, B<--quiet>"
msgstr ""

#: debian-buster debian-unstable fedora-rawhide mageia-cauldron archlinux
#, no-wrap
msgid "B<-r>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-s>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-t>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-v>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<0>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<1>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EACCES>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EAGAIN>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EBUSY>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EEXIST>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EINTR>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EIO>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ELOOP>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EMFILE>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENFILE>"
msgstr ""

#: archlinux debian-unstable fedora-rawhide mageia-cauldron debian-buster
#, no-wrap
msgid "B<ENODEV>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENOENT>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENOSPC>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENOSYS>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ENOTDIR>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ERANGE>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EROFS>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<ESRCH>"
msgstr ""

#: fedora-rawhide mageia-cauldron
msgid "B<info grub>"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "BB"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Before glibc 2.10:"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "C library/kernel differences"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
#, no-wrap
msgid "COMMANDS"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Char"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Compile and link with I<-pthread>.\n"
msgstr ""

#: archlinux
msgid "Current maintainers:"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DA"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DF"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Dec"
msgstr ""

#: archlinux debian-unstable mageia-cauldron
#, no-wrap
msgid "December 2019"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Description"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
msgid "Do not pipe output into a pager\\&."
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "E0"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "E1"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "E2"
msgstr ""

#: debian-buster archlinux debian-unstable mageia-cauldron
#, no-wrap
msgid "E2fsprogs version 1.45.5"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "E4"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "E5"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "E6"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "E7"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "E8"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "E9"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EA"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EB"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EC"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ED"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EE"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "EF"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ENVIRONMENT VARIABLES"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "F1"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "F2"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "HISTORY"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Hex"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "I<Note>: There is no glibc wrapper for this system call; see NOTES."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "If I<x> is a NaN, a NaN is returned."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Insufficient kernel memory was available."
msgstr ""

#: debian-buster fedora-rawhide debian-unstable archlinux mageia-cauldron
#, no-wrap
msgid "January 2020"
msgstr ""

#: archlinux fedora-rawhide mageia-cauldron debian-buster debian-unstable
#, no-wrap
msgid "July 2014"
msgstr ""

#: debian-buster
#, no-wrap
msgid "June 2019"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Link with I<-lm>."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "MT-Safe locale"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""

#: fedora-rawhide archlinux debian-buster debian-unstable mageia-cauldron
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Mtools' texinfo doc"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "NO-BREAK SPACE"
msgstr ""

#: debian-buster debian-unstable archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Name"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Note\\ of\\ warning"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "Oct"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
msgid "On success, 0 is returned, a non-zero failure code otherwise\\&."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "Out of memory."
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
msgid "Print a short help text and exit\\&."
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
msgid "Print a short version string and exit\\&."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Program source"
msgstr ""

#: archlinux debian-buster debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "See\\ Also"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Since glibc 2.10:"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
#, no-wrap
msgid "System Administration"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The following errors can occur:"
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron fedora-rawhide
msgid "The following options are understood:"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"The texinfo doc looks most pretty when printed or as html.  Indeed, in the "
"info version certain examples are difficult to read due to the quoting "
"conventions used in info."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "The variant returning I<double> also conforms to SVr4, 4.3BSD, C89."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "These functions first appeared in glibc in version 2.1."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation, and may not be entirely accurate or complete.  See the end of "
"this man page for details."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation. However, this process is only approximative, and some items, "
"such as crossreferences, footnotes and indices are lost in this translation "
"process.  Indeed, these items have no appropriate representation in the "
"manpage format.  Moreover, not all information has been translated into the "
"manpage version.  Thus I strongly advise you to use the original texinfo "
"doc.  See the end of this manpage for instructions how to view the texinfo "
"doc."
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "To generate a html copy, run:"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"To generate a printable copy from the texinfo doc, run the following "
"commands:"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "To generate an info copy (browsable using emacs' info mode), run:"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Viewing\\ the\\ texi\\ doc"
msgstr ""

#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Wed Feb 26 2014"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""

#: archlinux debian-buster debian-unstable mageia-cauldron
#, no-wrap
msgid "\\ "
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"\\&A premade html can be found at \\&\\&CW<\\(ifhttp://www.gnu.org/software/"
"mtools/manual/mtools.html\\(is>"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\(bu"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "_"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "_GNU_SOURCE"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr ""

#: fedora-rawhide archlinux debian-buster debian-unstable mageia-cauldron
msgid "give a short usage message"
msgstr ""

#: fedora-rawhide archlinux debian-buster debian-unstable mageia-cauldron
msgid "give this help list"
msgstr ""

#: debian-buster
#, no-wrap
msgid "mtools-4.0.23"
msgstr ""

#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "mtools-4.0.24"
msgstr ""

#: fedora-rawhide archlinux debian-buster debian-unstable mageia-cauldron
msgid "print program version"
msgstr ""

#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
msgid "should give you access to the complete manual."
msgstr ""

#: archlinux debian-unstable mageia-cauldron fedora-rawhide
#, no-wrap
msgid "systemd 245"
msgstr ""
